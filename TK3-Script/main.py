import random

from models.Table import Table
from utils.Create_Table_From_CSV import create_table_from_csv, create_table_from_iterable_data
from faker import Faker


fake = Faker()


def create_users():
    return create_table_from_csv('User_Sion', 'User_Sion')


def create_sponsors():
    return create_table_from_csv('Sponsor', 'Sponsor')


def create_relawans():
    return create_table_from_csv('Relawan', 'Relawan')


def create_donaturs():
    return create_table_from_csv('Donatur', 'Donatur')


def create_keahlian_relawans(relawan: Table):
    NUM_OF_ROWS = 225

    field_names = ['email', 'keahlian']

    data = []

    relawan_data = relawan.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        email = random.choice(relawan_data).email
        keahlian = fake.bs()

        while (email + ' ' + keahlian) in pk_constraint:
            email = random.choice(relawan_data).email
            keahlian = fake.job()

        data.append([email, keahlian])

        pk_constraint.add(email + ' ' + keahlian)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Keahlian_Relawan')


def create_organisasis():

    NUM_OF_ROWS = 30

    status = ['Verified', 'Not verified']
    https = ['http', 'https']

    field_names = [
        'email_organisasi', 'website', 'nama',
        'provinsi', 'kabupaten_kota', 'kecamatan', 'kelurahan',
        'kode_pos', 'status_verifikasi',
    ]

    data = []

    pk_constraint= set()

    for i in range(NUM_OF_ROWS):
        domain_name = fake.domain_name()
        email_organisasi = fake.user_name() + '@' + domain_name

        while email_organisasi in pk_constraint:
            domain_name = fake.domain_name()
            email_organisasi = fake.user_name() + '@' + domain_name

        website = https[random.randint(0, 1)] + '://' + domain_name + '/'
        nama = fake.company() + ' ' + fake.company_suffix()
        provinsi = fake.state()
        kabupaten_kota = fake.city()
        kecamatan = fake.street_name()
        kelurahan = fake.street_name()
        kode_pos = fake.postcode()
        status_verifikasi = status[random.randint(0, 1)]

        data.append([email_organisasi, website, nama,
                         provinsi, kabupaten_kota, kecamatan,
                         kelurahan, kode_pos, status_verifikasi])

        pk_constraint.add(email_organisasi)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Organisasi')

def create_pengurus_organisasis(users: Table, organisasis: Table):

    NUM_OF_ROWS = 110


    field_names = ['email', 'organisasi']

    user_data = users.model.datas
    organisasi_data = organisasis.model.datas

    data = []

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        email = random.choice(user_data).email
        while email in pk_constraint:
            email = random.choice(user_data).email

        organisasi = random.choice(organisasi_data).email_organisasi

        data.append([email, organisasi])

        pk_constraint.add(email)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Pengurus_Organisasi')



def create_tujuan_organisasis(organisasis: Table):
    NUM_OF_ROWS = 15

    field_names = ['organisasi', 'tujuan']

    data = []

    pk_organisasi = set()

    organisasis_data = organisasis.model.datas

    for i in range(NUM_OF_ROWS):
        organisasi = random.choice(organisasis_data).email_organisasi
        while (organisasi in pk_organisasi):
            organisasi = random.choice(organisasis_data).email_organisasi
        tujuan = fake.bs()

        pk_organisasi.add(organisasi)
        data.append([organisasi, tujuan])

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Tujuan_Organisasi')

def create_relawan_organisasis(relawans: Table, organisasis: Table):
    NUM_OF_ROWS = 110

    field_names = ['email_relawan', 'organisasi']

    data = []

    organisasis_data = organisasis.model.datas
    relawans_data = relawans.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        email_relawan = random.choice(relawans_data).email
        organisasi = random.choice(organisasis_data).email_organisasi
        while (email_relawan+organisasi in pk_constraint):
            email_relawan = random.choice(relawans_data).email
            organisasi = random.choice(organisasis_data).email_organisasi

        data.append([email_relawan, organisasi])
        pk_constraint.add(email_relawan+organisasi)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Relawan_Organisasi')


def create_organisasi_tervirifikasi(organisasis: Table):
    NUM_OF_ROWS = 30

    status = ['Active', 'Inactive']

    field_names = ['email_organisasi', 'nomor_registrasi', 'status_aktif']

    data = []

    organisasis_data = organisasis.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        email_organisasi = random.choice(organisasis_data).email_organisasi
        while (email_organisasi in pk_constraint):
            email_organisasi = random.choice(organisasis_data).email_organisasi

        status_aktif = random.choice(status)
        nomor_registrasi = fake.pystr(max_chars=50)

        data.append([email_organisasi, nomor_registrasi, status_aktif])
        pk_constraint.add(email_organisasi)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Organisasi_Terverifikasi')


def create_donatur_organisasi(donatur: Table, organisasi_terverifikasi: Table):
    NUM_OF_ROWS = 30

    field_names = ['donatur', 'organisasi', 'tanggal', 'nominal']

    data = []

    organisasi_terverifikasi_data= organisasi_terverifikasi.model.datas

    donaturs_data = donatur.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        donatur = random.choice(donaturs_data).email
        organisasi = random.choice(organisasi_terverifikasi_data).email_organisasi
        while (donatur + ' ' + organisasi) in pk_constraint:
            donatur = random.choice(donaturs_data).email
            organisasi = random.choice(organisasi_terverifikasi_data).email_organisasi

        tanggal = fake.date_this_decade().strftime('%m/%d/%Y')
        nominal = str(random.randint(0, 2000000000))

        data.append([donatur, organisasi, tanggal, nominal])
        pk_constraint.add(donatur + ' ' + organisasi)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Donatur_Organisasi')


def create_penilaian_performa(relawan: Table, organisasi_terverifikasi: Table):
    NUM_OF_ROWS = 75

    field_names = ['email_relawan', 'organisasi', 'id', 'deskripsi', 'tgl_penilaian', 'nilai_skala']

    data = []

    relawan_data = relawan.model.datas
    organisasi_terverifikasi_data = organisasi_terverifikasi.model.datas

    pk_constraint = list()

    for i in range(NUM_OF_ROWS):
        email_relawan = random.choice(relawan_data).email
        organisasi = random.choice(organisasi_terverifikasi_data).email_organisasi
        tgl_penilaian = fake.date_this_decade()

        for j in range(random.randint(1, 5)):
            id = str(pk_constraint.count((email_relawan + ' ' + organisasi)) + 1)

            deskripsi = fake.sentence()
            nilai_skala = str(random.randint(0, 5))
            tgl_penilaian = fake.date_between_dates(date_start=tgl_penilaian)

            data.append([email_relawan, organisasi, id, deskripsi, tgl_penilaian.strftime('%m/%d/%Y'), nilai_skala])
            pk_constraint.append((email_relawan + ' ' + organisasi))

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Penilaian_Performa')


def create_sponsor_organisasi(sponsor: Table, organisasi: Table):
    NUM_OF_ROWS = 50

    field_names = ['sponsor', 'organisasi', 'tanggal', 'nominal']

    data = []

    sponsor_data = sponsor.model.datas
    organisasi_data = organisasi.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        sponsor = random.choice(sponsor_data).email
        organisasi = random.choice(organisasi_data).email_organisasi

        while (sponsor + ' ' + organisasi) in pk_constraint:
            sponsor = random.choice(sponsor_data).email
            organisasi = random.choice(organisasi_data).email_organisasi

        tanggal = fake.date_this_decade()
        nominal = str(random.randint(0, 2000000000))

        data.append([sponsor, organisasi, tanggal.strftime('%m/%d/%Y'), nominal])
        pk_constraint.add((sponsor + ' ' + organisasi))

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Sponsor_Organisasi')

def create_kegiatan(organisasi_terverifikasi: Table):
    NUM_OF_ROWS = 200

    field_names = ['kode_unik', 'organisasi_perancang', 'judul',
                   'dana_dibutuhkan', 'tanggal_mulai', 'tanggal_selesai',
                   'deskripsi']

    data = []

    organisasi_data = organisasi_terverifikasi.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        kode_unik = fake.pystr(max_chars=20)
        while kode_unik in pk_constraint:
            kode_unik = fake.pystr(max_chars=20)

        organisasi_perancang = random.choice(organisasi_data).email_organisasi
        judul = ' '.join([word.capitalize() for word in fake.words(nb=random.randint(1, 5))])
        dana_dibutuhkan = str(random.randint(0, 200000000))
        tanggal_mulai = fake.date_this_decade()
        tanggal_selesai = fake.date_between_dates(date_start=tanggal_mulai)
        deskripsi = fake.paragraph()

        data.append([kode_unik, organisasi_perancang, judul, dana_dibutuhkan,
                     tanggal_mulai.strftime('%m/%d/%Y'), tanggal_selesai.strftime('%m/%d/%Y'),
                     deskripsi])

        pk_constraint.add(kode_unik)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Kegiatan')


def create_donatur_kegiatan(donatur: Table, kegiatan: Table):
    NUM_OF_ROWS = 50

    field_names = ['donatur', 'kegiatan', 'tanggal', 'nominal']

    data = []

    donatur_data = donatur.model.datas
    kegiatan_data = kegiatan.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        donatur = random.choice(donatur_data).email
        kegiatan = random.choice(kegiatan_data).kode_unik

        while (donatur + ' ' + kegiatan) in pk_constraint:
            donatur = random.choice(donatur_data).email
            kegiatan = random.choice(kegiatan_data).kode_unik

        tanggal = fake.date_this_decade()
        nominal = str(random.randint(0, 2000000000))

        data.append([donatur, kegiatan, tanggal.strftime('%m/%d/%Y'), nominal])
        pk_constraint.add((donatur + ' ' + kegiatan))

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Donatur_Kegiatan')


def create_kategori():
    NUM_OF_ROWS = 50

    field_names = ['kode', 'nama']

    data = []

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        kode = fake.pystr(max_chars=20)

        while kode in pk_constraint:
            kode = fake.pystr(max_chars=20)

        nama = ' '.join([word.capitalize() for word in fake.words(nb=random.randint(1, 2))])

        data.append([kode, nama])
        pk_constraint.add(kode)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Kategori')


def create_kategori_kegiatan(kategori: Table, kegiatan: Table):
    NUM_OF_ROWS = 50

    field_names = ['kode_kegiatan', 'kode_kategori']

    data = []

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        kode_kegiatan = random.choice(kegiatan.model.datas).kode_unik
        kode_kategori = random.choice(kategori.model.datas).kode

        while (kode_kegiatan + ' ' + kode_kategori) in pk_constraint:
            kode_kegiatan = random.choice(kegiatan.model.datas).kode_unik
            kode_kategori = random.choice(kategori.model.datas).kode

        data.append([kode_kegiatan, kode_kategori])
        pk_constraint.add(kode_kegiatan + ' ' + kode_kategori)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Kategori_Kegiatan')


def create_reward(kegiatan: Table):
    NUM_OF_ROWS = 75

    field_names = ['kode_kegiatan', 'barang_reward', 'harga_min', 'harga_max']

    data = []

    pk_constraint = set()

    kegiatan_data = kegiatan.model.datas

    for i in range(NUM_OF_ROWS):
        kode_kegiatan = random.choice(kegiatan_data).kode_unik
        barang_reward = fake.word().capitalize()

        while (kode_kegiatan + ' ' + barang_reward) in pk_constraint:
            kode_kegiatan = random.choice(kegiatan_data).kode_unik
            barang_reward = fake.word()

        harga_min = str(random.randint(0, 200000000))
        harga_max = str(random.randint(int(harga_min), 2000000000))

        data.append([kode_kegiatan, barang_reward, harga_min, harga_max])
        pk_constraint.add(kode_kegiatan + ' ' + barang_reward)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Reward')


def create_berita(kegiatan: Table):
    NUM_OF_ROWS = 75

    field_names = ['kode_unik', 'kegiatan', 'judul',
                   'deskripsi', 'tgl_update', 'tgl_kegiatan']

    data = []

    kegiatan_data= kegiatan.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        kode_unik = fake.pystr(max_chars=20)
        while kode_unik in pk_constraint:
            kode_unik = fake.pystr(max_chars=20)

        kegiatan = random.choice(kegiatan_data).kode_unik
        judul = ' '.join([word.capitalize() for word in fake.words(nb=random.randint(1, 5))])
        tgl_update= fake.date_this_decade()
        tgl_kegiatan = fake.date_between_dates(date_start=tgl_update)
        deskripsi = fake.paragraph()

        data.append([kode_unik, kegiatan, judul, deskripsi,
                     tgl_update.strftime('%m/%d/%Y'), tgl_kegiatan.strftime('%m/%d/%Y')])

        pk_constraint.add(kode_unik)

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Berita')


def create_laporan_keuangan(organisasi: Table):
    NUM_OF_ROWS = 350

    field_names = ['organisasi', 'tgl_dibuat', 'rincian_pemasukan',
                   'total_pemasukan', 'total_pengeluaran', 'is_disetujui']

    data = []

    organisasi_data = organisasi.model.datas

    pk_constraint = set()

    for i in range(NUM_OF_ROWS):
        organisasi = random.choice(organisasi_data).email_organisasi
        tgl_dibuat = fake.date_this_decade()
        while (organisasi + ' ' + tgl_dibuat.strftime('%m/%d/%Y')) in pk_constraint:
            organisasi = random.choice(organisasi_data).email_organisasi
            tgl_dibuat = fake.date_this_decade()

        rincian_pemasukan = fake.paragraph()
        total_pemasukan = random.randint(0, 200000000)
        total_pengeluaran = random.randint(0, 200000000)
        is_disetujui = str(random.randint(0, 1) == 0);

        data.append([organisasi, tgl_dibuat.strftime('%m/%d/%Y'), rincian_pemasukan, str(total_pemasukan),
                     str(total_pengeluaran), is_disetujui])

        pk_constraint.add(organisasi + ' ' + tgl_dibuat.strftime('%m/%d/%Y'))

    return create_table_from_iterable_data(field_names=field_names, data=data, table_name='Laporan_Keuangan')


users = create_users()
sponsors = create_sponsors()
donaturs = create_donaturs()
relawans = create_relawans()
keahlian_relawans = create_keahlian_relawans(relawans)

# Without CSV
organisasis = create_organisasis()
pengurus_organisasis = create_pengurus_organisasis(users, organisasis)
tujuan_organisasis = create_tujuan_organisasis(organisasis)
relawan_organisasis = create_relawan_organisasis(relawans=relawans, organisasis=organisasis)
organisasi_terverifikasi = create_organisasi_tervirifikasi(organisasis)
donatur_organisasi = create_donatur_organisasi(donaturs, organisasi_terverifikasi)
penilaian_performa = create_penilaian_performa(relawans, organisasi_terverifikasi)
sponsor_organisasi = create_sponsor_organisasi(sponsor=sponsors, organisasi=organisasis)
kegiatan = create_kegiatan(organisasi_terverifikasi)
donatur_kegiatan = create_donatur_kegiatan(donaturs, kegiatan)
kategori = create_kategori()
kategori_kegiatan = create_kategori_kegiatan(kategori, kegiatan)
reward = create_reward(kegiatan)
berita = create_berita(kegiatan)
laporan_keuangan = create_laporan_keuangan(organisasi_terverifikasi)

tables = [
    users,
    sponsors,
    donaturs,
    relawans,
    keahlian_relawans,
    organisasis,
    pengurus_organisasis,
    tujuan_organisasis,
    relawan_organisasis,
    organisasi_terverifikasi,
    donatur_organisasi,
    penilaian_performa,
    sponsor_organisasi,
    kegiatan,
    donatur_kegiatan,
    kategori,
    kategori_kegiatan,
    reward,
    berita,
    laporan_keuangan,
]

output_file = open('sql/insert.sql', 'w')

for table in tables:
    output_file.write(table.model.to_sql(table.table_name) + '\n')

output_file.close()