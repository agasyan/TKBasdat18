from models.SqlModel import SqlModelContainer

class Table:
    def __init__(self, model: SqlModelContainer, table_name: str):
        self.model = model
        self.table_name = table_name