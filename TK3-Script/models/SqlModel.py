class SqlModelContainer:
    def __init__(self):
        self.datas = []

    def to_sql(self, table_name):
        if len(self.datas) == 0:
            raise Exception('Empty')

        sql = 'INSERT INTO %s (' % table_name.upper()
        public_attrs = [attr for attr in dir(self.datas[0]) if not attr.startswith('__')]
        for attr in public_attrs:
            sql += attr + ','
        sql = sql[:-1] + ') VALUES '
        for i1, data in enumerate(self.datas):
            if i1 > 0:
                sql += ',\n'
            sql += '('
            for i2, attr in enumerate(public_attrs):
                if i2 > 0:
                    sql += ', '
                sql += "'%s'" % data.__getattribute__(attr).replace("'", "''")
            sql += ')'
        return sql + ';'

    def is_pk_constraint_satisfied(self, **kwargs):
        if kwargs.items() == 0:
            raise Exception()
        key, value = kwargs.items()[0]
        for data in self.datas:
            if data.__getattribute__(key) == value:
                return False
        return True


class SqlModel:

    def __init__(self):
        pass
