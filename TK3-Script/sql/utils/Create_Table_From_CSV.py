from models.SqlModel import SqlModelContainer, SqlModel
from models.Table import Table

import csv


def create_table_from_csv(file_name: str, table_name:str):
    table = SqlModelContainer()
    with open('csv/%s.csv' % file_name) as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        for row in reader:
            tuple = SqlModel()
            for key in reader.fieldnames:
                tuple.__setattr__(key, row[key])
            table.datas.append(tuple)

    return Table(model=table, table_name=table_name)


def create_table_from_iterable_data(field_names: list, data: list, table_name: str):
    table = SqlModelContainer()
    for row in data:
        model = SqlModel()
        for index, key in enumerate(field_names):
            model.__setattr__(key, row[index])
        table.datas.append(model)

    return Table(model=table, table_name=table_name)