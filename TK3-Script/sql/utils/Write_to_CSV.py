from models.Table import Table
import csv


def write_to_csv(table: Table):
    with open('csv/%s.csv' % table.table_name, 'w') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        public_props = [attr for attr in dir(table.model.datas[0]) if not attr.startswith('__')]
        writer.writerow(public_props)
        for data in table.model.datas:
            row = []
            for attr in public_props:
                row.append(data.__getattribute__(attr))
            writer.writerow(row)