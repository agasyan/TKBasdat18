# SION - Sistem Informasi Organisasi Nirlaba Application
Tugas Akhir Basis Data

## Tech Stack
- Flask
- PostgreSQL
- Vue.js
- Bootstrap
- Heroku

## Installation & Getting Started
```
virtualenv venv
source venv/bin/activate
pip install requirements.txt
python3 run.py

```

## Todos
[] Add todos

### About Us
**Kelompok B02**
- Albertus Angga Raharja
- Aldo Bima Syahputra
- Agas Yanpratama

