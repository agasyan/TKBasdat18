from django.test import SimpleTestCase
from .helpers import create_insert_sql


class TestHelpers(SimpleTestCase):
    def test_create_insert_sql(self):
        actual_sql = create_insert_sql(table_name='SION__GeneralUser', email='albertusangga@gmail.com', password='password', nama='Albertus Angga')
        expected_sql = 'INSERT INTO %s (%s, %s, %s) VALUES (%s, %s, %s);' % ('SION__GeneralUser',
                                                                            'email',
                                                                            'password',
                                                                            'nama',
                                                                            'albertusangga@gmail.com',
                                                                            'password',
                                                                            'Albertus Angga'
                                                                            );
        self.assertEqual(actual_sql, expected_sql)
