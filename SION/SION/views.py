from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from SION.registrasi import forms
from SION.models import UserSion, Donatur, Organisasi, TujuanOrganisasi, PengurusOrganisasi, Sponsor, Relawan, KeahlianRelawan

from django.db import connection


def index_view(request):
    return render(request, 'index.html')


def registrasi_view(request):
    return redirect('/registrasi/donatur/')


@login_required(login_url='/login/')
def profile_view(request):
    user = {}
    if request.user.is_authenticated:
        user = request.user
        if user.is_donatur:
            donatur = Donatur.objects.raw("SELECT * FROM %s WHERE email='%s';" % (
                Donatur._meta.db_table, user.email
            ))[0]
            return render(request, 'profile.html', {'user':user, 'donatur':donatur})
        elif user.is_relawan:
            relawan = Relawan.objects.raw("SELECT * FROM %s WHERE email = '%s';" % (
                Relawan._meta.db_table, user.email
                ))[0]
            with connection.cursor() as cursor:
                cursor.execute("SELECT keahlian FROM %s WHERE email = '%s';" % (KeahlianRelawan._meta.db_table, user.email))
                relawan_keahlian = dictfetchall(cursor)
            if (relawan_keahlian == None):
                relawan_keahlian = "(TIDAK ADA KEAHLIAN)"
            print(relawan_keahlian)
            return render(request,'profile.html',{'user':user,'relawan_keahlian':relawan_keahlian,'relawan':relawan})
        elif user.is_sponsor:
            sponsor = Sponsor.objects.raw("SELECT * FROM %s WHERE email='%s';" % (
                Sponsor._meta.db_table, user.email
            ))[0]
            return render(request, 'profile.html', {'user':user, 'sponsor':sponsor})
            
    return render(request, 'profile.html', {'user':user})

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def organisasi_list_view(request):
    #ambil data semua daftar organisasi
    list_organisasi = Organisasi.objects.raw("SELECT * FROM %s" % (
                Organisasi._meta.db_table
            ))
    
    return render(request, 'lihat_organisasi.html',{'list_organisasi': list_organisasi})


def organisasi_detail_view(request,email):
    #ambil data organisasi terkait
    organisasi = Organisasi.objects.raw("SELECT * FROM %s WHERE email_organisasi = '%s';" % (
        Organisasi._meta.db_table, email
    ))[0]


    #ambil data tujuan organisasi terkait
    with connection.cursor() as cursor:
        cursor.execute("SELECT tujuan FROM tujuan_organisasi WHERE organisasi = '%s';" % (email))
        tujuan = (cursor.fetchone())
    
    if (tujuan == None):
        tujuan = "-"
    else :
        tujuan = tujuan[0]

    #ambil data pengurus
    pengurus = UserSion.objects.raw("SELECT nama AS nama, p.email AS email FROM %s as u, pengurus_organisasi as p WHERE p.organisasi = '%s' AND p.email = u.email;" % (
            UserSion._meta.db_table,  email
        ))

    #ambil data donatur
    donatur = UserSion.objects.raw("SELECT DISTINCT nama AS nama, d.organisasi AS email FROM %s as u, donatur_organisasi as d WHERE d.organisasi = '%s' AND d.donatur = u.email;" % (
            UserSion._meta.db_table,  email
        ))


    #ambil data sponsor
    sponsor = UserSion.objects.raw("SELECT DISTINCT u.email AS email FROM %s as u, sponsor_organisasi as s WHERE s.organisasi = '%s' AND s.sponsor = u.email;" % (
            UserSion._meta.db_table,  email
        ))

    #ambil data total donasi
    with connection.cursor() as cursor:
        cursor.execute("SELECT SUM(nominal) FROM sponsor_organisasi WHERE organisasi = '%s'" % (email))
        jumlah_donasi_donatur = (cursor.fetchone()[0])

        cursor.execute("SELECT SUM(nominal) FROM donatur_organisasi WHERE organisasi = '%s'" % (email))
        jumlah_donasi_sponsor = (cursor.fetchone()[0])
    
    #check if query value is null, then set to 0
    if (jumlah_donasi_donatur == None) :
        jumlah_donasi_donatur = 0;

    if (jumlah_donasi_sponsor == None) :
        jumlah_donasi_sponsor = 0;

    total_donasi = jumlah_donasi_donatur + jumlah_donasi_sponsor
  
    return render(request, 'halaman_organisasi.html',{'organisasi':organisasi,'tujuan_organisasi':tujuan,'pengurus':pengurus,'donatur':donatur, 'sponsor':sponsor, 'total_donasi' : total_donasi})
    

def not_found(request):
    return render(request, '404.html')
