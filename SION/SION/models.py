from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import connection, models
from django.contrib.auth.hashers import (
    make_password,
)

from SION.utils.helpers import create_insert_sql


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password, nama, alamat_lengkap, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)

        with connection.cursor() as cursor:
            sql = create_insert_sql(table_name=UserSion._meta.db_table,
                                    email=email,
                                    password=make_password(password),
                                    nama=nama,
                                    alamat_lengkap=alamat_lengkap,
                                    **extra_fields)
            cursor.execute(sql)
            query_set = UserSion.objects.raw("SELECT * FROM %s WHERE email = '%s';" % (UserSion._meta.db_table, email))
            if len(list(query_set)) > 0:
                return query_set[0]
            else:
                raise Exception()
                return None

    def create_general_user(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self.create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, **extra_fields)

    def create_donatur(self, email, password, **extra_fields):
        user = self.create_general_user(email=email, password=password, **extra_fields)
        sql = create_insert_sql(table_name=Donatur._meta.db_table, email=email, saldo=0)
        with connection.cursor() as cursor:
            cursor.execute(sql)
            return user

    def create_relawan(self, email, password, no_hp, tanggal_lahir, keahlians, **extra_fields):
        user = self.create_general_user(email=email, password=password, **extra_fields)
        sqls = [create_insert_sql(table_name=Relawan._meta.db_table, email=email, no_hp=no_hp, tanggal_lahir=tanggal_lahir)]
        for key in keahlians:
            sqls.append(create_insert_sql(table_name=KeahlianRelawan._meta.db_table, email=email, keahlian=keahlians.get(key)))
        with connection.cursor() as cursor:
            for sql in sqls:
                cursor.execute(sql)
            return user

    def create_sponsor(self, email, password=None, logo_sponsor=None, **extra_fields):
        user = self.create_general_user(email=email, password=password, **extra_fields)
        sql = create_insert_sql(table_name=Sponsor._meta.db_table, email=email, logo_sponsor=logo_sponsor)
        with connection.cursor() as cursor:
            cursor.execute(sql)
            return user

    def create_pengurus(self, email, organisasi, **extra_fields):
        password = self.make_random_password()
        user = self.create_general_user(email=email, password=password, **extra_fields)
        with connection.cursor() as cursor:
            sql = create_insert_sql(table_name=PengurusOrganisasi._meta.db_table, email=email, organisasi=organisasi)
            cursor.execute(sql)
            return {'email': email, 'password': password}


class OrganisasiManager(models.Manager):
    def create_organisasi(self, email_organisasi, tujuan, *args, **kwargs):
        validated_data = {}
        for key in kwargs:
            if hasattr(Organisasi, key):
                validated_data[key] = kwargs.get(key)
        validated_data.setdefault('status_verifikasi', True)

        sql = create_insert_sql(table_name=Organisasi._meta.db_table, email_organisasi=email_organisasi, **validated_data)
        insert_tujuan_organisasi_sql = create_insert_sql(table_name=TujuanOrganisasi._meta.db_table, organisasi=email_organisasi, tujuan=tujuan)
        with connection.cursor() as cursor:
            cursor.execute(sql)
            cursor.execute(insert_tujuan_organisasi_sql)




class UserSion(AbstractBaseUser, PermissionsMixin):
    """
    Represents user and authentication model
    """
    objects = UserManager()

    # required fields
    email = models.EmailField(unique=True, primary_key=True)
    nama = models.CharField(max_length=100)
    alamat_lengkap = models.TextField()
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'user_sion'

    @property
    def role(self):
        if self.is_donatur:
            return 'Donatur'
        elif self.is_relawan:
            return 'Relawan'
        elif self.is_sponsor:
            return 'Sponsor'
        elif self.is_pengurus:
            return 'Pengurus'
        else:
            return 'Anonymous'

    @property
    def is_sponsor(self):
        query_set = Sponsor.objects.raw("SELECT * FROM %s WHERE email = '%s';" % (Sponsor._meta.db_table, self.email))
        return len(list(query_set)) > 0

    @property
    def is_donatur(self):
        query_set = Donatur.objects.raw("SELECT * FROM %s WHERE email = '%s';" % (
            Donatur._meta.db_table, self.email))
        return len(list(query_set)) > 0

    @property
    def is_relawan(self):
        query_set = Relawan.objects.raw("SELECT * FROM %s WHERE email = '%s';" %
                                        (Relawan._meta.db_table, self.email))
        return len(list(query_set)) > 0

    @property
    def is_pengurus(self):
        with connection.cursor() as cursor:
            sql = "SELECT * FROM %s WHERE email = '%s';" % (PengurusOrganisasi._meta.db_table, self.email)
            cursor.execute(sql)
            rows = cursor.fetchall()
            return len(rows) > 0

    def __str__(self):
        return self.email


class Donatur(models.Model):
    email = models.OneToOneField(UserSion, primary_key=True, on_delete=models.CASCADE, db_column='email')
    saldo = models.IntegerField(default=0)

    class Meta:
        db_table = 'donatur'
        managed = False

    def __str__(self):
        return self.email


class Sponsor(models.Model):
    email = models.OneToOneField(UserSion, primary_key=True, on_delete=models.CASCADE, db_column='email')
    logo_sponsor = models.ImageField(upload_to='uploads/')

    class Meta:
        db_table = 'sponsor'
        managed = False

    def __str__(self):
        return self.email


class Relawan(models.Model):
    email = models.OneToOneField(UserSion, primary_key=True, on_delete=models.CASCADE, db_column='email')
    no_hp = models.CharField(max_length=20)
    tanggal_lahir = models.DateField()

    class Meta:
        db_table = 'relawan'
        managed = False

    def __str__(self):
        return self.email


class KeahlianRelawan(models.Model):
    email = models.ForeignKey(Relawan, on_delete=models.CASCADE, db_column='email')
    keahlian = models.CharField(max_length=50)

    class Meta:
        db_table = 'keahlian_relawan'
        managed = False
        unique_together = (('email', 'keahlian'),)

    def __str__(self):
        return "%s %s" % (self.email, self.keahlian)


class Kategori(models.Model):
    kode = models.CharField(primary_key=True, max_length=50)
    nama = models.CharField(max_length=20)

    class Meta:
        db_table = 'kategori'
        managed = False

class Organisasi(models.Model):
    email_organisasi = models.CharField(primary_key=True, max_length=50)
    website = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    provinsi = models.CharField(max_length=50)
    kabupaten_kota = models.CharField(max_length=50)
    kecamatan = models.CharField(max_length=50)
    kelurahan = models.CharField(max_length=50)
    kode_pos = models.CharField(max_length=50)
    status_verifikasi = models.CharField(max_length=50)

    class Meta:
        db_table = 'organisasi'
        managed = False


class OrganisasiTerverifikasi(models.Model):
    email_organisasi = models.OneToOneField(Organisasi, on_delete=models.CASCADE, db_column='email_organisasi')
    nomor_registrasi = models.CharField(max_length=50)
    status_aktif = models.CharField(max_length=50)

    class Meta:
        db_table = 'organisasi_terverifikasi'
        managed = False


class PengurusOrganisasi(models.Model):
    email = models.OneToOneField(UserSion, on_delete=models.CASCADE, db_column='email')
    organisasi = models.ForeignKey(Organisasi, on_delete=models.CASCADE, db_column='organisasi')

    class Meta:
        db_table = 'pengurus_organisasi'
        managed = False


class PenilaianPerforma(models.Model):
    email_relawan = models.ForeignKey(Relawan, on_delete=models.CASCADE, db_column='email_relawan')
    organisasi = models.ForeignKey(OrganisasiTerverifikasi, on_delete=models.CASCADE, db_column='organisasi')
    id_number = models.IntegerField()
    tgl_penilaian = models.DateField()
    deskripsi = models.TextField()
    nilai_skala = models.IntegerField()

    class Meta:
        db_table = 'penilaian_performa'
        managed = False
        unique_together = (('email_relawan', 'organisasi', 'id_number'),)


class RelawanOrganisasi(models.Model):
    email_relawan = models.ForeignKey(Relawan, on_delete=models.CASCADE, db_column='email_relawan')
    organisasi = models.ForeignKey(Organisasi, on_delete=models.CASCADE, db_column='organisasi')

    class Meta:
        db_table = 'relawan_organisasi'
        managed = False
        unique_together = (('email_relawan', 'organisasi'),)


class SponsorOrganisasi(models.Model):
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE, db_column='sponsor')
    organisasi = models.ForeignKey(OrganisasiTerverifikasi, on_delete=models.CASCADE, db_column='organisasi')
    tanggal = models.DateField()
    nominal = models.IntegerField()

    class Meta:
        db_table = 'sponsor_organisasi'
        managed = False
        unique_together = (('sponsor', 'organisasi'),)


class TujuanOrganisasi(models.Model):
    organisasi = models.ForeignKey(Organisasi, on_delete=models.CASCADE, db_column='organisasi')
    tujuan = models.TextField()

    class Meta:
        db_table = 'tujuan_organisasi'
        managed = False
        unique_together = (('organisasi', 'tujuan'),)


class DonaturOrganisasi(models.Model):
    donatur = models.ForeignKey(Donatur, on_delete=models.CASCADE, db_column='donatur')
    nominal = models.IntegerField()
    organisasi = models.ForeignKey(OrganisasiTerverifikasi, on_delete=models.CASCADE, db_column='organisasi')
    tanggal = models.DateField()

    class Meta:
        db_table = 'donatur_organisasi'
        managed = False
        unique_together = (('donatur', 'organisasi'),)


class Kegiatan(models.Model):
    kode_unik = models.CharField(primary_key=True, max_length=20)
    organisasi_perancang = models.ForeignKey(OrganisasiTerverifikasi, on_delete=models.CASCADE, db_column='organisasi_perancang')
    judul = models.CharField(max_length=50)
    dana_dibutuhkan = models.IntegerField()
    tanggal_mulai = models.DateField()
    tanggal_selesai = models.DateField()
    deskripsi = models.TextField()

    class Meta:
        db_table = 'kegiatan'
        managed = False


class LaporanKeuangan(models.Model):
    organisasi = models.ForeignKey(OrganisasiTerverifikasi, on_delete=models.CASCADE, db_column='organisasi',)
    tgl_dibuat = models.DateField()
    total_pemasukan = models.IntegerField()
    total_pengeluaran = models.IntegerField()
    is_disetujui = models.BooleanField()
    rincian_pemasukan = models.TextField()

    class Meta:
        db_table = 'laporan_keuangan'
        managed = False
        unique_together = (('organisasi', 'tgl_dibuat'),)


class Berita(models.Model):
    kode_unik = models.CharField(primary_key=True, max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, db_column='kegiatan')
    judul = models.CharField(max_length=100)
    deskripsi = models.TextField()
    tgl_update = models.DateField()
    tgl_kegiatan = models.DateField()

    class Meta:
        db_table = 'berita'
        managed = False


class DonaturKegiatan(models.Model):
    donatur = models.ForeignKey(Donatur, on_delete=models.CASCADE, db_column='donatur')
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, db_column='kegiatan')
    tanggal = models.DateField()
    nominal = models.IntegerField()

    class Meta:
        db_table = 'donatur_kegiatan'
        managed = False
        unique_together = (('donatur', 'kegiatan'),)


class KategoriKegiatan(models.Model):
    kode_kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, db_column='kode_kegiatan')
    kode_kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE, db_column='kode_kategori')

    class Meta:
        db_table = 'kategori_kegiatan'
        managed = False
        unique_together = (('kode_kategori', 'kode_kegiatan'),)


class Reward(models.Model):
    kode_kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, db_column='kode_kegiatan')
    barang_reward = models.CharField(primary_key=True, max_length=20)
    harga_min = models.IntegerField()
    harga_max = models.IntegerField()

    class Meta:
        db_table = 'reward'
        managed = False
        unique_together = (('barang_reward', 'kode_kegiatan'),)