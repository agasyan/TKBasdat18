document.addEventListener('DOMContentLoaded', () => {
    const el = document.getElementsByClassName('tabs')[0];
    const instance = M.Tabs.init(el);
});

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#registrasi-app',
    data: {
        penguruses: [{
            index: 1,
            namaFormName: 'pengurus_nama_1',
            namaFormId: 'id_pengurus_nama_1',
            emailFormName: 'pengurus_email_1',
            emailFormId: 'id_pengurus_email_1',
            alamatFormName: 'pengurus_alamat_1',
            alamatFormId: 'id_pengurus_alamat_1',
        }],
    },
    methods: {
        addPengurus: function() {
            this.penguruses.push({
                index: this.penguruses.length + 1,
                namaFormName: `pengurus_nama_${this.penguruses.length + 1}`,
                namaFormId: `id_pengurus_nama_${this.penguruses.length + 1}`,
                emailFormName: `pengurus_email_${this.penguruses.length + 1}`,
                emailFormId: `id_pengurus_email_${this.penguruses.length + 1}`,
                alamatFormName: `pengurus_alamat_${this.penguruses.length + 1}`,
                alamatFormId: `id_pengurus_alamat_${this.penguruses.length + 1}`,
            });
        },
        deletePengurus: function() {
            if (this.penguruses.length > 1) {
                this.penguruses.pop();
            }
        }
    }
});