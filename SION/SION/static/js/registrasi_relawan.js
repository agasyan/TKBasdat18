var numberOfKeahlian = 0;

document.addEventListener('DOMContentLoaded', () => {
    const el = document.getElementsByClassName('tabs')[0];
    const instance = M.Tabs.init(el);
});

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#registrasi-app',
    data: {
        message: "Hello World!",
        nama: '',
        email: '',
        password: '',
        confirmPassword: '',
        kecamatan: '',
        kabupaten: '',
        provinsi: '',
        kodepos: '',
        no_hp: '',
        tgl_lahir: '',
        keahlians: [{ index: 1, formName: 'keahlian_1', formId: 'id_keahlian_1' }],
    },
    methods: {
        addKeahlian: function() {
            this.keahlians.push({
                index: this.keahlians.length + 1,
                formName: `keahlian_${this.keahlians.length + 1}`,
                formId: `id_keahlian_${this.keahlians.length + 1}`,
            });
        },
        deleteKeahlian: function() {
            if (this.keahlians.length > 1) this.keahlians.pop();
        },
    }
});