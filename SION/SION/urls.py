"""SION URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from SION.auth.urls import urlpatterns as auth_urls
from SION.registrasi.urls import urlpatterns as register_urls
from SION.donasi_organisasi.urls import urlpatterns as donasi_organisasi_urls

from . import views

urlpatterns = [
    path('', views.index_view),
    path('registrasi/', views.registrasi_view),
    path('organisasi/',     views.organisasi_list_view),
    path('organisasi/halaman/<email>', views.organisasi_detail_view),
    path('profile/', views.profile_view),
    path('admin/', admin.site.urls),
    *auth_urls,
    *register_urls,
    *donasi_organisasi_urls,
]
