from django.urls import path

from .import views

urlpatterns = [
    path('organisasi/donasi/', views.organisasi_donasi_view),
]

