from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from SION.models import Organisasi
from django.db import connection


class DonasiOrganisasiForm(forms.Form):
    #get data all orgnisasi for select form
    CHOICES = list();
    list_organisasi = Organisasi.objects.raw("SELECT * FROM %s" % (
                Organisasi._meta.db_table
            ))
    for organisasi in list_organisasi :
        CHOICES.append((organisasi.email_organisasi, organisasi.nama))

    #select form
    select_organisasi = forms.ChoiceField(choices=CHOICES)

    #nominal form
    nominal = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'validate', 'placeholder': 'nominal, angka integer bulat'}),
        label='Nominal',
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        super().clean()
        cleaned_data = self.cleaned_data
        
        nominal = cleaned_data['nominal']
        select_organisasi = cleaned_data['select_organisasi']
        
        #cek minimum nominal donasi
        if (self.user.is_sponsor) :
            min_nominal = 2000000
        else :
            min_nominal = 0
            
        print(min_nominal)
        if (nominal < min_nominal) :
            minimum_nominal_constraint_error = ValidationError(_('Anda harus berdonasi minimal sebesar %s'%(min_nominal)))
            self.add_error('nominal', minimum_nominal_constraint_error)
            raise minimum_nominal_constraint_error

        #cek apakah user pernah donasi ke organisasi terkait sebelumnya
        if (self.user.is_sponsor) :
            with connection.cursor() as cursor:
                cursor.execute("SELECT sponsor , organisasi FROM sponsor_organisasi WHERE organisasi = '%s' AND sponsor = '%s'" % (select_organisasi,self.user.email))
                list_donasi = cursor.fetchall()

            if (len(list_donasi) > 0) :
                sponsor_organisasi_integrity_error = ValidationError(_('Anda sudah pernah berdonasi ke organisasi tersebut'))
                self.add_error('select_organisasi', sponsor_organisasi_integrity_error)
                raise sponsor_organisasi_integrity_error 
        elif (self.user.is_donatur) :
            with connection.cursor() as cursor:
                cursor.execute("SELECT donatur, organisasi FROM donatur_organisasi WHERE organisasi = '%s' AND donatur = '%s'" % (select_organisasi,self.user.email))
                list_donasi = cursor.fetchall()

            if (len(list_donasi) > 0) :
                donatur_organisasi_integrity_error = ValidationError(_('Anda sudah pernah berdonasi ke organisasi tersebut'))
                self.add_error('select_organisasi', donatur_organisasi_integrity_error)
                raise donatur_organisasi_integrity_error 

        #cek apakah saldo donatur mencukupi
        if (self.user.is_donatur) :
            with connection.cursor() as cursor:
                cursor.execute("SELECT saldo FROM donatur WHERE email = '%s'" % (self.user.email))
                saldo = cursor.fetchall()[0][0] 
            if (saldo < nominal) :
                sufficient_balance_constraint_error = ValidationError('saldo anda berjumlah %i, tidak mencukupi untuk melakukan donasi' % (saldo))
                self.add_error('select_organisasi', sufficient_balance_constraint_error)
                raise sufficient_balance_constraint_error

        return cleaned_data