from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required

from .forms import DonasiOrganisasiForm
from SION.utils.helpers import create_insert_sql
from django.db import connection

import datetime

@login_required(login_url='/login/')
def organisasi_donasi_view(request):
    if(request.user.is_donatur or request.user.is_sponsor):
        form = DonasiOrganisasiForm()    

        if request.method == "POST":
            form = DonasiOrganisasiForm(request.POST, user=request.user)
            if form.is_valid():
                cleaned_data = form.cleaned_data
                email = cleaned_data['select_organisasi']
                nominal = cleaned_data['nominal']
                now = datetime.datetime.now();
                date = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
                user = request.user.email
                print(user)
                if (request.user.is_donatur) :
                    data = {'donatur':user,'organisasi':email,'tanggal':date,'nominal':nominal}
                    sql = create_insert_sql(table_name="donatur_organisasi",sponsor=user,organisasi=email,tanggal=date,nominal=nominal);
                    print(sql)
                elif (request.user.is_sponsor) :
                    data = {'sponsor':user,'organisasi':email,'tanggal':date,'nominal':nominal}
                    sql = create_insert_sql(table_name="sponsor_organisasi",sponsor=user,organisasi=email,tanggal=date,nominal=nominal);
                    print(sql)

                with connection.cursor() as cursor:
                    cursor.execute(sql)
        return render(request, 'donasi_organisasi.html', {'form': form})
    else :
        return render(request, 'cant_donate.html')
    
