from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views.decorators.http import require_http_methods

from django.http import JsonResponse

from SION.auth.forms import LoginForm
from SION.models import UserSion

@require_http_methods(["GET", "POST"])
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            query_set = UserSion.objects.raw("SELECT * FROM %s WHERE email ='%s';" % (
                UserSion._meta.db_table, email))
            user = query_set[0]
            login(request, user)
            return redirect('/profile')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


@login_required(login_url='/login/')
def logout_view(request):
    logout(request)
    return render(request, 'logout.html')


def check_view(request):
    data = {
        'login': False,
        'is_sponsor': False,
        'is_donatur': False,
        'is_relawan': False,
    }

    if request.user.is_authenticated:
        data['login'] = True
        data['is_sponsor'] = request.user.is_sponsor
        data['is_donatur'] = request.user.is_donatur
        data['is_relawan'] = request.user.is_relawan

    return JsonResponse(data)


