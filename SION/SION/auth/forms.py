from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import (
    check_password,
)

from SION.models import UserSion


class LoginForm(forms.Form):
    email = forms.EmailField(max_length=50, widget=forms.EmailInput(attrs={'class': 'validate'}))
    password = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={'class': 'validate'}))

    def clean(self):
        super().clean()

        cleaned_data = self.cleaned_data
        email, password = cleaned_data['email'], cleaned_data['password']

        is_user_with_this_email_exist_sql = "SELECT * FROM %s WHERE email = '%s';" % (
            UserSion._meta.db_table, email)
        query_set = UserSion.objects.raw(is_user_with_this_email_exist_sql)
        if len(list(query_set)) == 0:
            email_does_not_exist_error = ValidationError(_("Couldn't find your account."))
            self.add_error('email', email_does_not_exist_error)
            raise email_does_not_exist_error

        user = query_set[0]
        if not check_password(password, user.password):
            wrong_password_error = ValidationError(_("Wrong password."))
            self.add_error('password', wrong_password_error)
            raise wrong_password_error

        return cleaned_data
