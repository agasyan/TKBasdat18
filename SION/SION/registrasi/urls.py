from django.urls import path

from . import views

urlpatterns = [
    path('registrasi/donatur/', views.registrasi_donatur),
    path('registrasi/relawan/', views.registrasi_relawan),
    path('registrasi/sponsor/', views.registrasi_sponsor),
    path('organisasi/registrasi/', views.registrasi_organisasi),
]

