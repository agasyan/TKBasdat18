from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from .forms import RegistrasiUserForm
from .forms import RegistrasiRelawanForm
from .forms import RegistrasiSponsorForm
from .forms import RegistrasiOrganisasiForm

from SION.models import UserManager
from SION.models import OrganisasiManager


@require_http_methods(["GET", "POST"])
def registrasi_donatur(request):
    if request.method == "POST":
        form = RegistrasiUserForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data

            user = UserManager().create_donatur(**cleaned_data)

            if request.user.is_authenticated:
                logout(request)

            login(request, user)

            return redirect('/profile')

    else:
        form = RegistrasiUserForm()

    return render(request, 'registrasi_donatur.html', {'form': form})


@require_http_methods(["GET", "POST"])
def registrasi_relawan(request):
    if request.method == "POST":
        form = RegistrasiRelawanForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            data = {
                **cleaned_data,
                'keahlians': form.get_keahlians(),
            }
            user = UserManager().create_relawan(**data)

            if request.user.is_authenticated:
                logout(request)

            login(request, user)

            return redirect('/profile')
    else:
        form = RegistrasiRelawanForm()

    return render(request, 'registrasi_relawan.html', {'form': form})


@require_http_methods(["GET", "POST"])
def registrasi_sponsor(request):
    if request.method == "POST":
        form = RegistrasiSponsorForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            UserManager().create_sponsor(**cleaned_data)
            user = authenticate(username=cleaned_data['email'], password=cleaned_data['password'])

            if user is not None:
                if request.user.is_authenticated:
                    logout(request)

                login(request, user)

                return redirect('/profile')

    else:
        form = RegistrasiSponsorForm()

    return render(request, 'registrasi_sponsor.html', {'form': form})


@require_http_methods(["GET", "POST"])
def registrasi_organisasi(request):
    form = {}
    if request.method == "POST":
        form = RegistrasiOrganisasiForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data

            pengurus_data = form.get_pengurus()

            organisasi_data = {}

            for key in cleaned_data:
                if key not in pengurus_data:
                    organisasi_data[key] = cleaned_data.get(key)

            organisasi = OrganisasiManager().create_organisasi(**organisasi_data)

            pengurus_credentials = []
            for key, pengurus in pengurus_data.items():
                credential = UserManager().create_pengurus(organisasi=organisasi_data['email_organisasi'], **pengurus)
                pengurus_credentials.append(credential)

            return render(request, 'after_registrasi_organisasi.html', {'organisasi': organisasi, 'pengurus_credentials': pengurus_credentials})
        else:
            print('not valid')
    else:
        form = RegistrasiOrganisasiForm()

    print(form)
    return render(request, 'registrasi_organisasi.html', {'form': form})

