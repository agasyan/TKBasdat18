from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


from SION.models import UserSion
from SION.models import Organisasi


class RegistrasiUserForm(forms.Form):
    nama = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'nama'}),
        label='Nama',
    )
    email = forms.EmailField(
        max_length=50,
        widget=forms.EmailInput(attrs={'class': 'validate', 'placeholder': 'email'}),
        label='Email'
    )
    password = forms.CharField(
        max_length=50,
        widget=forms.PasswordInput(attrs={'class': 'validate', 'placeholder': 'password'}),
        label='Password'
    )
    confirm_password = forms.CharField(
        max_length=50,
        widget=forms.PasswordInput(attrs={'class': 'validate', 'placeholder': 'confirm_password'}),
        label='Confirm Password'
    )
    alamat = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'jalan/desa/kelurahan/no.rumah'}),
        label='Alamat'
    )
    kecamatan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'kecamatan'}),
        label='Kecamatan'
    )
    kabupaten = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'kabupaten'}),
        label='Kabupaten'
    )
    provinsi = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'provinsi'}),
        label='Provinsi'
    )
    kodepos = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'kodepos'}),
        label='Kodepos'
    )

    def clean(self):
        super().clean()

        cleaned_data = self.cleaned_data

        password, confirm_password = cleaned_data.get('password'), cleaned_data.get('confirm_password')

        if password != confirm_password:
            confirm_password_error = ValidationError(_("Passwords didn't match. Please try again."))
            self.add_error('confirm_password', confirm_password_error)
            raise confirm_password_error

        email = self.cleaned_data['email']

        is_email_already_exist_sql = "SELECT * FROM %s WHERE email = '%s'" % (UserSion._meta.db_table, email)

        if len(list(UserSion.objects.raw(is_email_already_exist_sql))) > 0:
            unique_email_constraint_error = ValidationError(_('User with that e-mail address already exists.'))
            self.add_error('email', unique_email_constraint_error)
            raise unique_email_constraint_error

        alamat_lengkap_keys = ['alamat', 'kecamatan', 'kabupaten', 'provinsi', 'kodepos']

        cleaned_data['alamat_lengkap'] = ", ".join([cleaned_data[key] for key in alamat_lengkap_keys])

        for key in alamat_lengkap_keys:
            del cleaned_data[key]
        del cleaned_data['confirm_password']

        return cleaned_data


class RegistrasiRelawanForm(RegistrasiUserForm):
    no_hp = forms.CharField(
        max_length=20,
        widget=forms.TextInput(attrs={
            'class': 'validate',
            'pattern': "^08[0-9]{9,}$",
            'placeholder': 'no hp',
        })
    )
    tanggal_lahir = forms.DateField(
        widget=forms.DateInput(attrs={'class':'validate', 'type': 'date', 'placeholder': 'tanggal lahir'})
    )

    def get_keahlians(self):
        keahlians = {}
        for key in self.data:
            if key.startswith("keahlian"):
                keahlians[key] = self.data.get(key)

        return keahlians


class RegistrasiSponsorForm(RegistrasiUserForm):
    logo_sponsor = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class': 'validate',
            'pattern': '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})',
            'placeholder': 'logo sponsor image url (i.e: https://www.imgur.com/ )',
            'label': 'Logo Sponsor',
        })
    )

class RegistrasiOrganisasiForm(forms.Form):
    nama = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Nama Organisasi'}),
        label='Nama Organisasi',
    )
    website = forms.CharField(
        max_length=50,
        widget=forms.URLInput(attrs={
            'class': 'validate',
            'placeholder': 'http://www.website.com atau https://www.website.com',
            'pattern': '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})',
        }),
        label='Website'
    )
    email_organisasi = forms.EmailField(
        max_length=50,
        widget=forms.EmailInput(attrs={'class': 'validate', 'placeholder': 'email@email.com'}),
        label='Email Organisasi'
    )
    alamat = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Jalan/Desa/Kelurahan/No.rumah'}),
        label='Alamat Organisasi'
    )
    kelurahan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Kelurahan'}),
        label='Kelurahan'
    )
    kecamatan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Kecamatan'}),
        label='Kecamatan'
    )
    kabupaten_kota = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Kabupaten/Kota'}),
        label='Kabupaten'
    )
    provinsi = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Provinsi'}),
        label='Provinsi'
    )
    kode_pos = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Kodepos'}),
        label='Kodepos'
    )
    tujuan = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate', 'placeholder': 'Visi/Misi Organisasi'}),
        label='Tujuan Organisasi'
    )


    def clean(self):
        super().clean()

        cleaned_data = self.cleaned_data

        email_organisasi = cleaned_data['email_organisasi']


        is_email_already_exist_sql = "SELECT * FROM %s WHERE email_organisasi = '%s'" % (Organisasi._meta.db_table, email_organisasi)

        if len(list(Organisasi.objects.raw(is_email_already_exist_sql))) > 0:
            unique_email_constraint_error = ValidationError(_('Organization with that e-mail address already exists.'))
            self.add_error('email_organisasi', unique_email_constraint_error)


        pengurus = self.get_pengurus()
        for key, val in pengurus.items():
            email = val['email']
            if len(list(UserSion.objects.raw("SELECT * FROM %s WHERE email = '%s'" % (UserSion._meta.db_table, email)))) > 0:
                unique_email_constraint_error = ValidationError(
                    _('E-mail address for %s already exists.' % key))
                self.add_error(None, unique_email_constraint_error)
            nama = val['nama']
            if len(nama) > 100:
                length_error = ValidationError(_('Name for %s exceed 10 characters.' % key))
                self.add_error(None, length_error)

        return cleaned_data

    def get_pengurus(self):
        keahlians = {}
        for key in self.data:
            if key.startswith("pengurus"):
                keahlians[key] = self.data.get(key)

        grouped_keahlian = {}
        for key in keahlians:
            idx = key[key.rfind('_')+1:]
            print(idx)
            new_key = 'pengurus_' + idx
            if new_key in grouped_keahlian:
                continue
            grouped_keahlian[new_key] = {
                'email': keahlians.get('pengurus_email_'+idx),
                'nama': keahlians.get('pengurus_nama_'+idx),
                'alamat_lengkap': keahlians.get('pengurus_alamat_'+idx),
            }
        print(grouped_keahlian)
        return grouped_keahlian