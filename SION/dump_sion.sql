--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3 (Ubuntu 10.3-1.pgdg16.04+1)
-- Dumped by pg_dump version 10.3 (Ubuntu 10.3-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: berita; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.berita (
    kode_unik character varying(50) NOT NULL,
    kegiatan character varying(50) NOT NULL,
    judul character varying(100) NOT NULL,
    deskripsi text NOT NULL,
    tgl_update date NOT NULL,
    tgl_kegiatan date NOT NULL
);


ALTER TABLE public.berita OWNER TO adalberht;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id character varying(254) NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: donatur; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.donatur (
    email character varying(50) NOT NULL,
    saldo integer NOT NULL
);


ALTER TABLE public.donatur OWNER TO adalberht;

--
-- Name: donatur_kegiatan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.donatur_kegiatan (
    donatur character varying(50) NOT NULL,
    kegiatan character varying(50) NOT NULL,
    tanggal date NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE public.donatur_kegiatan OWNER TO adalberht;

--
-- Name: donatur_organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.donatur_organisasi (
    donatur character varying(50) NOT NULL,
    organisasi character varying(50) NOT NULL,
    tanggal date NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE public.donatur_organisasi OWNER TO adalberht;

--
-- Name: kategori; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.kategori (
    kode character varying(50) NOT NULL,
    nama character varying(20) NOT NULL
);


ALTER TABLE public.kategori OWNER TO adalberht;

--
-- Name: kategori_kegiatan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.kategori_kegiatan (
    kode_kegiatan character varying(50) NOT NULL,
    kode_kategori character varying(20) NOT NULL
);


ALTER TABLE public.kategori_kegiatan OWNER TO adalberht;

--
-- Name: keahlian_relawan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.keahlian_relawan (
    email character varying(50) NOT NULL,
    keahlian character varying(50) NOT NULL
);


ALTER TABLE public.keahlian_relawan OWNER TO adalberht;

--
-- Name: kegiatan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.kegiatan (
    kode_unik character varying(20) NOT NULL,
    organisasi_perancang character varying(50) NOT NULL,
    judul character varying(50) NOT NULL,
    dana_dibutuhkan integer NOT NULL,
    tanggal_mulai date NOT NULL,
    tanggal_selesai date NOT NULL,
    deskripsi text NOT NULL
);


ALTER TABLE public.kegiatan OWNER TO adalberht;

--
-- Name: laporan_keuangan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.laporan_keuangan (
    organisasi character varying(50) NOT NULL,
    tgl_dibuat date NOT NULL,
    total_pemasukan integer NOT NULL,
    total_pengeluaran integer NOT NULL,
    is_disetujui boolean NOT NULL,
    rincian_pemasukan text NOT NULL
);


ALTER TABLE public.laporan_keuangan OWNER TO adalberht;

--
-- Name: organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.organisasi (
    email_organisasi character varying(50) NOT NULL,
    website character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    provinsi character varying(50) NOT NULL,
    kabupaten_kota character varying(50) NOT NULL,
    kecamatan character varying(50) NOT NULL,
    kelurahan character varying(50) NOT NULL,
    kode_pos character varying(50) NOT NULL,
    status_verifikasi character varying(50) NOT NULL
);


ALTER TABLE public.organisasi OWNER TO adalberht;

--
-- Name: organisasi_terverifikasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.organisasi_terverifikasi (
    email_organisasi character varying(50) NOT NULL,
    nomor_registrasi character varying(50) NOT NULL,
    status_aktif character varying(50) NOT NULL
);


ALTER TABLE public.organisasi_terverifikasi OWNER TO adalberht;

--
-- Name: pengurus_organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.pengurus_organisasi (
    email character varying(50) NOT NULL,
    organisasi character varying(50) NOT NULL
);


ALTER TABLE public.pengurus_organisasi OWNER TO adalberht;

--
-- Name: penilaian_performa; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.penilaian_performa (
    email_relawan character varying(50) NOT NULL,
    organisasi character varying(50) NOT NULL,
    id_number integer NOT NULL,
    tgl_penilaian date NOT NULL,
    deskripsi text NOT NULL,
    nilai_skala integer NOT NULL
);


ALTER TABLE public.penilaian_performa OWNER TO adalberht;

--
-- Name: relawan; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.relawan (
    email character varying(50) NOT NULL,
    no_hp character varying(20) NOT NULL,
    tanggal_lahir date NOT NULL
);


ALTER TABLE public.relawan OWNER TO adalberht;

--
-- Name: relawan_organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.relawan_organisasi (
    email_relawan character varying(50) NOT NULL,
    organisasi character varying(50) NOT NULL
);


ALTER TABLE public.relawan_organisasi OWNER TO adalberht;

--
-- Name: reward; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.reward (
    kode_kegiatan character varying(50) NOT NULL,
    barang_reward character varying(20) NOT NULL,
    harga_min integer NOT NULL,
    harga_max integer NOT NULL
);


ALTER TABLE public.reward OWNER TO adalberht;

--
-- Name: sponsor; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.sponsor (
    email character varying(50) NOT NULL,
    logo_sponsor character varying(100) NOT NULL
);


ALTER TABLE public.sponsor OWNER TO adalberht;

--
-- Name: sponsor_organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.sponsor_organisasi (
    sponsor character varying(50) NOT NULL,
    organisasi character varying(50) NOT NULL,
    tanggal date NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE public.sponsor_organisasi OWNER TO adalberht;

--
-- Name: tujuan_organisasi; Type: TABLE; Schema: public; Owner: adalberht
--

CREATE TABLE public.tujuan_organisasi (
    organisasi character varying(50) NOT NULL,
    tujuan text NOT NULL
);


ALTER TABLE public.tujuan_organisasi OWNER TO adalberht;

--
-- Name: user_sion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_sion (
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    email character varying(254) NOT NULL,
    nama character varying(100) NOT NULL,
    alamat_lengkap text NOT NULL,
    is_superuser boolean DEFAULT false
);


ALTER TABLE public.user_sion OWNER TO postgres;

--
-- Name: user_sion_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_sion_groups (
    id integer NOT NULL,
    usersion_id character varying(254) NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.user_sion_groups OWNER TO postgres;

--
-- Name: user_sion_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_sion_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sion_groups_id_seq OWNER TO postgres;

--
-- Name: user_sion_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_sion_groups_id_seq OWNED BY public.user_sion_groups.id;


--
-- Name: user_sion_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_sion_user_permissions (
    id integer NOT NULL,
    usersion_id character varying(254) NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.user_sion_user_permissions OWNER TO postgres;

--
-- Name: user_sion_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_sion_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sion_user_permissions_id_seq OWNER TO postgres;

--
-- Name: user_sion_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_sion_user_permissions_id_seq OWNED BY public.user_sion_user_permissions.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: user_sion_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_groups ALTER COLUMN id SET DEFAULT nextval('public.user_sion_groups_id_seq'::regclass);


--
-- Name: user_sion_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.user_sion_user_permissions_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add berita	6	add_berita
17	Can change berita	6	change_berita
18	Can delete berita	6	delete_berita
19	Can add donatur kegiatan	7	add_donaturkegiatan
20	Can change donatur kegiatan	7	change_donaturkegiatan
21	Can delete donatur kegiatan	7	delete_donaturkegiatan
22	Can add donatur organisasi	8	add_donaturorganisasi
23	Can change donatur organisasi	8	change_donaturorganisasi
24	Can delete donatur organisasi	8	delete_donaturorganisasi
25	Can add kategori	9	add_kategori
26	Can change kategori	9	change_kategori
27	Can delete kategori	9	delete_kategori
28	Can add kategori kegiatan	10	add_kategorikegiatan
29	Can change kategori kegiatan	10	change_kategorikegiatan
30	Can delete kategori kegiatan	10	delete_kategorikegiatan
31	Can add keahlian relawan	11	add_keahlianrelawan
32	Can change keahlian relawan	11	change_keahlianrelawan
33	Can delete keahlian relawan	11	delete_keahlianrelawan
34	Can add kegiatan	12	add_kegiatan
35	Can change kegiatan	12	change_kegiatan
36	Can delete kegiatan	12	delete_kegiatan
37	Can add laporan keuangan	13	add_laporankeuangan
38	Can change laporan keuangan	13	change_laporankeuangan
39	Can delete laporan keuangan	13	delete_laporankeuangan
40	Can add organisasi	14	add_organisasi
41	Can change organisasi	14	change_organisasi
42	Can delete organisasi	14	delete_organisasi
43	Can add organisasi terverifikasi	15	add_organisasiterverifikasi
44	Can change organisasi terverifikasi	15	change_organisasiterverifikasi
45	Can delete organisasi terverifikasi	15	delete_organisasiterverifikasi
46	Can add pengurus organisasi	16	add_pengurusorganisasi
47	Can change pengurus organisasi	16	change_pengurusorganisasi
48	Can delete pengurus organisasi	16	delete_pengurusorganisasi
49	Can add penilaian performa	17	add_penilaianperforma
50	Can change penilaian performa	17	change_penilaianperforma
51	Can delete penilaian performa	17	delete_penilaianperforma
52	Can add relawan organisasi	18	add_relawanorganisasi
53	Can change relawan organisasi	18	change_relawanorganisasi
54	Can delete relawan organisasi	18	delete_relawanorganisasi
55	Can add reward	19	add_reward
56	Can change reward	19	change_reward
57	Can delete reward	19	delete_reward
58	Can add sponsor organisasi	20	add_sponsororganisasi
59	Can change sponsor organisasi	20	change_sponsororganisasi
60	Can delete sponsor organisasi	20	delete_sponsororganisasi
61	Can add tujuan organisasi	21	add_tujuanorganisasi
62	Can change tujuan organisasi	21	change_tujuanorganisasi
63	Can delete tujuan organisasi	21	delete_tujuanorganisasi
64	Can add user sion	22	add_usersion
65	Can change user sion	22	change_usersion
66	Can delete user sion	22	delete_usersion
67	Can add donatur	23	add_donatur
68	Can change donatur	23	change_donatur
69	Can delete donatur	23	delete_donatur
70	Can add relawan	24	add_relawan
71	Can change relawan	24	change_relawan
72	Can delete relawan	24	delete_relawan
73	Can add sponsor	25	add_sponsor
74	Can change sponsor	25	change_sponsor
75	Can delete sponsor	25	delete_sponsor
\.


--
-- Data for Name: berita; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.berita (kode_unik, kegiatan, judul, deskripsi, tgl_update, tgl_kegiatan) FROM stdin;
AxzcSEQZYcIjlGNtCdCj	EifCvBdXqIjfDttGsQhp	Nice Her Return Here	Begin movement character past always this hard. Instead while she the arm shoulder thousand mission.	2017-03-31	2017-07-31
DvfhJZuwfhEzGfQhBymg	poKStuXWZIWUhbxusAsU	Consider Cultural Thought Free	Suddenly property begin important throughout. True arrive expect heavy. Surface stuff decade push find live.	2016-06-14	2018-04-24
lOiBMLMwzGEixVTIoFIJ	zFIwxgogWVIEygrsuXip	Reality	Pick woman through up prepare.	2017-07-25	2017-09-25
oumogtycSnfrZzWLTAeZ	emgKjuWWpMpnwPKxUUfG	Message Store List Indicate Big	Boy night former catch western activity. Give major thousand change improve. Rather sell them accept sign business community.	2012-04-24	2016-01-06
ApbeyXUTWzbVNPmJUiiH	iafJQkAFrLKbbjZQgAhI	Series Use	Blue ok city laugh. Task others discussion according news thousand father. Rich number claim people particular step.	2011-10-21	2014-05-07
GuWlGJWmgnQWxlyLprgx	rfrRTllOUXAUBvlkYjUO	Catch Age North Middle	Age article man nation. Security training why none tree authority must.	2017-07-06	2017-08-18
FDHWoRwkeGIQBLTaziHy	ZfvIiVCmXDaepksydTzb	Response Half Base Recent Red	At head fall return own. Ball eight public return point choose weight.	2011-06-09	2012-03-01
zWdfDggOGJoblLptuXwd	dRNPtJCOtgctMNOJxQDl	Buy	Old thank family smile. One key watch talk adult per.	2015-06-24	2015-10-18
vHZpcewvhyBlShzOhPzD	gVxEOWXRhdDoqIQYenTe	Somebody	Catch college new tough you discover rise. Wife current various sit.	2015-01-17	2016-10-08
OYyDzHkJARZVrFddrpaT	lhjzGExcbZuijkFrpqik	Decide Operation Site	Different natural above hand campaign candidate center. Be return chance fill step activity term.	2017-10-26	2018-03-22
HjHqCZKEgAswHVvugVIh	eSYblbJKmdKvFeuWqngl	Begin You Skin	Concern appear model others. Term design where activity. Player notice decision. Gas more society prevent arrive pick reveal factor.	2015-04-01	2018-01-15
ySeNKgKGIkespYKBvTmh	jVDWFkZPrjyqOGMrzLfg	Television	Fund Congress line mean.	2011-11-04	2013-04-04
rDcWmDFbuLisyWerJmKT	VSXQtRtdhDqbFPmxcFGe	By Open Question Rise	Research daughter as artist huge education.	2014-01-01	2016-01-08
xycntytpvkLLVJpDiTOS	RqgeLdoTPECfjhPKcAUC	Take Moment	Change window real half garden. Eight claim guy current glass trouble guy.	2012-06-19	2013-04-03
ypHKaDNiCgfylVVdaTtx	ASWHnAPiVkHxGxUguORE	Arrive Kind Page Agent Course	Stop PM base notice. Arrive bill country conference mission. Financial also car behind.	2016-11-27	2017-03-11
BzxIPBKLnhYKRonzASih	AArIhcRgghqWLjVrmrac	Method Evidence Billion Anything	Rich hour trip together country.	2011-03-28	2011-06-13
YKCIAAeBDhBViLbKgXmA	nvuSJTMZbyGQdlMHNDeT	Quickly Different Room Recently Speak	Morning move anything represent another. Give traditional I lead.	2010-06-01	2015-11-06
TZMKwbBiGVyaOALRJUaN	GINuFEMVNpewCOPlFAgT	Job Price Experience	Official movie follow take along quite.	2012-07-06	2015-03-17
mwlDPZzZZqrSIOgGcZzB	qKDKHFASHwVwauTIEkUy	Young Staff Forget Everyone Wrong	National challenge manager consider window before. Can usually tree summer resource eight and.	2010-08-25	2012-01-16
EOvNXAXMsnQdRZBafvaV	aBodfVzEbxAXHdMZHRoi	To Common Successful	Contain ever this crime traditional station left. Either senior miss wait like carry response. Heavy commercial cost prevent position major population often.	2013-05-31	2018-04-23
DhLglrklQcBKijUbuXPQ	wrtadwPAbxGcHNdfODnB	Alone Television Center Simple	Table clearly worry. Probably could seven reality. Yet news exactly whatever choice reality play.	2015-01-09	2017-10-16
TDYOkePcHKTRlcgLgzXg	ZVBXKUtGYNoYXuwTDDSG	Well Law Garden Week	Apply black central war evening. Wife smile institution purpose alone sport.	2011-09-29	2017-04-30
LsqHHUzLQcmIgAEcvuCd	RBVkWthdOwiisPhiKlcK	Performance	Reflect brother about travel. Law sense past none long small. Perhaps finish after.	2015-08-07	2018-03-31
tYWXPAzAAZUNMgApqhWS	hocjMsgREeENhDiGCXSj	Raise Political Scene Pm	Lead our job beautiful claim church month. Increase some kitchen. Need treat participant laugh grow writer.	2017-07-20	2018-02-22
yUCkpKoalkAkKaeoGSyi	KFVCCXAuefEDZpFkwFWP	Share Without	Voice change offer window light culture. Pass send note quickly.	2010-12-10	2013-04-03
FedKZEYmWQHxPeSkVoTH	XVrlsKyKVrHtiZuRNJXc	End Often	According past road choose son north. Peace notice once deal say. Order later put yourself follow last thank.	2014-04-26	2015-05-06
unilnkCawlXbulzYJLsy	NpJFXNUbWryizCLyswMH	Everything Them	Something industry admit each fast serious three. Public most white understand return soon. Organization heavy white party approach law.	2016-04-11	2017-04-01
acmStHlfwoQeLeVdsTfE	yhKWyBHzFLCttDhcxgSY	Able During Though Most	Gun score assume. Upon way general. Sing analysis live they change.	2012-06-13	2014-07-08
ljSrnXcdlePcuhvPpQfz	EQrhBUUZPfgeOUHgCDAF	Book Agree Heart	Mr let class soldier. Ready one this improve thought room anything.	2015-02-09	2016-01-12
pVoOuJXXeCJHRVjNFHmj	RkfiaZdeZXLsPjuDWdlx	Degree	Year art service tree stay owner possible. Store hour impact task interest song. Science consumer care.	2010-08-22	2018-02-07
laojwLhRCHrEVacAnsOI	OKcRTVAZJSZALvMDYJfT	Capital Few Knowledge Challenge	When since baby give focus me rest. Safe game skin read. Base need writer performance several.	2016-02-26	2016-03-29
vBphkjvgtPxtElWUnMtN	cFtrmZuHgmGWGtsWMsFY	Agreement	Until prevent practice responsibility all. Rock like can bit politics toward. Newspaper computer chance foreign enjoy.	2013-11-21	2016-11-07
oZxlYeroGRlckamOIhCU	zmryMgzDUrkXLQAVOJNo	Score	Amount understand challenge million couple law. Bank such feeling focus. Five available write understand get word.	2013-06-01	2013-07-20
IRnqWMRSSuIupcfGCbag	KFVCCXAuefEDZpFkwFWP	Investment	Everybody police tend. Hotel when forget look camera chance region. Much down energy police lay stuff occur shake.	2017-11-16	2017-11-17
UMxVGNbKuJmliGCWMivz	ddrAQiTwNnYZcEWDlryS	Example Game National	Adult finally deep new school career. Six true finish short role knowledge.	2010-10-17	2012-01-09
kxbukQbeoUAdFvVmnTgd	awBdmbqIYpIaQlEePKXG	Onto Money	Read majority bad data amount. Rate if really find article without.	2013-04-20	2017-04-16
FScTFZpWAQzzamDZkMyA	eSYblbJKmdKvFeuWqngl	Fish Officer You Join	Some check change agreement director issue. Any even institution. Pay say today forget fine reason important.	2013-08-11	2015-03-10
AnaJkiJOiiwhryTnShnK	eysmJIgOOYiDBLFiuEoh	Spring Table Fund	Responsibility consider war style through. Arm that billion set. Listen only interesting impact.	2014-09-17	2015-07-07
ygnzwiWPNKFYraZAGrXB	RBVkWthdOwiisPhiKlcK	Where	Order board middle together common focus effort. Above morning their. Democratic entire wrong clear by.	2011-02-27	2011-03-01
uQlxQBPwXPeTRFgcbsZo	bOneUsYxBlcApICCJacC	Determine Account Hope Not	White item reflect thought chair. Girl southern security question free catch weight action. Free upon ok report usually fire.	2012-09-05	2018-02-03
ijWQJqYUqEABGNtBJJry	VfzzZfxDkOzMWpIilYka	Wide Experience Artist Clearly Former	Eight its great player wind surface. Possible figure property region right. Agency Democrat alone officer keep indicate.	2013-09-21	2018-02-13
JKRkUJrUXGheULrYmEVq	mWtLAnLHbUdgYFnWfpbN	But Player Simple Issue	Yes identify issue degree perform matter design. Since move pull compare nation positive. Type common accept.	2011-09-22	2017-12-23
mxkgNEvznftjSFjkNhRC	mNEykxgODYNHrIQtViTj	Finally Future	Still serious teacher price. Space international far.	2014-07-13	2018-03-24
bIUPvAhMTcEqDISnMHWd	oCJijwMmNJpWDQpIcXUQ	Join Own International	Best yet garden throw. Change kid entire bed. If out interesting staff wide decide firm role.	2013-05-10	2017-05-06
apdIJbkHGerBbbcfTVZh	FshaNbwntNiSADIljAIc	Miss North	Deal other tell develop keep ahead newspaper.	2015-11-30	2016-09-26
lRVGFGyxmDujMlNtOHyy	YrauzBBSoECHPXPabTgz	Score Rather Always Top Hard	Key space above trial door. East oil evening staff plant. Own company heart common.	2013-08-09	2015-03-23
WFNajJnvGuJXQPiIFqBy	JjRVStNcmGrhnYuRJmhn	Few Officer Feel	Rest year since care economic street difficult budget. Say move try face. Guess position hundred cultural itself forget public recognize.	2012-09-18	2016-09-28
wsKvYlCruBDLzurQtmSo	qKDKHFASHwVwauTIEkUy	Control	Test time so standard another cost message win.	2018-04-19	2018-04-22
sETlYsQGntrKPyEgjJxP	KFVCCXAuefEDZpFkwFWP	Everything Goal Present Prepare Seek	Much late industry education short baby. Table front tough find blue.	2017-05-28	2017-06-25
KOBzwvVGtIzmcDoyaYsn	cFtrmZuHgmGWGtsWMsFY	Push Daughter Case	We official hair suddenly federal can. Think deal word blood without. Treatment trip only major.	2015-08-23	2016-12-27
grflASqddRNtIovCppNq	FNfawvTZbuvWFtfrRbGI	Allow Rise Young	From later sure effort avoid. Picture model society responsibility get.	2014-07-02	2015-02-16
oGOUXkwaUvOGeoYnYwbR	gVxEOWXRhdDoqIQYenTe	Career Money West	Lawyer begin upon without cut. East police home simple. Choose ever realize eight own.	2017-09-28	2018-01-15
BsLhevIiMppGeGRikjsn	awBdmbqIYpIaQlEePKXG	Item Tree Yet State	Better understand matter task.	2014-10-14	2018-02-11
qWktvdKYCnSmWqUMJzem	ZBiiLFBoYEuMUdfetGzg	Discover Accept	Choice town statement civil blood. Actually election measure. Call church green fall seat.	2013-01-02	2013-04-16
XFyhoAHDjeqjiyiWlmQP	yYxLrFNUbxFFkCYSmRNb	Management	Wonder onto theory defense here local. Language safe yourself technology. Mouth poor form natural. Behind democratic social region.	2012-09-27	2013-06-25
IkxygrjcBTqWZXBxBhQt	VSXQtRtdhDqbFPmxcFGe	Create Success Local	Show future may offer community my large.	2012-02-20	2015-08-15
dduTrKrGNofkEBsTYHFc	NZIVtgisIVXKyBSzrohq	International Impact	Off type fill her close. Claim mention throw foreign.	2014-04-27	2016-08-14
DpfOjVZNspIxUbfuaeYd	eFLyOHiKXBRQYGysPGCv	Out Use Many	Similar admit house door commercial how evening. Almost whatever size second through kind summer debate.	2012-11-25	2017-05-18
nQrvgjnVIBBQBetXJFIn	ARIKBCBjjCFAhDzTijIe	Someone Save Term Night Light	Let point list standard break argue. Today visit ask. Fast example Mr size bring impact that.	2015-01-31	2017-04-04
diObouuoTjCMWjRWmKyj	vjtQXDqDbzrxbYZhgjPY	Want Low Later Along	Born these hear foot offer late. Pretty might me carry.	2012-04-05	2012-07-22
CkcpdmIwMpSCuCCjoYSK	QRdLNDdVxImFHwPkJEmf	Realize Best Executive Much	Condition past threat heavy.	2010-09-15	2015-08-23
wwXapjLzkxbuAEdcTDfb	dzEcdqljrzjAMVoNMQmg	Me	Pick because company leave debate. Begin actually pay network exist school. Current investment arm its four.	2016-11-22	2018-02-17
zEgbmVZyYSHeuHAbKDsQ	FshaNbwntNiSADIljAIc	Effect Figure	Ball play develop bring. Increase education discover important approach production dark.	2017-12-02	2018-01-15
tKPhAOtWuPqLFxFErSnp	NZIVtgisIVXKyBSzrohq	Image Detail Result	Various management term significant century natural some. Doctor test produce road speech season realize. Budget full blue quality create. List throw without small.	2015-07-04	2017-10-09
JemhDELXfqaRbXiObSWq	vjtQXDqDbzrxbYZhgjPY	Sea	Feeling simply candidate compare quite yeah business professional. Make despite step ten dog bit. Stop year perform research those.	2017-02-23	2017-04-01
AHHfAtLBqwFyQelCySmZ	fIIqYUaLxrtxLTsfsHJa	Specific Let Draw Movie	Tv short popular off. Practice thus receive possible but.	2010-07-19	2015-01-24
HbRXrvxXAzVcVPyuxuTd	NpJFXNUbWryizCLyswMH	Page Which	Begin beyond experience candidate mother action dream security. Week item home.	2014-02-17	2017-05-28
niwmLwLEmQvvVTJTYcfU	gyuUMnTTTpISAKrOOENR	Very Field Serious Every	Thing agree lot price campaign responsibility dinner.	2010-09-03	2017-02-22
hLZeniLTwgxlpQpHqKkC	HPvCYLzMyBJwsdpAWKRx	Scientist Condition	Or good bad young subject prove. Believe produce various second set far amount.	2014-04-20	2017-06-29
RBFVeqhhGzBosKxUpTRt	mvZxnKSSpCOvkdVLIudW	Why Other Role Hot	Law power energy tend but boy. When job open.	2011-01-18	2017-08-25
xFIDrlTNCjCoKptlwSkh	MbJWLbgtaeSBOJESSxyD	Support Relate Say Fact	Family station hand bed position fill white. Computer season politics protect TV.	2014-04-21	2014-05-13
iFWBKeelkQJIussymcqd	MTiRgCveqIdsTJOggVgu	Between	Hot cut throughout. Blue other shoulder nothing then plant PM. Prevent state whole sea itself father someone fund.	2016-08-16	2016-09-08
iCImKSguEopEFOqmlEnf	VSXQtRtdhDqbFPmxcFGe	Compare Like Law Partner	End scientist have. Performance soon man plant guess.	2011-05-31	2012-04-24
rDtGwWYDVmjirTMEoQlz	EQrhBUUZPfgeOUHgCDAF	Answer Without List	Draw respond prevent themselves. Local walk technology capital.	2011-03-29	2013-12-01
XsrRGTSUeVXoafUnvNvR	UFEOjVZpeCNcZnJlSKEB	Within	Top concern impact. Staff concern able role build while. Out very inside people nearly. Bill weight medical near.	2010-03-21	2017-03-06
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	SION	berita
7	SION	donaturkegiatan
8	SION	donaturorganisasi
9	SION	kategori
10	SION	kategorikegiatan
11	SION	keahlianrelawan
12	SION	kegiatan
13	SION	laporankeuangan
14	SION	organisasi
15	SION	organisasiterverifikasi
16	SION	pengurusorganisasi
17	SION	penilaianperforma
18	SION	relawanorganisasi
19	SION	reward
20	SION	sponsororganisasi
21	SION	tujuanorganisasi
22	SION	usersion
23	SION	donatur
24	SION	relawan
25	SION	sponsor
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-05-19 21:06:43.895012+07
2	contenttypes	0002_remove_content_type_name	2018-05-19 21:06:43.906805+07
3	auth	0001_initial	2018-05-19 21:06:43.987049+07
4	auth	0002_alter_permission_name_max_length	2018-05-19 21:06:44.009674+07
5	auth	0003_alter_user_email_max_length	2018-05-19 21:06:44.018552+07
6	auth	0004_alter_user_username_opts	2018-05-19 21:06:44.02711+07
7	auth	0005_alter_user_last_login_null	2018-05-19 21:06:44.035898+07
8	auth	0006_require_contenttypes_0002	2018-05-19 21:06:44.03848+07
9	auth	0007_alter_validators_add_error_messages	2018-05-19 21:06:44.048576+07
10	auth	0008_alter_user_username_max_length	2018-05-19 21:06:44.05749+07
11	auth	0009_alter_user_last_name_max_length	2018-05-19 21:06:44.066333+07
12	SION	0001_initial	2018-05-19 21:06:44.168148+07
13	admin	0001_initial	2018-05-19 21:06:44.221268+07
14	admin	0002_logentry_remove_auto_add	2018-05-19 21:06:44.234617+07
15	sessions	0001_initial	2018-05-19 21:06:44.251625+07
16	SION	0002_auto_20180519_1410	2018-05-19 21:14:38.592589+07
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: donatur; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.donatur (email, saldo) FROM stdin;
mbanburyp@friendfeed.com	1000000
blaydonq@ocn.ne.jp	1100000
jcalabryr@cmu.edu	1200000
dmcbays@berkeley.edu	1300000
kricciardiellot@google.com.au	1400000
evaissiereu@ibm.com	1500000
rwildashv@spotify.com	1600000
smaierw@ifeng.com	1700000
bcundyx@reverbnation.com	1800000
sdarleyy@admin.ch	1900000
aaharoniz@geocities.com	2000000
mwithnall10@youtube.com	2100000
cfraczak11@jugem.jp	2200000
mbernhardt12@tripadvisor.com	2300000
gfisher13@technorati.com	2400000
vmeriot14@wunderground.com	2500000
jplevin15@squidoo.com	2600000
oduffan16@yolasite.com	2700000
blaming17@devhub.com	2800000
jgounel18@tuttocitta.it	2900000
gbonson19@marketwatch.com	3000000
cjacob1a@webmd.com	3100000
vokennedy1b@i2i.jp	3200000
dcardon1c@unicef.org	3300000
bpicot1d@businessinsider.com	3400000
donatur1@donatur.id	0
donatur2@donatur.id	0
\.


--
-- Data for Name: donatur_kegiatan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.donatur_kegiatan (donatur, kegiatan, tanggal, nominal) FROM stdin;
blaydonq@ocn.ne.jp	ZfvIiVCmXDaepksydTzb	2012-06-19	284247820
rwildashv@spotify.com	VGLFpidMrKtsVSOAJTaY	2011-10-03	1524555290
dmcbays@berkeley.edu	RBVkWthdOwiisPhiKlcK	2016-02-06	169569555
aaharoniz@geocities.com	RBVkWthdOwiisPhiKlcK	2015-12-30	494331136
dmcbays@berkeley.edu	mdktgIoiFAXvvUhzTCPo	2012-09-28	660111868
sdarleyy@admin.ch	EWNFPyThgKscblBKrtbk	2010-11-20	316739458
blaming17@devhub.com	zmryMgzDUrkXLQAVOJNo	2013-10-06	172296503
rwildashv@spotify.com	gmDzMQQowfVyowaCauRu	2010-07-27	358350939
jgounel18@tuttocitta.it	nJraaevGlLXsjXxWTbJF	2010-07-12	1028050470
evaissiereu@ibm.com	gmDzMQQowfVyowaCauRu	2010-05-02	1704349372
dmcbays@berkeley.edu	yBPuoCvOWuGpjoTkQWIL	2013-07-21	1352953443
vmeriot14@wunderground.com	QuBjdWwqeMnqUYlelbLo	2011-03-11	143517218
dcardon1c@unicef.org	poKStuXWZIWUhbxusAsU	2016-01-14	1875632380
vokennedy1b@i2i.jp	KWWEtlEaWmEyqCBKsuCp	2016-03-31	739403704
cfraczak11@jugem.jp	hocjMsgREeENhDiGCXSj	2011-09-28	391481139
blaming17@devhub.com	yRKJyIaonxlBZbISgtwS	2013-08-10	163816267
vmeriot14@wunderground.com	uhlDPzMQNuwCdbouTNcE	2010-06-16	1052834053
sdarleyy@admin.ch	fmcEEYHeprFKQFblGKTC	2016-10-26	1423777124
rwildashv@spotify.com	SOiupDYJvVVoGEwqHdJG	2012-12-05	1639913995
cjacob1a@webmd.com	SrtIaXnMVHsGLFpzFwia	2011-11-01	1589637422
dmcbays@berkeley.edu	SrtIaXnMVHsGLFpzFwia	2014-12-05	1042505275
dmcbays@berkeley.edu	CBLDIFjIBbrVNojdnYpB	2018-02-08	1317868718
evaissiereu@ibm.com	gkTWLFDyyFhAADWbEFPy	2014-03-19	1095105456
gfisher13@technorati.com	poKStuXWZIWUhbxusAsU	2014-09-19	1267485876
blaydonq@ocn.ne.jp	lhMuerIPadCePhpTeneg	2015-05-29	1609330219
mbernhardt12@tripadvisor.com	cMRkQHLqdnsuQtYXYWXk	2011-12-18	1456548628
mbanburyp@friendfeed.com	UJxVjdYlmwWUdtWHndSp	2018-01-01	1125082413
dcardon1c@unicef.org	UFEOjVZpeCNcZnJlSKEB	2010-01-13	1366238382
cjacob1a@webmd.com	QzDCkvPPPWqLeLtWbrai	2017-02-09	1970643313
jgounel18@tuttocitta.it	dRNPtJCOtgctMNOJxQDl	2014-06-21	274020068
vmeriot14@wunderground.com	jVDWFkZPrjyqOGMrzLfg	2016-03-13	229792233
sdarleyy@admin.ch	LMGzcEyRGmYYVpivJnhe	2014-01-05	1080965143
jplevin15@squidoo.com	lhjzGExcbZuijkFrpqik	2010-06-08	1780936752
gfisher13@technorati.com	awBdmbqIYpIaQlEePKXG	2016-09-24	1470289484
rwildashv@spotify.com	LMGzcEyRGmYYVpivJnhe	2013-07-29	1212133239
rwildashv@spotify.com	rViakiLlouaYvslYgHvC	2013-09-03	963258833
bcundyx@reverbnation.com	NcpjSfxNwODNRbSwKMvP	2013-06-03	834407482
aaharoniz@geocities.com	MTiRgCveqIdsTJOggVgu	2014-09-21	71724841
evaissiereu@ibm.com	rViakiLlouaYvslYgHvC	2017-02-04	449807491
jcalabryr@cmu.edu	MTjZCRbfFnHoLMvBYptg	2015-04-05	1332630127
gbonson19@marketwatch.com	DvyFjumsgrWqLhtXSXCR	2014-01-31	620673855
cjacob1a@webmd.com	KFVCCXAuefEDZpFkwFWP	2016-08-20	1211204209
mbanburyp@friendfeed.com	yBPuoCvOWuGpjoTkQWIL	2017-10-09	298869587
mbanburyp@friendfeed.com	cXHZxCfUsiATFYBLQpwz	2012-06-07	481751297
jplevin15@squidoo.com	cFtrmZuHgmGWGtsWMsFY	2010-10-16	1373175541
mbanburyp@friendfeed.com	poKStuXWZIWUhbxusAsU	2014-11-05	452350959
blaming17@devhub.com	HLareEiBLeTUnZXWEpIX	2017-07-20	1122266129
mbernhardt12@tripadvisor.com	mdktgIoiFAXvvUhzTCPo	2016-02-25	42518375
vokennedy1b@i2i.jp	YrauzBBSoECHPXPabTgz	2013-07-25	314077906
oduffan16@yolasite.com	jhtPekJETGwrNQnHLYan	2013-06-07	1805980039
\.


--
-- Data for Name: donatur_organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.donatur_organisasi (donatur, organisasi, tanggal, nominal) FROM stdin;
vmeriot14@wunderground.com	kendrapatterson@salinas.com	2017-02-05	110387377
mbanburyp@friendfeed.com	ronald25@fields.com	2010-03-05	1743013137
vmeriot14@wunderground.com	wterry@jones.org	2016-04-23	48672620
dmcbays@berkeley.edu	jennifermedina@edwards.com	2014-04-21	1505174980
jplevin15@squidoo.com	jenningselizabeth@sweeney.com	2010-11-24	1630804671
cjacob1a@webmd.com	lgarza@moreno-villarreal.com	2015-03-06	1877176212
evaissiereu@ibm.com	kevinsalinas@potts.net	2011-09-18	1633591443
gbonson19@marketwatch.com	lowetimothy@collier.biz	2014-10-11	1582753794
vmeriot14@wunderground.com	trananthony@brown.com	2014-03-23	1173868035
rwildashv@spotify.com	jenningselizabeth@sweeney.com	2015-09-10	1263081308
mbernhardt12@tripadvisor.com	kevinsalinas@potts.net	2011-03-09	951383614
vmeriot14@wunderground.com	douglashatfield@decker-woodard.com	2014-01-13	196784686
jplevin15@squidoo.com	marc24@lewis.org	2016-02-01	1387608855
bcundyx@reverbnation.com	wterry@jones.org	2012-12-16	923901051
jgounel18@tuttocitta.it	kevincannon@sims.biz	2012-02-16	1324461794
jgounel18@tuttocitta.it	kendrapatterson@salinas.com	2015-10-05	963291621
bcundyx@reverbnation.com	flyons@wall.com	2011-06-18	752891368
bpicot1d@businessinsider.com	jacquelinehood@bowman-lewis.biz	2011-05-08	328323365
vmeriot14@wunderground.com	sarah68@fletcher.com	2016-07-25	1038492945
cjacob1a@webmd.com	griffithwilliam@freeman.com	2010-02-13	1915607379
cjacob1a@webmd.com	ruizheather@freeman.com	2016-04-27	311816414
dmcbays@berkeley.edu	griffithwilliam@freeman.com	2011-02-08	381724618
dcardon1c@unicef.org	lpotts@mayo.org	2011-04-18	828453681
mbanburyp@friendfeed.com	lpotts@mayo.org	2016-12-08	1449749342
evaissiereu@ibm.com	marktaylor@haynes.com	2014-02-12	1307418290
kricciardiellot@google.com.au	ruizheather@freeman.com	2013-07-28	1711861064
dcardon1c@unicef.org	sarah68@fletcher.com	2010-10-25	126465331
evaissiereu@ibm.com	wterry@jones.org	2015-10-22	200366251
sdarleyy@admin.ch	sarah68@fletcher.com	2011-10-31	1425776620
oduffan16@yolasite.com	sharonhensley@thomas.com	2015-04-15	1580895567
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.kategori (kode, nama) FROM stdin;
AtHrZvcHKzikRrTHPOTZ	Baby
XZbDwQTXKkGZdHRjaNMn	Either
gmcDrdmGQzpseKbAHwlQ	Health
mUprPAhrScTdfJaltZdU	May
NsTTnTmaGDNmTGJVYMAM	New
nPmTKVLXyIoYKKrYozyV	Pull Off
QMoIlmTdXTYAkfoYgveI	Yourself
vyUdfpAsTyIVdgHxgnrI	Civil
fmYYELgDIKtbxfQibQQZ	Much Floor
mUKCZPgIQTplsIBVtihL	While
azzMSuBwyGrUePTBmjYi	Old
tJletIBBXoAchzdPQowr	Report Organization
HEWMlagnXQqWjORQoCwr	Table Another
VXQQGKaLREtrmPgqyPZL	Road
YAbsBHRvmGUBjrWidzRD	Necessary Movie
KgKoVjaoKJyjpBYMwKpY	Fast Throughout
iyTJavItPqIUQxmIWecI	Enough
lvebygryXTvFwINeXKlH	Whether Him
ASvjvnuVonuhxXaXAXIh	Discover
IJbzNfkXhaqDKArgrJww	Manager Mrs
bOmWgnzNJGaHPSdSuUtv	Activity Bar
dgprRTesqtzqALTAsmjV	Per Probably
knflyawJqXKGwgGqSGWr	Star Oil
ZVwjUkjEQefxNyAaXGxR	Economy
ufOBsffFJfgbviBgGjtq	Notice Level
PTJEwXGJszoyRLTdRxHx	Or Remember
ndgnnNaNgwznOoTTWPpk	Off Upon
ZmljBtAjVSCirDxRqDjh	Serious Short
qKgoSaEgODMJuAkoPthv	Style
WqWlRoeMGdQoidRrKVbp	Improve
LvugVwujjeNOieJdmmCq	Support Wife
sDaAFUqIJNzuGFLlFFOC	Thousand Place
GhZDovMGaJDNLxTmhrNG	North Seem
HCIOUGrfvAgApLzOWCOv	Tv
msRcVQFTSlUTRgkqYmhJ	Eye
XgLFWVmLWufVNRztOrkR	Strong
WTzVOvvfpqfiPxHKWxHv	Director Two
YqfHkOPJEkEGDwEPRKPo	Enough
tWTANirOBVHGTegclkcR	Present
jPylRWSaCVpcajyNMERR	Whole
tNCtmXPtkSKCXvlUrPjY	Close
ijOeNTCSptDskemfDVlN	Deal
VElYADRfdhHRnHwkPtpw	By Food
VQbPyhynIuqVmFOkBTWu	Design
mDIADYHTSrMgyAeWuSCr	We
WQGVqINetoZTQBSZgAxo	Go
mIuriHLIbBcaAvxwwwLx	Family Every
OxcLANWxkBHvrXjMRHJK	Appear Team
GjDEnDxIWiqUwjFkQaAg	Benefit Those
DsFWKryvoShpRrZmFBZw	Recently
\.


--
-- Data for Name: kategori_kegiatan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.kategori_kegiatan (kode_kegiatan, kode_kategori) FROM stdin;
eoqHjbtdpSStiuGMSUSy	VXQQGKaLREtrmPgqyPZL
jRfzmVGuviSEGbitzWgi	iyTJavItPqIUQxmIWecI
FeHbHHCrkkOtJUOzVKjn	XZbDwQTXKkGZdHRjaNMn
OXasjRqPFGNuIXASbzIh	nPmTKVLXyIoYKKrYozyV
zFIwxgogWVIEygrsuXip	gmcDrdmGQzpseKbAHwlQ
nvuSJTMZbyGQdlMHNDeT	fmYYELgDIKtbxfQibQQZ
MGjQebCOQZdTAbRaIunU	msRcVQFTSlUTRgkqYmhJ
NZIVtgisIVXKyBSzrohq	tJletIBBXoAchzdPQowr
DxKOzmEFhzfChzrCXWBi	ZVwjUkjEQefxNyAaXGxR
yBPuoCvOWuGpjoTkQWIL	tNCtmXPtkSKCXvlUrPjY
aBodfVzEbxAXHdMZHRoi	iyTJavItPqIUQxmIWecI
MfMGNGSQbvnHKiFZBcYb	AtHrZvcHKzikRrTHPOTZ
RxLGLpPfjiLiGXVapiJB	AtHrZvcHKzikRrTHPOTZ
jmsbrxJMPCQqVblisMSy	qKgoSaEgODMJuAkoPthv
OEODpLpwHlrVZIJZYHFh	XZbDwQTXKkGZdHRjaNMn
bOneUsYxBlcApICCJacC	WTzVOvvfpqfiPxHKWxHv
CKOfpGAYUdRIdjoZMklU	tJletIBBXoAchzdPQowr
LMGzcEyRGmYYVpivJnhe	VQbPyhynIuqVmFOkBTWu
KgjbYQsZIQpzUWHWTSJA	AtHrZvcHKzikRrTHPOTZ
AAQdVVvbIpxDsNoKQZaQ	PTJEwXGJszoyRLTdRxHx
hzPenDzQzfUcmpjsIBRz	LvugVwujjeNOieJdmmCq
cMRkQHLqdnsuQtYXYWXk	gmcDrdmGQzpseKbAHwlQ
EifCvBdXqIjfDttGsQhp	fmYYELgDIKtbxfQibQQZ
MfMGNGSQbvnHKiFZBcYb	GjDEnDxIWiqUwjFkQaAg
hzPenDzQzfUcmpjsIBRz	mUKCZPgIQTplsIBVtihL
lFQzWXHzNspnvIZhFvoA	knflyawJqXKGwgGqSGWr
aBodfVzEbxAXHdMZHRoi	fmYYELgDIKtbxfQibQQZ
LsrabZHeDSSZDEQrgnNO	knflyawJqXKGwgGqSGWr
zKAtUrlOkcNGeovvnYoJ	GjDEnDxIWiqUwjFkQaAg
QzDCkvPPPWqLeLtWbrai	HEWMlagnXQqWjORQoCwr
SOiupDYJvVVoGEwqHdJG	knflyawJqXKGwgGqSGWr
aBodfVzEbxAXHdMZHRoi	ijOeNTCSptDskemfDVlN
vlGrkdEdnBOVUqObAxrZ	ZVwjUkjEQefxNyAaXGxR
stfRguzNWuuyMOZQWyHY	ZmljBtAjVSCirDxRqDjh
CIdGXXKmNUCPYgCebpDP	ASvjvnuVonuhxXaXAXIh
aDqkjftVCVIybcEYZkdR	IJbzNfkXhaqDKArgrJww
jmsbrxJMPCQqVblisMSy	DsFWKryvoShpRrZmFBZw
MbJWLbgtaeSBOJESSxyD	qKgoSaEgODMJuAkoPthv
VfzzZfxDkOzMWpIilYka	tJletIBBXoAchzdPQowr
WmxgpzcIGdcekEpaFxrD	knflyawJqXKGwgGqSGWr
SgEfsZCKGqYrMtBkcqyj	mDIADYHTSrMgyAeWuSCr
CKOfpGAYUdRIdjoZMklU	ZVwjUkjEQefxNyAaXGxR
KAUZloFvQahvmKlBxJbX	mUKCZPgIQTplsIBVtihL
neEmBMTYYHwQUYoUgCCv	IJbzNfkXhaqDKArgrJww
ZfvIiVCmXDaepksydTzb	ijOeNTCSptDskemfDVlN
feptvFDGTvFhfQgIKXWI	YqfHkOPJEkEGDwEPRKPo
TCqqYAvjyseqjAuAhMeh	VQbPyhynIuqVmFOkBTWu
CKOfpGAYUdRIdjoZMklU	ndgnnNaNgwznOoTTWPpk
ifnWlCQOaWXrvWYoUEEj	knflyawJqXKGwgGqSGWr
AArIhcRgghqWLjVrmrac	AtHrZvcHKzikRrTHPOTZ
\.


--
-- Data for Name: keahlian_relawan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.keahlian_relawan (email, keahlian) FROM stdin;
hthowless2f@opensource.org	synergize end-to-end applications
hbloomfield1o@tuttocitta.it	reinvent back-end networks
kstainton2h@state.gov	empower synergistic markets
sidle2d@sfgate.com	exploit strategic info-mediaries
vvillaret2g@utexas.edu	syndicate killer niches
kstickler20@nbcnews.com	optimize mission-critical schemas
lcollopy1v@so-net.ne.jp	embrace collaborative content
akezar1g@cdbaby.com	repurpose frictionless functionalities
rtalmadge1f@php.net	aggregate ubiquitous bandwidth
sgeerits2q@tiny.cc	repurpose e-business bandwidth
hthowless2f@opensource.org	seize intuitive models
kstickler20@nbcnews.com	aggregate front-end info-mediaries
tpinyon21@amazonaws.com	implement leading-edge initiatives
njameson2l@yandex.ru	engage vertical convergence
cpirdy1p@usatoday.com	innovate turn-key web-readiness
njameson2l@yandex.ru	scale extensible schemas
bfeldhorn1n@gnu.org	aggregate seamless functionalities
pcases1t@4shared.com	reinvent virtual channels
swallworke24@imageshack.us	envisioneer web-enabled content
njameson2l@yandex.ru	reinvent proactive bandwidth
amconie25@com.com	exploit sticky users
eclubb2r@hp.com	drive dot-com action-items
lcollopy1v@so-net.ne.jp	facilitate bricks-and-clicks niches
rinnocenti1r@comcast.net	aggregate synergistic methodologies
rtalmadge1f@php.net	enable front-end infrastructures
eclubb2r@hp.com	deploy killer systems
fbartrop1e@sciencedaily.com	extend holistic synergies
lcollopy1v@so-net.ne.jp	matrix 24/7 ROI
lcollopy1v@so-net.ne.jp	scale synergistic convergence
amconie25@com.com	facilitate proactive partnerships
eclubb2r@hp.com	target wireless markets
bgreber1w@moonfruit.com	strategize enterprise e-commerce
vvillaret2g@utexas.edu	incentivize intuitive action-items
cshevlin2e@comcast.net	architect frictionless mindshare
jgot1u@hao123.com	morph enterprise applications
edilgarno28@canalblog.com	seize virtual info-mediaries
mgehring1q@spiegel.de	revolutionize proactive users
tmayhead22@hc360.com	optimize sexy action-items
kstainton2h@state.gov	innovate cross-platform metrics
tpinyon21@amazonaws.com	engineer dynamic web services
bgreber1w@moonfruit.com	unleash magnetic technologies
vvillaret2g@utexas.edu	integrate e-business synergies
akezar1g@cdbaby.com	aggregate synergistic mindshare
awhatman2c@twitpic.com	engineer magnetic eyeballs
swallworke24@imageshack.us	optimize transparent communities
jgot1u@hao123.com	envisioneer one-to-one solutions
bgreber1w@moonfruit.com	matrix rich networks
sgeerits2q@tiny.cc	seize enterprise models
aligertwood1k@who.int	enable dot-com e-tailers
kstainton2h@state.gov	benchmark customized paradigms
rbarents26@naver.com	incubate compelling markets
mgehring1q@spiegel.de	mesh collaborative methodologies
bgreber1w@moonfruit.com	architect extensible eyeballs
gstebbings23@ow.ly	grow leading-edge partnerships
tpinyon21@amazonaws.com	embrace scalable markets
gbortolussi1z@yahoo.com	orchestrate bleeding-edge functionalities
eclubb2r@hp.com	implement innovative e-services
mgehring1q@spiegel.de	implement collaborative content
afortoun2a@imdb.com	productize best-of-breed e-business
flyford1y@homestead.com	leverage B2C action-items
jgot1u@hao123.com	engage compelling technologies
lbattill2p@apache.org	enhance leading-edge paradigms
vvillaret2g@utexas.edu	scale world-class technologies
tmayhead22@hc360.com	facilitate front-end partnerships
rglide2k@ox.ac.uk	transition compelling info-mediaries
bgreber1w@moonfruit.com	aggregate collaborative e-tailers
swallworke24@imageshack.us	generate scalable metrics
sgeerits2q@tiny.cc	matrix next-generation metrics
mgehring1q@spiegel.de	implement interactive technologies
acrawshaw2j@hexun.com	incentivize leading-edge interfaces
ashadwick1h@spiegel.de	e-enable viral infrastructures
acrawshaw2j@hexun.com	visualize synergistic web services
akezar1g@cdbaby.com	e-enable next-generation action-items
afortoun2a@imdb.com	revolutionize e-business portals
vvillaret2g@utexas.edu	synergize holistic portals
pcases1t@4shared.com	syndicate turn-key vortals
rinnocenti1r@comcast.net	scale strategic platforms
mdermot1m@foxnews.com	whiteboard back-end e-tailers
amconie25@com.com	synthesize B2B paradigms
akezar1g@cdbaby.com	facilitate cross-platform e-services
afortoun2a@imdb.com	embrace one-to-one networks
rglide2k@ox.ac.uk	drive visionary bandwidth
afortoun2a@imdb.com	expedite wireless architectures
hbloomfield1o@tuttocitta.it	benchmark value-added supply-chains
aligertwood1k@who.int	optimize sticky users
cshevlin2e@comcast.net	redefine interactive partnerships
kmcwhirter1s@ibm.com	repurpose viral systems
tpinyon21@amazonaws.com	empower next-generation synergies
hthowless2f@opensource.org	embrace granular web services
gbortolussi1z@yahoo.com	streamline viral eyeballs
ashadwick1h@spiegel.de	envisioneer compelling deliverables
mgehring1q@spiegel.de	facilitate virtual users
tlummis2n@springer.com	seize global vortals
kmcwhirter1s@ibm.com	generate front-end web-readiness
gbortolussi1z@yahoo.com	seize strategic e-tailers
kstickler20@nbcnews.com	transition visionary web services
kstickler20@nbcnews.com	embrace web-enabled convergence
eclubb2r@hp.com	whiteboard viral ROI
jgot1u@hao123.com	maximize strategic info-mediaries
rbarents26@naver.com	grow vertical users
vvillaret2g@utexas.edu	transition rich relationships
swallworke24@imageshack.us	visualize global schemas
mgehring1q@spiegel.de	reinvent back-end applications
swallworke24@imageshack.us	mesh user-centric e-markets
tmayhead22@hc360.com	synergize turn-key platforms
mgehring1q@spiegel.de	transform strategic portals
cbradnick2b@addtoany.com	maximize collaborative web-readiness
jgeorgeon29@sbwire.com	re-contextualize collaborative systems
gbortolussi1z@yahoo.com	mesh dot-com e-business
hthowless2f@opensource.org	integrate collaborative supply-chains
cpirdy1p@usatoday.com	re-intermediate transparent e-markets
bfishbourn2o@berkeley.edu	productize front-end supply-chains
swallworke24@imageshack.us	drive next-generation models
jgeorgeon29@sbwire.com	productize turn-key users
jgeorgeon29@sbwire.com	enable sticky supply-chains
kmcwhirter1s@ibm.com	deliver open-source supply-chains
gbortolussi1z@yahoo.com	reinvent plug-and-play markets
jgeorgeon29@sbwire.com	seize e-business partnerships
bbiggerdike2i@unesco.org	empower open-source infrastructures
swallworke24@imageshack.us	repurpose clicks-and-mortar info-mediaries
kstainton2h@state.gov	envisioneer value-added experiences
rtalmadge1f@php.net	facilitate wireless niches
asouthern1x@mit.edu	cultivate strategic networks
mgehring1q@spiegel.de	repurpose sticky action-items
afortoun2a@imdb.com	iterate innovative technologies
rinnocenti1r@comcast.net	architect killer partnerships
rtalmadge1f@php.net	seize innovative schemas
pgebb1l@yahoo.com	deploy value-added platforms
kstickler20@nbcnews.com	extend frictionless web-readiness
cshevlin2e@comcast.net	deliver best-of-breed solutions
vvillaret2g@utexas.edu	grow interactive e-services
pgebb1l@yahoo.com	incubate leading-edge paradigms
cshevlin2e@comcast.net	visualize clicks-and-mortar paradigms
acrawshaw2j@hexun.com	transition visionary technologies
bfeldhorn1n@gnu.org	generate granular models
tlummis2n@springer.com	revolutionize robust eyeballs
kbruineman1j@wikipedia.org	exploit virtual interfaces
awhatman2c@twitpic.com	e-enable viral bandwidth
mdermot1m@foxnews.com	benchmark virtual paradigms
bbiggerdike2i@unesco.org	revolutionize mission-critical web-readiness
hthowless2f@opensource.org	redefine bleeding-edge solutions
tmayhead22@hc360.com	architect impactful solutions
jgot1u@hao123.com	brand cross-media applications
vvillaret2g@utexas.edu	innovate vertical e-commerce
sidle2d@sfgate.com	productize visionary web services
vvillaret2g@utexas.edu	leverage best-of-breed supply-chains
rbarents26@naver.com	empower seamless interfaces
fbartrop1e@sciencedaily.com	grow web-enabled users
pgebb1l@yahoo.com	brand mission-critical solutions
tmayhead22@hc360.com	syndicate holistic relationships
eclubb2r@hp.com	synergize wireless e-tailers
mdermot1m@foxnews.com	streamline granular e-markets
akezar1g@cdbaby.com	innovate killer functionalities
rtalmadge1f@php.net	benchmark impactful e-tailers
pcases1t@4shared.com	iterate user-centric infrastructures
edilgarno28@canalblog.com	reinvent visionary convergence
lbattill2p@apache.org	unleash value-added e-services
amconie25@com.com	synergize dot-com solutions
ivieyra27@cbslocal.com	aggregate cutting-edge e-commerce
bfishbourn2o@berkeley.edu	reinvent integrated infrastructures
rglide2k@ox.ac.uk	facilitate out-of-the-box systems
fbartrop1e@sciencedaily.com	visualize end-to-end eyeballs
acrawshaw2j@hexun.com	iterate out-of-the-box vortals
sidle2d@sfgate.com	re-intermediate collaborative channels
acrawshaw2j@hexun.com	streamline clicks-and-mortar applications
rbarents26@naver.com	disintermediate 24/365 systems
cbradnick2b@addtoany.com	streamline ubiquitous channels
tmayhead22@hc360.com	empower bleeding-edge convergence
sgeerits2q@tiny.cc	redefine plug-and-play platforms
kstickler20@nbcnews.com	engineer compelling bandwidth
edilgarno28@canalblog.com	synergize ubiquitous technologies
eclubb2r@hp.com	visualize cutting-edge systems
rtalmadge1f@php.net	brand visionary supply-chains
awhatman2c@twitpic.com	productize strategic supply-chains
vvillaret2g@utexas.edu	leverage visionary convergence
rinnocenti1r@comcast.net	innovate intuitive info-mediaries
tpinyon21@amazonaws.com	orchestrate transparent users
njameson2l@yandex.ru	visualize virtual web services
gbortolussi1z@yahoo.com	engage web-enabled initiatives
lcollopy1v@so-net.ne.jp	revolutionize viral interfaces
njameson2l@yandex.ru	engage rich e-business
pgebb1l@yahoo.com	productize B2B metrics
rglide2k@ox.ac.uk	harness user-centric interfaces
bfishbourn2o@berkeley.edu	harness cross-platform platforms
awhatman2c@twitpic.com	implement killer e-markets
eclubb2r@hp.com	synthesize back-end ROI
sidle2d@sfgate.com	visualize revolutionary functionalities
rtalmadge1f@php.net	whiteboard cross-media channels
fbartrop1e@sciencedaily.com	generate strategic mindshare
akezar1g@cdbaby.com	productize interactive platforms
kmcwhirter1s@ibm.com	envisioneer world-class methodologies
mdermot1m@foxnews.com	morph cutting-edge users
pcases1t@4shared.com	facilitate efficient infrastructures
tmayhead22@hc360.com	synthesize compelling niches
kbruineman1j@wikipedia.org	integrate global deliverables
mdermot1m@foxnews.com	transform real-time channels
pcases1t@4shared.com	aggregate one-to-one applications
mgehring1q@spiegel.de	maximize bricks-and-clicks channels
tlummis2n@springer.com	engage killer supply-chains
tmayhead22@hc360.com	streamline value-added web-readiness
rbarents26@naver.com	incubate customized mindshare
tlummis2n@springer.com	visualize intuitive mindshare
bfishbourn2o@berkeley.edu	extend web-enabled solutions
cbream2m@shareasale.com	visualize virtual web-readiness
hbloomfield1o@tuttocitta.it	visualize world-class partnerships
hthowless2f@opensource.org	disintermediate cutting-edge experiences
cshevlin2e@comcast.net	innovate cross-media web services
cbream2m@shareasale.com	incentivize extensible deliverables
sidle2d@sfgate.com	aggregate proactive schemas
edilgarno28@canalblog.com	streamline world-class e-markets
cshevlin2e@comcast.net	morph synergistic vortals
acrawshaw2j@hexun.com	redefine strategic models
jgot1u@hao123.com	brand customized experiences
tmayhead22@hc360.com	empower real-time info-mediaries
fbartrop1e@sciencedaily.com	re-intermediate end-to-end solutions
jgot1u@hao123.com	innovate value-added e-tailers
bfeldhorn1n@gnu.org	expedite dynamic channels
afortoun2a@imdb.com	enhance e-business functionalities
rinnocenti1r@comcast.net	monetize end-to-end web-readiness
cbradnick2b@addtoany.com	incentivize distributed ROI
bgreber1w@moonfruit.com	morph e-business applications
pstubbley1i@flavors.me	re-intermediate magnetic deliverables
sgeerits2q@tiny.cc	repurpose out-of-the-box communities
cbradnick2b@addtoany.com	transition clicks-and-mortar users
cbradnick2b@addtoany.com	integrate value-added e-business
relawan1@relawan.id	Keahlian 1
relawan1@relawan.id	Keahlian 2
relawan1@relawan.id	Keahlian 3
\.


--
-- Data for Name: kegiatan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.kegiatan (kode_unik, organisasi_perancang, judul, dana_dibutuhkan, tanggal_mulai, tanggal_selesai, deskripsi) FROM stdin;
KAUZloFvQahvmKlBxJbX	xenglish@mcdaniel.com	International Expert Discussion Executive Within	122854696	2017-04-01	2018-01-29	Future behind so. Ask too employee wrong him.
yYxLrFNUbxFFkCYSmRNb	marc24@lewis.org	Out Item Light	141517395	2013-01-08	2018-04-09	Else attention yourself easy building capital person.
FwragpGvjcYtLcZzbeCo	richardallison@morrow-jackson.net	Real Talk	183369877	2012-03-21	2015-09-20	Billion technology head. Century region way form its compare.
XGAgKUDbogrgbOLhXknu	kevincannon@sims.biz	Full Pull	155883722	2010-12-08	2014-01-12	For song man whom remember mean population. Per myself of who whom skill.
MfMGNGSQbvnHKiFZBcYb	wterry@jones.org	Begin	149296520	2014-05-26	2015-04-20	Mind follow step loss. Else number level rise response we must. Him determine other recent ok.
OXasjRqPFGNuIXASbzIh	lowetimothy@collier.biz	Guess Wrong Take	95287525	2012-12-25	2015-01-05	Remain present father scientist environment meeting. Street enjoy girl cut.
pblSsWSTxSHKAYLoBizj	lowetimothy@collier.biz	Father Game Think Family Ever	28487796	2018-03-11	2018-03-26	Treatment capital per there yes first brother. Process thus common common spend.
tnAWtpaTLqRVFpxTFUMz	petersonmichael@miller-gray.com	Trouble Including Test Family Too	828323	2012-08-11	2014-07-13	Home seem food education than every past today. When responsibility place five. Knowledge bank meeting especially usually seem.
iwLgdZcpWzKfavVWlvNm	petersonmichael@miller-gray.com	Art Accept	107535066	2011-05-03	2012-08-15	Break same drive significant. Buy as government political head very. Reveal final state over week mention usually.
VGLFpidMrKtsVSOAJTaY	jenningselizabeth@sweeney.com	Enjoy Cultural Difficult Value Interest	109815034	2017-03-01	2017-10-12	Financial own newspaper two leg. Share worry song. Gun form room answer social.
OEODpLpwHlrVZIJZYHFh	kendrapatterson@salinas.com	Activity Seven	55449815	2015-09-21	2017-10-09	Specific foot rise number husband color real. If with young maintain than food ever whatever. Indeed various support discover fire those.
ukgAnFfldqgdaLybGfOo	kevincannon@sims.biz	Protect	131551155	2013-08-23	2015-04-21	Standard large officer. Wife resource could market method reach fact stop. Notice read opportunity sell western agency region. Word spend range become condition clearly industry series.
JxmPaCqEnfKVDBwItvxp	michaelrogers@johnston-zavala.com	Share Miss Budget	75701112	2010-07-11	2013-01-06	Exist conference new above financial authority lawyer. Put article tree fine how exist.
zqdbDWdZlHIocKOsbsRz	flyons@wall.com	Woman Policy Important Today Shake	158207326	2016-04-16	2017-02-12	Picture treatment business room cut. Respond if create. Eat fly child marriage tough these radio.
mdktgIoiFAXvvUhzTCPo	uhoward@thomas.net	Drug	77363637	2011-04-27	2015-03-30	Agency understand system little and simple. State yard later need. At decision about change president card several game.
tjHskwrZmCzUeuGftTuK	lowetimothy@collier.biz	Allow Find Response	165735118	2010-01-27	2016-12-17	Painting recognize subject second end. Growth property both process happy around.
RxLGLpPfjiLiGXVapiJB	wterry@jones.org	Account After Medical	35281505	2013-08-16	2014-06-04	Ground military him above six choose reveal. Reveal one sell along television ground.
YrauzBBSoECHPXPabTgz	nguyenryan@peck.com	Draw Walk	47138499	2013-01-05	2015-05-07	Sell protect candidate politics term once. Shoulder drop short performance.
YujtGdlPLXQqvkKrGDuD	xenglish@mcdaniel.com	Mission Reflect Notice	99440096	2017-02-03	2017-09-23	Likely born resource town. Sing ten inside development light occur.
hzPenDzQzfUcmpjsIBRz	michaelrogers@johnston-zavala.com	Market Old Really Class	160811543	2014-12-03	2016-11-17	Outside challenge central doctor anyone five staff. Mission mind play life final. Only very just town book Democrat anything.
KWWEtlEaWmEyqCBKsuCp	jenningselizabeth@sweeney.com	Fear Most Situation Carry Message	55877592	2013-04-16	2016-05-06	Remember identify whole. Take result street within.
ASWHnAPiVkHxGxUguORE	jenningselizabeth@sweeney.com	Great Shake Student	106323879	2011-04-16	2016-01-12	Southern speech themselves throw treat. Dinner high ball training. Government member meeting. Republican value actually sometimes speak.
ueowbUaoZIwReCOCRAOF	douglashatfield@decker-woodard.com	Occur Fine	132831994	2013-08-01	2015-01-12	Six data kind.
SjdMssiJsIuDaxtPaadt	richardallison@morrow-jackson.net	Everybody No Design Hundred	91600742	2015-07-19	2016-02-01	Expert space later help above. Instead country describe above her chance. Side step particularly.
YfvaGhhnNpBrPnSeXJec	lgarza@moreno-villarreal.com	Debate Admit Statement According	175494040	2010-04-30	2012-02-23	Interest week leg reflect television hand. Husband fine act west. Benefit assume car challenge claim.
sfiHTzdDTZyGSqhYMsjV	richardallison@morrow-jackson.net	Appear	156652624	2018-01-14	2018-02-23	Expect security sell spring feel past pass. Quickly according bed yes visit center including arm. Nearly boy condition.
OfcsezgzEooUCuwGOhmb	douglashatfield@decker-woodard.com	Police Power Smile Dark Bring	173322846	2017-01-12	2018-04-08	Light analysis might current pretty. Pretty him indeed new down. Technology attack off than discover. Place decision son.
wrtadwPAbxGcHNdfODnB	kevincannon@sims.biz	Surface Contain Leave Gas	98213441	2017-01-14	2017-05-06	Effort plan star energy prepare letter. Ask in score national beyond particularly best. Two imagine wind believe daughter. Television debate agreement.
CnRBkPbkHYVUYwYctdPZ	douglashatfield@decker-woodard.com	Seem	40986211	2016-10-12	2017-11-24	East college seem wife. Perhaps room environment.
yBPuoCvOWuGpjoTkQWIL	michaelrogers@johnston-zavala.com	How Save Let Control	16369699	2016-02-03	2016-07-11	Clearly increase practice final speak probably by. Fire Congress property never rest how.
jwdXZfahMoZLsrLBDMCZ	xenglish@mcdaniel.com	Short Outside Tree Bag With	142582242	2017-03-12	2017-04-29	Each simple foreign style exactly. Finally anyone money.
jRfzmVGuviSEGbitzWgi	jennifermedina@edwards.com	Enough Likely	32536442	2015-12-14	2017-08-27	We nothing picture last. Scene check require Congress visit. Someone back quality we hard partner.
DxKOzmEFhzfChzrCXWBi	flyons@wall.com	Fast Pay	12751992	2010-06-07	2017-08-03	Situation drive write foreign big.
cMRkQHLqdnsuQtYXYWXk	kevincannon@sims.biz	Participant She Reach Cup When	113224068	2016-12-27	2016-12-27	Foreign be fear media leader politics contain pick. Mr food try society way I cost.
mWtLAnLHbUdgYFnWfpbN	michaelrogers@johnston-zavala.com	Difference	148723912	2010-09-13	2012-02-04	Right what detail popular them player across. Impact break wind.
gVxEOWXRhdDoqIQYenTe	xenglish@mcdaniel.com	Husband	126769512	2016-02-11	2016-06-25	Range step show nature class movie almost. Business green fish resource finally nature. Class history exactly site thank treatment.
OqLsxihlvHLUSOaNeaIz	jacquelinehood@bowman-lewis.biz	Industry Attack Star	15083455	2016-03-11	2016-11-14	Hospital hit point forward weight state kid. Surface marriage through draw century soon religious.
eysmJIgOOYiDBLFiuEoh	xenglish@mcdaniel.com	Color	124275026	2014-10-04	2015-03-23	Thus something those time mind as. Mean condition human remain section.
fIIqYUaLxrtxLTsfsHJa	kevinsalinas@potts.net	Amount Let Feeling Whole Your	5693724	2013-05-30	2017-06-13	Activity list nation fill. She mission someone.
bkMTjPkeCskWbwuEXpKc	flyons@wall.com	Debate Cup	29397592	2013-05-14	2016-03-02	Wall personal responsibility lot turn. High rule site impact nothing week life. Child nor light ever place laugh modern.
lFQzWXHzNspnvIZhFvoA	sarah68@fletcher.com	Surface	133896871	2017-09-30	2017-12-14	Offer decade leave meet team article sort. Citizen message stuff. Field community pay lawyer wife. Choice indicate direction because development.
riEYjwCWHssnxfyosZai	lgarza@moreno-villarreal.com	Affect Huge Everyone Their Woman	52713608	2013-02-05	2017-03-22	Improve already meet past man white scene. But quality seek drive small two. Factor school Democrat could.
gmDzMQQowfVyowaCauRu	shelley15@moran.com	Across Simply Old	32999895	2014-10-20	2015-06-28	Others box movement recently machine wear edge. Natural explain third.
vmULbsMFlZQcfzbXTEBT	xenglish@mcdaniel.com	Wrong Walk Home	58603830	2013-01-27	2015-02-20	Story every artist get technology. Community back bad seek.
VSXQtRtdhDqbFPmxcFGe	richardallison@morrow-jackson.net	Particular	10898079	2014-10-05	2017-10-05	Whose adult rest sense long here region. Sea rather car necessary.
DJgViLfeIiBYwcVzTuSN	sarah68@fletcher.com	Billion	7786448	2012-05-12	2014-01-19	Just spring reflect. Often realize face pay evening manage. Production real these improve.
kmqMSHdLlFMXRvdOcYnC	lgarza@moreno-villarreal.com	Financial Whatever Less Wind Late	136347918	2018-01-23	2018-03-22	Rate term accept film sing major. Interest hit the receive magazine father reason.
CIdGXXKmNUCPYgCebpDP	lpotts@mayo.org	Involve Star Ok	161052109	2014-01-27	2015-05-30	Carry require feeling PM our thank benefit. With on be defense within medical effect oil. Who what state forget hope join.
RBVkWthdOwiisPhiKlcK	ruizheather@freeman.com	American Use Determine Us	196646832	2011-01-06	2015-06-25	Wall treatment from worker develop person evening. Cold tonight social future herself throw clear. Week writer least particularly act thought.
ALggCmUwmDfPjFmzkDmJ	lgarza@moreno-villarreal.com	Clearly Low Word	175948634	2012-01-11	2012-09-19	Identify sell be admit surface. Respond talk fact cost loss. Else with TV everyone.
vlGrkdEdnBOVUqObAxrZ	uhoward@thomas.net	Too Smile Know Position Party	185333100	2010-11-05	2011-12-05	Economic pull me politics son common difficult.
CKOfpGAYUdRIdjoZMklU	ruizheather@freeman.com	Hope Manager	194508289	2012-05-18	2014-07-26	Treat some exactly newspaper ten room. Nature receive born war admit policy my.
FeHbHHCrkkOtJUOzVKjn	trananthony@brown.com	By	143561853	2016-02-04	2017-02-22	Sure pattern dinner window else national mind. Company forward game point scene price.
cOhtuqdRQjDfflMqKlZL	marktaylor@haynes.com	Space	135209702	2013-03-25	2014-06-06	Movie result election life about. College beyond down here government environmental.
mfsnPWaVqJZcLdNnJpxJ	jennifermedina@edwards.com	Toward Imagine Pattern New	159274255	2017-03-05	2017-05-14	Measure truth accept require. Scene follow decide much lose power.
PfiGEmLjAGRAbZaLQNNi	jennifermedina@edwards.com	Hospital Investment Great	5860394	2012-07-08	2013-10-25	Model deep production attorney group. Treatment cover per rest open scientist fast. Red light front lose street south. Institution could necessary newspaper remain without memory.
DBIDEXWaQGhQGtmWSBXY	richardallison@morrow-jackson.net	Possible Quite Morning	119617903	2012-08-21	2013-06-01	Around weight western just consumer participant.
aDqkjftVCVIybcEYZkdR	jennifermedina@edwards.com	Else Least My Investment Community	148030345	2015-12-07	2016-04-30	Challenge major letter behind either. Show improve send account interest. Now once husband determine choice next apply.
jVDWFkZPrjyqOGMrzLfg	lgarza@moreno-villarreal.com	Say Clear Behavior Gas	55276609	2012-05-17	2014-02-25	Though foot health can life listen environment. Moment quite build feel. Defense nothing hand kind memory many operation.
NpJFXNUbWryizCLyswMH	marktaylor@haynes.com	Ever Seven Really Young	89161812	2012-11-02	2013-06-06	Ability start range attorney far year hot. That PM course research cost window.
WmxgpzcIGdcekEpaFxrD	griffithwilliam@freeman.com	Various Training Realize Song Wall	191015038	2017-11-08	2018-03-13	Purpose admit teacher. Represent allow democratic sea relationship of sell. Among entire within safe.
ImEVKTmquRyVqYuTbnaT	nguyenryan@peck.com	Expert Determine Produce	56023171	2013-12-21	2015-03-01	Product large fast. Why morning early meeting begin wear.
gyuUMnTTTpISAKrOOENR	ryanmarshall@greene.com	Nature Reduce	59147012	2010-09-28	2015-02-27	Resource foot cultural star. Name situation position citizen role each guy. Someone art big herself.
QRdLNDdVxImFHwPkJEmf	marktaylor@haynes.com	New Car	57696597	2016-01-05	2017-05-18	Agency figure so contain cover nation.
ptaRKeEcvAGvqvcuufbI	kevincannon@sims.biz	Store	110195060	2017-07-06	2018-03-11	Far nice least sing amount. Forget east alone discover.
uhlDPzMQNuwCdbouTNcE	lowetimothy@collier.biz	Statement Indicate	169381900	2015-06-16	2017-11-27	Argue skill south ever thousand. Identify they table happen them scene.
GINuFEMVNpewCOPlFAgT	petersonmichael@miller-gray.com	Consumer Organization Activity	122570487	2011-07-30	2015-01-26	Protect recognize detail game however prepare. Project determine edge executive visit along. Past down sometimes happen in young sister.
oCJijwMmNJpWDQpIcXUQ	michaelrogers@johnston-zavala.com	Simply Sign Need Yeah	26822263	2013-06-10	2016-11-06	Own gas discover deal. None both figure that bed tend. Place bring into kitchen whole church.
xrjMnVwePemAXNAlmEPA	ruizheather@freeman.com	Thus Together Box	70587070	2017-07-04	2018-02-15	Participant front choice religious month positive. Pull quality spring decade international despite almost. Indicate someone whole free degree wish game.
rViakiLlouaYvslYgHvC	kevincannon@sims.biz	Always	95965960	2016-08-07	2017-09-12	Road southern name security catch answer. Parent bed despite. Song get owner final as Mr teach side.
awBdmbqIYpIaQlEePKXG	michaelrogers@johnston-zavala.com	That News	76351316	2017-04-18	2017-12-31	Ready full when final reduce figure space. Think throughout young article federal land as. Computer instead everyone poor.
MbJWLbgtaeSBOJESSxyD	xenglish@mcdaniel.com	Tell Represent	26914921	2013-02-12	2015-02-20	Involve word safe former building perform education discussion. Them still himself among would listen baby. Kind system music away.
zKAtUrlOkcNGeovvnYoJ	douglashatfield@decker-woodard.com	Somebody Mrs Area	81842148	2012-09-07	2013-02-15	Why west under wear represent father. Consumer listen interview street risk skin.
emgKjuWWpMpnwPKxUUfG	marc24@lewis.org	Describe Go Skin	37019426	2016-03-16	2017-12-24	Development course rule people bag accept Mrs. Capital indicate reality.
mNEykxgODYNHrIQtViTj	ronald25@fields.com	Respond Quickly Common High	25078150	2016-08-15	2018-04-15	Happy provide city friend series live Mrs back. Rather peace end ahead poor my. Choice sign development can.
pOXbAyDdZiGWwjjYhKSu	marc24@lewis.org	Poor Defense Sometimes Right	155098455	2017-03-17	2017-07-24	Move community specific movement reveal man raise day. Land western Mr east.
kGJrjWLYYMMcTCRRRCsQ	ryanmarshall@greene.com	Blue Clear Grow Treat	124732701	2016-08-22	2016-11-30	Spend hope you national in yes. Catch senior adult customer.
RkfiaZdeZXLsPjuDWdlx	lgarza@moreno-villarreal.com	Suddenly	65256878	2015-02-07	2015-03-03	Nature get agreement everyone fact probably. My court instead treatment. Visit ask mission.
xTVvgaxPBsmcnkRoYZfs	wterry@jones.org	Drug Figure Event International Various	165935510	2017-10-27	2018-02-08	Fall forward people include. Represent let fine might low story stock ask. Analysis PM interesting onto name cost authority dinner.
TwzfwWRjFAxgVAEvSukq	thomaspeters@rodriguez.com	Enjoy Foot Three Process Soon	37944495	2012-12-20	2013-02-06	On right lawyer discover thousand which. His leg help simple. Short line sport.
LRHkHkGCiCnsEfqdXINI	lgarza@moreno-villarreal.com	Order Drive But	87932804	2014-05-19	2015-10-23	Those large attention rest impact church. Necessary heart section majority wait increase room history. Seven determine nation inside.
eSYblbJKmdKvFeuWqngl	flyons@wall.com	One Arm Good Even	49003291	2010-05-06	2012-01-17	Might leg under evening cause.
TCqqYAvjyseqjAuAhMeh	marc24@lewis.org	Really Window	100053539	2017-01-02	2017-09-22	Thing note nice push sure. Check during tell amount. Contain model word fall.
cFtrmZuHgmGWGtsWMsFY	kevincannon@sims.biz	Still Side Chance Business Source	36807551	2018-03-01	2018-04-16	Especially recent draw girl measure. Rock as billion here.
vjtQXDqDbzrxbYZhgjPY	shelley15@moran.com	House	129277739	2011-10-22	2016-08-26	Accept enter defense set morning. Open morning think ago structure along quite thing. Book specific live kind professor democratic rather.
yRKJyIaonxlBZbISgtwS	douglashatfield@decker-woodard.com	International Share Read Term Maybe	31788622	2013-01-07	2015-05-30	Cut ago want law level. Wind win north month.
lmoYaVUsaDbClPRlHYxA	kevincannon@sims.biz	Pm Machine Recent	62381704	2017-08-13	2018-02-10	Act relationship game daughter leave painting. I better discussion box message during free.
gWdPeqJRnARsCRkxILaS	ryanmarshall@greene.com	Way Same Hour Fact Interest	95924889	2018-02-24	2018-03-14	Present environmental management ground material interest turn. Charge manager instead board effect thank. Industry peace later mouth.
stfRguzNWuuyMOZQWyHY	jennifermedina@edwards.com	Television	134113240	2012-03-11	2012-06-11	Final soldier it for great common compare. Treat effect travel short thousand beat. Goal others people care church more.
ScwkScKVxFgbaUuSLMcd	petersonmichael@miller-gray.com	Reveal Outside Population Former	78495200	2016-10-17	2017-02-26	Affect imagine race road. Every six myself make reflect figure citizen indicate.
GsEGYBpkKIHmQnTddcjo	uhoward@thomas.net	Cold Above Type	73691728	2013-01-28	2017-08-06	Role experience anyone discussion. Probably his agree tell protect.
PZluZwQdywdmBuAEyWso	sharonhensley@thomas.com	Rate	37990130	2014-07-25	2016-04-22	Provide result middle father almost our always. Voice two play year with however ability. Yard name school.
afyPjQOWncGEgTXufhBq	thomaspeters@rodriguez.com	Better Seek Simple Street	9428664	2013-09-10	2016-05-01	Be book skin place rich have hotel. Exactly close speech scene clearly treat.
rfrRTllOUXAUBvlkYjUO	ryanmarshall@greene.com	Movie Drug	128895998	2011-02-02	2016-07-04	Not meet weight response pass. Why father defense me.
MuJtQBfNBsPHqrFMPHeQ	richardallison@morrow-jackson.net	Pm Rate Author Kitchen	64881470	2017-06-23	2017-12-07	Gun party order. Full mean score also loss.
MTjZCRbfFnHoLMvBYptg	wterry@jones.org	Lot Point Worker However Dog	149583776	2011-01-22	2013-10-18	Ready statement return just our determine myself leg. Task number his already true they example. Message one interesting learn.
RSYTIJzlvonWLZawvpWW	wterry@jones.org	Key	189254184	2015-05-15	2017-08-21	Down national talk market music challenge anything. Production fish blue would crime. Rather around simple this although.
VChzKIQvEzRwzkqxxqpP	michaelrogers@johnston-zavala.com	Smile Way Cold Walk Place	20873881	2010-01-05	2013-05-08	Who out green special organization among trial. Religious fight they rule floor. Pressure century open drive food career.
NcpjSfxNwODNRbSwKMvP	richardallison@morrow-jackson.net	Child Five Fish	158035281	2012-06-18	2014-08-26	Role sort director forget improve. Assume past maintain record leader can. Four risk benefit look design.
SrtIaXnMVHsGLFpzFwia	trananthony@brown.com	Hear Beyond Particular Court Almost	109968117	2010-03-01	2011-08-17	Do health job theory sit professional house. Herself two hospital others recent player.
EWNFPyThgKscblBKrtbk	jennifermedina@edwards.com	Year Study	42471306	2013-04-11	2014-05-23	Citizen board agreement democratic consumer table. Month attorney field glass receive recent doctor.
uRbxOAqCzYjZAvdiugKV	flyons@wall.com	Although More Soldier	47146626	2017-01-25	2017-07-04	Television single challenge coach series important. Idea scientist wife watch quickly. Newspaper bring fact center.
tCjZpeyLHfZHHDekIocr	richardallison@morrow-jackson.net	Worry Tend Together	79469083	2015-08-29	2017-05-31	The computer future. Policy order art ok. Well structure speech our artist onto manage.
CBLDIFjIBbrVNojdnYpB	flyons@wall.com	It Matter School	41988599	2011-03-01	2016-03-11	While group main chair source. Chance year already.
QuBjdWwqeMnqUYlelbLo	douglashatfield@decker-woodard.com	Could My Piece Rule Pressure	20705384	2018-01-29	2018-04-17	Table bit more bill. Blood only card live mission serve study.
catYfotuWTlwQTwCUHXu	kevinsalinas@potts.net	Improve Young Your Describe	139113641	2017-10-06	2018-04-15	Tree agency science total arrive. Nation none beat garden another. At participant another seek.
ZwAPgaBdApgDgzZvAXcN	sarah68@fletcher.com	Father Say Popular Resource	153497745	2014-02-25	2016-12-24	Around front wrong surface on put. Start product sense protect measure matter. Sell apply understand traditional way manager between true.
FshaNbwntNiSADIljAIc	sarah68@fletcher.com	Represent Fear Watch Job	20658408	2013-05-28	2017-08-20	Draw you but bar computer threat. Size interest thousand always require want grow yet. Only purpose result sea world military.
uYRPdBrbjzjKMYgIQagT	kevinsalinas@potts.net	Them Speech Usually Evidence Watch	56954168	2011-09-16	2014-11-03	Involve cup identify able individual help bill. Leader several artist lay Democrat. Act around direction up population.
neEmBMTYYHwQUYoUgCCv	marc24@lewis.org	Mind	152476788	2016-06-15	2018-01-30	Bill phone until again choose yet. Win recognize force life next. Yard early school stop.
KFVCCXAuefEDZpFkwFWP	marc24@lewis.org	President	140553785	2011-11-29	2013-05-05	Set executive represent animal consumer. Shoulder with political others.
cDToXOmPKqlUrSbrPoRc	griffithwilliam@freeman.com	Situation	62001149	2017-05-22	2017-07-18	Money hour notice real boy she mind value. Reflect color it able least policy thing. Student set woman window camera since left eye.
JjRVStNcmGrhnYuRJmhn	douglashatfield@decker-woodard.com	Property Our	45456265	2013-06-18	2014-04-05	Letter itself total between magazine reach. Act address office laugh television. Everyone may either interest.
LsrabZHeDSSZDEQrgnNO	marktaylor@haynes.com	Trouble	97800156	2011-05-17	2013-03-14	Trial person military west budget. Nor describe over much staff total foreign as. True full level nice sound network each.
LzxYKREErnjynoHyKZCN	trananthony@brown.com	Compare	10556077	2014-04-03	2015-10-10	Issue police necessary network hospital improve write require. Us there good politics.
ZfvIiVCmXDaepksydTzb	kendrapatterson@salinas.com	Effect Himself	155023465	2011-02-12	2017-04-05	Management especially old they interview. Inside big imagine memory.
OsrgGTQcTVWGwRKWRHXI	wterry@jones.org	Power Growth Left	163485216	2012-08-26	2018-01-11	Early team politics study. Well commercial production few. None option safe scientist practice whatever piece point. Coach pull represent wonder treat structure nothing.
ZBiiLFBoYEuMUdfetGzg	kendrapatterson@salinas.com	Social Simply	47737755	2010-11-03	2017-06-07	Citizen actually PM natural full test. Catch there whom who wish develop. Service recognize administration throw through work north.
qWGsQVNWKIYcEemjolWK	kendrapatterson@salinas.com	Agency Forward Sign	84771933	2012-02-17	2013-09-17	Yeah item professional career here knowledge. Determine project thought sea how else hear country.
DOYsviwZowretupjkdAT	jacquelinehood@bowman-lewis.biz	Once Song Agreement Us	62421172	2010-12-16	2013-03-18	Film pass government maintain public set system. Ball fill fight.
nJraaevGlLXsjXxWTbJF	jenningselizabeth@sweeney.com	Food	100739280	2013-01-14	2018-03-01	Our including feel plant he worker evening. Share part decide father television they. Property financial anything fire.
LMGzcEyRGmYYVpivJnhe	nguyenryan@peck.com	Building Think	132757982	2014-04-21	2015-05-23	Agree toward good use officer I two. Growth sit appear letter discussion.
vALrthliMpIPHguUJnWj	sarah68@fletcher.com	Tend Save Side Cut	118929828	2013-01-20	2013-06-30	Scientist hospital third community customer consider. Wish network no food management.
OUWcWydxvuKKZZiNDUmM	michaelrogers@johnston-zavala.com	Office Require	120420254	2010-05-04	2018-01-16	Reduce customer imagine truth they person. Garden wait sign possible method. Charge pressure international rise surface language door.
SgEfsZCKGqYrMtBkcqyj	douglashatfield@decker-woodard.com	Should Baby	20529955	2017-12-11	2017-12-31	International world face term. Get rest thank believe enough staff.
eFLyOHiKXBRQYGysPGCv	sharonhensley@thomas.com	Notice Choice Though Organization Trial	71226252	2013-10-10	2015-06-01	Manage around news sense add ever. Discussion city skin force option. Though listen pull.
JLtPSphRSpHTMvxkRhAN	jacquelinehood@bowman-lewis.biz	Administration Want Should Teach Down	65746451	2010-09-19	2012-09-04	Cut member while enough church avoid. Stuff purpose bad. Food central still send.
dRNPtJCOtgctMNOJxQDl	lpotts@mayo.org	Rest Citizen Response Anything Close	34661316	2010-10-16	2012-02-04	Medical together at structure off. Suffer necessary improve now also change party fire. Adult center door allow enough recognize.
feptvFDGTvFhfQgIKXWI	jenningselizabeth@sweeney.com	Get	103833922	2012-06-19	2018-03-11	View raise it probably score reality. True player standard. Radio material worry make.
sVvjfAbHyEUtebTyFdEc	uhoward@thomas.net	Size Old	65838751	2014-06-26	2016-08-05	Summer strong bar institution then. Tough she really participant large mention. Their consumer throw end suggest.
aLsLUHdjcWoFZqKnEQRE	nguyenryan@peck.com	What Member Really Ten Throughout	137344986	2016-02-21	2017-09-18	Father near food chance seat. Defense agreement themselves professional also avoid.
adHELOGnOQoAWxxOOxqZ	jacquelinehood@bowman-lewis.biz	Some	25952586	2010-02-12	2013-09-12	Commercial reach force wrong continue company. Teacher small treatment house explain stop better item. Career focus marriage understand across.
jhtPekJETGwrNQnHLYan	kendrapatterson@salinas.com	Difficult Drive Her	175188377	2010-04-27	2010-04-29	Make six present especially. Billion soon window high. Letter other student central.
MTiRgCveqIdsTJOggVgu	sarah68@fletcher.com	Especially Order Add Voice	131929161	2010-09-21	2015-11-27	Speak without just perform. Arrive increase upon bar onto hard section.
iiaAHqWaZWSWTwVehEAL	thomaspeters@rodriguez.com	May National Term Myself House	48213317	2014-03-22	2014-11-14	Simply line board foreign interesting start property record. Street could agency early federal court. Child performance catch pass them spend.
EifCvBdXqIjfDttGsQhp	ronald25@fields.com	Here	24474814	2011-04-12	2015-08-01	Practice practice remain concern back rate. Investment yourself financial could election soon beautiful seat.
fmcEEYHeprFKQFblGKTC	shelley15@moran.com	Respond Account Stand Best	21600036	2010-09-20	2017-06-17	Relate thank south crime. Suddenly build new either. Land hand reduce itself myself nor give each.
MoQLDDxyAQZjAZvoaHTr	jacquelinehood@bowman-lewis.biz	Deep Three Spring	176407856	2010-08-25	2015-07-14	Prove your ahead short. Production time family together story book. Information past determine growth responsibility stand.
uwLnXkzyTcPtSqjyunGS	jenningselizabeth@sweeney.com	Fly	124506283	2012-09-14	2016-10-20	Be life listen sort very break.
KgjbYQsZIQpzUWHWTSJA	flyons@wall.com	Hospital Face Road School Local	189672509	2011-11-16	2017-05-25	Positive toward morning plant step. Appear stuff fire any.
RxabymYHbFutZlfzEEuC	petersonmichael@miller-gray.com	Process	19918782	2017-03-06	2017-04-03	Safe white role choice strong ok anything heavy. Before whom detail report both result which.
opXApMexLBoLMVmyEIbL	ruizheather@freeman.com	Since Deep	168037398	2011-02-28	2017-11-04	Language seat notice capital lot level Republican. Might onto within second century compare institution rather. Book travel sign leg.
bOneUsYxBlcApICCJacC	ryanmarshall@greene.com	Add	39979616	2014-12-22	2018-04-14	Grow respond guy standard continue generation carry anyone. Left direction degree position around worker. Allow number allow the.
ifnWlCQOaWXrvWYoUEEj	ruizheather@freeman.com	Finally Over Oil Institution Exist	9626932	2015-03-30	2015-04-21	Which begin number. Specific task subject mother clear key describe financial.
OKcRTVAZJSZALvMDYJfT	wterry@jones.org	Light Modern	189079870	2011-06-28	2015-03-20	Fund opportunity author cold.
HLareEiBLeTUnZXWEpIX	flyons@wall.com	Board Difference Explain Prepare Discussion	71398376	2018-03-12	2018-03-21	Participant economy government message military. Sense result news these exactly thus enough early. Better as her everybody we dinner.
NMPJVAmQcfvYUASHEgXW	shelley15@moran.com	And Itself Project Financial Last	33599857	2014-10-13	2016-03-17	Property indeed than and.
PcFeZGIRhkBIqnrYntEf	thomaspeters@rodriguez.com	Also Individual Design International	119641752	2010-05-17	2014-03-31	Appear thank end try start. Our performance leader method cover.
eoqHjbtdpSStiuGMSUSy	kevinsalinas@potts.net	Art Argue Wear Because	32470219	2012-01-28	2012-06-12	Include continue much. Change certain notice front news out series article. Food station break ask marriage edge recent.
xHIvAMksUkFBUIdBQqvN	douglashatfield@decker-woodard.com	Increase Actually Imagine Write Election	28644070	2013-06-03	2016-03-04	Draw garden young they minute whether. Skill require each determine. Free white little scientist.
EvpICweHGVwoBlHfPWJo	lowetimothy@collier.biz	Wonder Its Finish Ask	176812996	2014-11-12	2017-10-03	Response five million even yes. If must across respond. Push student parent themselves.
adyRcMdlDPosUUAKdPBS	kevincannon@sims.biz	Sense Simple	103760595	2017-03-04	2018-02-26	Hand yeah must. Forward professor pay piece. Can friend for rate current support about indeed.
nvywxGyYZOxKqTKxmPgC	richardallison@morrow-jackson.net	Many Amount Wear Prepare	192559672	2015-09-25	2016-12-11	Two need of. Including forget light.
OKlYzauWrcXPDaoOlKlk	richardallison@morrow-jackson.net	Much Development Record Discover Image	119492165	2012-11-12	2016-01-05	Whatever interesting PM condition. Happen level hard agree. Final food step clear skin toward compare.
zmryMgzDUrkXLQAVOJNo	xenglish@mcdaniel.com	Prevent Almost	117511446	2014-10-26	2018-01-04	Feeling learn affect. Store officer rate low benefit author.
DYNGuYPYiypECqFUzLlG	sharonhensley@thomas.com	Huge Similar Staff Ask	28177461	2013-01-19	2014-07-14	Available final throughout play article one. Interesting stop over else in begin product. All test final piece same section those.
ROUkFAUjLhUzEjmNVzog	marktaylor@haynes.com	Example Actually Character	120436876	2013-02-20	2017-10-03	Notice rather create exist. Friend involve fire building later.
QzDCkvPPPWqLeLtWbrai	lowetimothy@collier.biz	Finish	143721217	2011-03-12	2014-09-04	Employee scene author him soon. Easy bad serious energy maybe.
EQrhBUUZPfgeOUHgCDAF	griffithwilliam@freeman.com	Drop	120969801	2012-02-12	2016-02-28	Then national these term economy magazine student. These technology enough red natural.
uoPuYqCBcMhbTxnPKCZI	ruizheather@freeman.com	Machine Think	64034347	2011-04-17	2012-01-06	With about so artist attorney upon. Develop about security before future information American.
UJxVjdYlmwWUdtWHndSp	griffithwilliam@freeman.com	Indicate Since Specific	39893887	2014-11-08	2016-01-16	Herself foot process but sense up. Seem minute PM while kind group. This left will painting.
jmsbrxJMPCQqVblisMSy	kendrapatterson@salinas.com	Cover Color Body	30411553	2010-01-16	2012-06-25	Space consider but away. War draw price those guy. Test morning attack eat box purpose.
yhKWyBHzFLCttDhcxgSY	jenningselizabeth@sweeney.com	Morning Play Catch Reflect	55421258	2017-11-16	2017-12-05	Attorney board parent lot generation nearly. Near five benefit new race play news. Everything across long without civil.
cXHZxCfUsiATFYBLQpwz	uhoward@thomas.net	According Impact Sister Control Deal	161793890	2016-09-27	2017-04-12	Hit than wear season paper. Commercial line represent notice arm should capital. Between table though much want maintain want.
zQSqlRqgwUQUSHWzMaPC	petersonmichael@miller-gray.com	Hour Raise Deep	59675033	2017-08-16	2018-02-28	Least understand lawyer relate painting up. Weight think change east form smile.
NZIVtgisIVXKyBSzrohq	shelley15@moran.com	Result Kitchen Body Within Guess	103329611	2012-05-31	2013-05-29	Visit strong which most interview prevent account. Listen civil bed school effect instead some. Seek throughout or stop authority human thousand.
MGjQebCOQZdTAbRaIunU	marktaylor@haynes.com	Painting Budget Audience	60183352	2011-09-12	2016-07-16	Learn forget in agreement past behind crime. Analysis force stock war business room available.
ZVBXKUtGYNoYXuwTDDSG	kendrapatterson@salinas.com	Rest Size	179664772	2012-11-14	2013-01-07	Administration read design use should. Thought project simple benefit.
iafJQkAFrLKbbjZQgAhI	petersonmichael@miller-gray.com	Attack Hundred Police Various	70598598	2010-10-24	2017-08-13	Catch generation field beautiful buy section save wear. Clear friend price. Hit middle inside degree.
lhjzGExcbZuijkFrpqik	marktaylor@haynes.com	Chance Build Staff Exist	132036900	2014-01-17	2017-02-13	Town low coach. Environment never music perhaps catch just type. Almost support black whether pay.
nvuSJTMZbyGQdlMHNDeT	richardallison@morrow-jackson.net	By	102347810	2014-10-02	2017-12-21	What half if statement under born same allow. None everybody trial age buy administration decade. While company make. Huge daughter strategy more.
ARIKBCBjjCFAhDzTijIe	kendrapatterson@salinas.com	Hotel	114265304	2011-06-25	2013-10-09	Skill cold reason. Four become authority especially firm memory.
lhMuerIPadCePhpTeneg	sarah68@fletcher.com	Trouble Support Activity Difficult Popular	69314852	2010-05-18	2016-06-27	Small kid notice meet idea sing. Easy behind wonder new me. Word condition form current power.
VfzzZfxDkOzMWpIilYka	marc24@lewis.org	Summer	101312282	2016-11-09	2017-09-10	Including address ground then technology management four. Rate however hot recognize federal election fly. For inside data this decade modern item.
zFIwxgogWVIEygrsuXip	lpotts@mayo.org	Form Employee	34067852	2016-12-16	2017-04-22	Miss own hit kid dark friend shoulder better. Husband board yeah.
ddrAQiTwNnYZcEWDlryS	flyons@wall.com	Low Choice	6270255	2017-01-08	2017-09-19	Meeting hospital material suggest during. Would method add.
aBodfVzEbxAXHdMZHRoi	lgarza@moreno-villarreal.com	Task Industry Little	62385013	2016-03-17	2016-07-01	Event woman skin media. Sea cut as.
gkTWLFDyyFhAADWbEFPy	jacquelinehood@bowman-lewis.biz	Yourself Hope	137936897	2011-10-30	2017-04-02	Forward war continue time talk subject owner own. Space whole enter method loss explain born his.
hocjMsgREeENhDiGCXSj	jenningselizabeth@sweeney.com	Describe Medical	67179766	2012-02-18	2012-12-21	Although direction my. Kid final gas police.
AArIhcRgghqWLjVrmrac	griffithwilliam@freeman.com	Right Important Tend Do	35837604	2011-01-26	2017-10-01	Type listen effort sound base I manager. Growth voice stuff recently discuss. Lay daughter step my career how.
dzEcdqljrzjAMVoNMQmg	ruizheather@freeman.com	Dream	24586557	2011-04-23	2017-08-02	Him happy style two morning citizen report. Director day around. Another end budget task agency guess.
AAQdVVvbIpxDsNoKQZaQ	lgarza@moreno-villarreal.com	Inside Year Significant Attention Hope	59362375	2011-05-15	2012-07-03	Work southern know eight media middle room.
DvyFjumsgrWqLhtXSXCR	jacquelinehood@bowman-lewis.biz	Such Woman Billion Policy Despite	79934474	2012-10-24	2012-12-23	Teacher strategy each grow receive. Tv million like pass.
tDCtxsxbkbnvgiozPqgJ	cindywong@clark-torres.biz	Something Employee Produce Them	52312727	2010-08-05	2011-03-31	Among even baby. Leg north heavy general after. Image stage lose firm operation responsibility.
poKStuXWZIWUhbxusAsU	flyons@wall.com	Than Start Strong	189549492	2012-06-06	2015-11-20	Free share card describe article. Nothing region eight least actually late Republican. Case event maintain question.
xWYZDAFHTIVNFHgCqafX	nguyenryan@peck.com	Concern Building My Administration	17972288	2010-05-19	2013-01-23	Eye exactly foot process. Case table second enjoy clear piece. Likely product friend candidate let.
SOiupDYJvVVoGEwqHdJG	petersonmichael@miller-gray.com	Fact Accept Than	191797927	2015-12-14	2017-07-09	Floor inside arrive news culture. Start card use when anything happen woman. Thought relate this.
qKDKHFASHwVwauTIEkUy	lpotts@mayo.org	He	177347634	2017-05-14	2017-09-25	Small young herself card impact.
AvVOMQHmAwnyLDqUlGmi	petersonmichael@miller-gray.com	Feel Cause Within	155265966	2018-03-08	2018-04-21	Eight loss light very full. Thank political everything threat order read. Bed itself question bit.
HPvCYLzMyBJwsdpAWKRx	cindywong@clark-torres.biz	Cup On	123977110	2016-04-12	2017-08-28	Item appear return.
IUZLSPyRKtvbRSwDEjSL	kendrapatterson@salinas.com	Arm Left Six	114457727	2012-10-19	2014-05-14	Little fine nice two security. Section skill thus trade. Include cell figure forget.
UFEOjVZpeCNcZnJlSKEB	michaelrogers@johnston-zavala.com	Couple Today Include May	5840313	2010-06-17	2017-06-10	Majority sure mean final ready series. Message leader view science want six. Sound never maybe item produce its actually economic.
mvZxnKSSpCOvkdVLIudW	marc24@lewis.org	Nothing Plan Deep	180757402	2011-06-15	2016-09-05	Best institution president become accept. Draw material radio. Just general she likely resource this.
OgkSyyMLxYuBPxODfwpP	griffithwilliam@freeman.com	Mention	57585911	2017-02-21	2017-07-21	Sense piece push experience last population executive. Watch old stock man religious.
SfrakAoNWzhaQbfROUxO	ruizheather@freeman.com	Agree	116637629	2017-04-07	2017-12-11	Hot capital Mr before wrong brother morning. At increase never top.
FNfawvTZbuvWFtfrRbGI	wterry@jones.org	Property	14526840	2016-09-06	2016-11-02	Set positive paper. Nice you through week finally.
NRlrFMuprhVcbVjDnFmX	griffithwilliam@freeman.com	Low Reason Thank Physical	172071494	2011-03-18	2016-10-27	Staff treatment plant any plant black. Leader evening grow tough head network painting. Issue sort sell school turn. Laugh fall resource course.
RqgeLdoTPECfjhPKcAUC	sharonhensley@thomas.com	Policy Bed Common Out Congress	79002926	2015-04-19	2017-06-08	Artist three thought impact high. Until whom network. Test suffer suddenly someone method.
XVrlsKyKVrHtiZuRNJXc	kevinsalinas@potts.net	Candidate Discuss Father	165970963	2015-01-10	2015-09-16	Same space hope health break while. Also because build identify. Dream house keep consider.
EfMgTTHjpbMbpSaLgKNa	trananthony@brown.com	Box Her Walk Drive	166461479	2011-07-24	2015-01-30	Bring anyone mention to course. All close total meeting fire effect operation area. Baby act develop.
\.


--
-- Data for Name: laporan_keuangan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.laporan_keuangan (organisasi, tgl_dibuat, total_pemasukan, total_pengeluaran, is_disetujui, rincian_pemasukan) FROM stdin;
jenningselizabeth@sweeney.com	2015-03-25	152929793	101468253	t	Say southern old listen me. Field stop camera quality commercial.
trananthony@brown.com	2015-11-16	160474893	37741696	t	Top how begin staff note set man. Available although near individual. Food service leave tough child culture process year.
kendrapatterson@salinas.com	2017-01-21	84829030	154190963	t	Evening president share effect change. Early military fall just. Mother start push center rest.
wterry@jones.org	2010-07-08	185244691	13095667	t	Garden test watch activity stand ok. Require act four must. Heavy among mention teach add.
petersonmichael@miller-gray.com	2015-01-27	92609735	18868117	f	Floor billion along mother. Open prove always suddenly official land.
petersonmichael@miller-gray.com	2013-01-08	172042082	15552698	f	Thought realize fund take available yard analysis. Movie end model down try college various. Wall kitchen resource serve.
lpotts@mayo.org	2017-05-24	1981946	95476301	f	Appear thank evidence blue once. Watch home especially stop.
richardallison@morrow-jackson.net	2013-02-04	140877739	35247598	f	Any wife peace nearly organization current assume economy. When situation nice.
kevinsalinas@potts.net	2013-08-10	66398047	4537189	f	Draw keep tree term candidate. Writer off son inside win since. Able what notice once organization with.
sarah68@fletcher.com	2012-02-24	169516075	9382068	t	Will doctor pretty entire. If discussion story pick collection drop. Six through down goal example job.
jacquelinehood@bowman-lewis.biz	2013-09-06	34692733	123276175	f	South option human among.
michaelrogers@johnston-zavala.com	2010-02-21	133941405	112023495	t	Particularly oil parent glass work officer box. Into company stage air culture something participant grow. Much simple wear certainly everybody try cut.
ronald25@fields.com	2010-02-09	83479934	164068379	t	Gas vote knowledge begin everything wonder. Skill citizen nature. Moment together college else chair not.
kevincannon@sims.biz	2013-01-02	158851091	70390023	t	Office institution member record good film. Organization consider nation hospital. Today air education remember.
jenningselizabeth@sweeney.com	2012-07-03	82114281	155916812	t	Final establish here level series movie soldier. Art ever through report.
lpotts@mayo.org	2017-02-26	17795819	36128062	t	First doctor design building himself top. Structure still election. Line difference leader until audience into amount.
uhoward@thomas.net	2014-11-01	6148374	11467458	f	Leg suggest wife program prove. Friend government find TV experience other sort. Culture different whole trip.
petersonmichael@miller-gray.com	2017-11-23	188748889	11916354	t	Fast difficult mean customer issue hit state program.
trananthony@brown.com	2011-06-05	24323766	17078888	f	Including health until college hard Mrs threat. Area early hold race spring rest prove. Friend oil control of store mother.
sharonhensley@thomas.com	2010-12-03	107270535	97095279	f	American window marriage citizen degree. Throw if thus number mean close growth. Service deal treatment argue.
sarah68@fletcher.com	2015-01-12	68346996	167459616	f	Task little style want expect soldier. Data hotel scene step morning pick good door. Whom station guess night brother. Serve keep east interview.
cindywong@clark-torres.biz	2012-02-03	49303631	127273740	f	Edge work true push win. Attorney doctor community mind.
michaelrogers@johnston-zavala.com	2013-09-16	164199308	7785973	t	Guess sound tree former. Range national just home agreement. Choose stay store hope full window oil take. Professor institution style himself raise.
xenglish@mcdaniel.com	2013-01-20	104442388	77566963	t	Available argue own report stuff business. Leave special would sit course including feeling change.
uhoward@thomas.net	2016-04-29	194171756	184341659	t	We deal employee check respond yourself. Ready however agree yourself program pretty. Second have dark church town bed democratic.
kevinsalinas@potts.net	2013-12-20	97838697	152643656	f	World each be power. Quality year see quality box marriage statement upon. Each three kitchen far.
sarah68@fletcher.com	2018-02-11	61838105	194153190	t	Whom particularly final. Whether chance produce final recent soon government. Include action question for build sound expect.
kendrapatterson@salinas.com	2013-09-24	49591683	155001150	t	On popular court. Main federal at method amount explain attack.
trananthony@brown.com	2016-11-23	105949044	164601951	t	Maintain reveal rate country know. Wind simply system three hard model million. Over policy year.
sharonhensley@thomas.com	2013-02-07	70835478	26155179	f	Example least fill world street very brother. Conference Mr treatment stay first sister amount.
lpotts@mayo.org	2016-06-16	98567799	41331077	f	Whose election simple. Player either either decide a. Me change hour I.
marktaylor@haynes.com	2017-09-23	23807293	15718543	t	Civil response stay technology really yourself. Between hour appear will and car term.
wterry@jones.org	2012-07-17	122647393	161357844	f	Remember most wife fund skill long old.
lgarza@moreno-villarreal.com	2016-06-28	150548602	152731632	t	South reveal him sea room. Dark table popular.
sharonhensley@thomas.com	2010-10-20	149803226	29389952	f	Window others although send sign mean. Listen prove raise employee represent president around social. Old couple want like be agent.
lgarza@moreno-villarreal.com	2017-12-20	37255695	138263665	t	Or position to position break. Risk wonder area generation help.
lpotts@mayo.org	2017-11-15	132179230	152878375	f	Allow never word early wish little Mr. Black eat ground baby none picture but.
marc24@lewis.org	2012-08-17	74240776	26250642	f	Example property claim grow treatment early sign. According including machine adult education open start. Style miss thousand Mrs opportunity break. Scene thousand opportunity impact.
richardallison@morrow-jackson.net	2017-11-18	139920066	159659555	f	Mouth here front happy possible wrong performance job. Whether company some above moment part. Not sure none.
flyons@wall.com	2011-11-01	105973566	117487340	f	Matter film body amount. Reflect treatment together skill trade throw exactly.
kevincannon@sims.biz	2014-11-28	92749702	102063468	t	Hair accept else safe. Many either apply week anyone somebody. Lose night blue south kind think hard.
douglashatfield@decker-woodard.com	2013-07-29	36763679	40697871	f	Speech fight can federal. Party pattern south later crime yourself.
ronald25@fields.com	2017-12-15	76345350	139517361	t	Leave source main class employee bill. School meet deal team decision rise.
sharonhensley@thomas.com	2017-09-01	176985537	199759193	t	More writer religious threat manager along century. Inside huge place upon since since. Whether person cause face.
lgarza@moreno-villarreal.com	2016-12-04	80125771	70270912	t	Phone continue Mr yet per look. Teacher special say better kind.
uhoward@thomas.net	2016-06-25	42681045	179126069	t	Side particular number reveal. My chance surface example state guess bed. Support result believe authority book off price.
lowetimothy@collier.biz	2016-08-10	36466731	77253162	f	Fight although stock local conference way result. Leader government moment toward. International than put beyond.
michaelrogers@johnston-zavala.com	2013-09-09	3601764	192684363	f	Above protect pressure wind. Reality sign write history. These situation music window.
xenglish@mcdaniel.com	2016-06-28	102436670	88362710	t	Concern spring get on add Mrs career. Yeah prepare country. Feel throw easy quickly community follow situation. Pass choose teacher institution face along husband.
ronald25@fields.com	2011-04-17	76535091	123494949	f	Hard particularly or energy little thing kitchen. Still PM nature business law. Spring fish generation natural.
uhoward@thomas.net	2013-08-23	147908010	78328016	t	Administration skin create provide continue door daughter service. Very paper technology issue lawyer available.
wterry@jones.org	2013-12-01	171977983	190004045	t	Population into when tonight person stock. True movie key clearly important. Hit blood hit on understand but.
uhoward@thomas.net	2017-10-04	161086111	82519665	f	Church similar wrong help kitchen.
lpotts@mayo.org	2015-08-20	44783995	147939204	t	Work positive land figure develop owner this relate. Standard relate so. The challenge somebody far church turn federal.
richardallison@morrow-jackson.net	2010-03-23	96114143	124289315	t	Chance open drive meeting increase.
kevinsalinas@potts.net	2018-01-21	138401338	6309583	f	Game interesting serious few. Just car bit thank. Which general surface authority blood other.
kevinsalinas@potts.net	2016-04-28	44789119	156085909	f	Military final yeah.
petersonmichael@miller-gray.com	2014-05-28	123905544	31595445	t	First investment pick side thousand book continue lose. Side husband short fire. Far wall loss couple.
wterry@jones.org	2016-02-13	18949153	37710709	t	Resource analysis pressure morning reflect control clearly. Color year care collection. Manager direction idea month.
petersonmichael@miller-gray.com	2014-03-05	151623303	133941302	f	From significant political collection anything through. Room local sort network.
jacquelinehood@bowman-lewis.biz	2011-09-22	104800358	163082136	f	Company receive ahead. Us feel act what notice room. Yourself those car little build social.
petersonmichael@miller-gray.com	2016-08-24	141943917	10425433	t	Run resource ever see. Factor throughout statement condition situation. Work democratic central inside.
jacquelinehood@bowman-lewis.biz	2015-12-09	146472730	87914481	t	Truth movie figure tree another state. Me score drop need already peace door.
griffithwilliam@freeman.com	2017-05-05	51844558	3081706	t	Tend quality away response stand my whether education. Music deal produce better. Audience the become follow or.
thomaspeters@rodriguez.com	2012-09-30	106513216	92799157	f	Likely imagine easy investment. Mention civil exactly whose. Church environmental subject sort high.
ruizheather@freeman.com	2012-02-27	62598022	111854104	f	Past will last loss save. Police reflect soldier drug.
cindywong@clark-torres.biz	2012-03-06	195834681	137232789	t	Show individual cultural charge hair while get. Here forward money buy so score. Act place commercial later.
marc24@lewis.org	2013-06-07	125205691	169364285	f	Result political value TV miss civil according. Class year former throw report. Interest far fight capital.
jenningselizabeth@sweeney.com	2012-09-21	8290422	3918865	t	Society food recognize. Visit box nation hold foot.
thomaspeters@rodriguez.com	2015-03-28	189122977	26013124	f	Production contain provide help clear morning. Anyone maybe professional allow. Girl white ask single interview million.
douglashatfield@decker-woodard.com	2014-06-08	22573824	190682699	t	Them heart my general compare.
thomaspeters@rodriguez.com	2012-07-05	43462636	121930195	t	Break guess institution audience.
michaelrogers@johnston-zavala.com	2015-02-17	76328995	115326391	t	Animal event respond also music. Stage wall religious.
jacquelinehood@bowman-lewis.biz	2014-03-17	18672555	179043822	t	How threat group general drop. Heart government leave news fast cell.
jacquelinehood@bowman-lewis.biz	2013-11-28	170345679	86918386	f	Second pattern or still carry. Beat idea our. Huge popular about them might.
kevincannon@sims.biz	2010-09-21	191068678	187533703	t	Himself might example like set PM focus.
shelley15@moran.com	2011-05-03	18504906	167656324	t	Your ok range vote report wife industry. Six represent response fly. Allow official hot record for wrong trouble.
nguyenryan@peck.com	2011-10-27	130818843	128614118	t	Dream people after guy social white. Knowledge family international. Rock detail almost thank poor.
nguyenryan@peck.com	2014-06-30	107659053	38501681	t	Play arm seven evening we. Score executive carry consumer plant. Foreign personal maintain west when.
ruizheather@freeman.com	2012-01-22	147373242	79464116	t	Compare pressure no long citizen democratic. Let create few decade phone national. Film poor pull accept treat.
douglashatfield@decker-woodard.com	2011-04-14	165935206	158873035	t	Term friend old away something activity international. Decide white tell another half instead staff. Certain million sign bar southern.
flyons@wall.com	2012-09-21	33405325	159387127	t	Meet smile why kitchen growth you politics. Give food base student democratic. Bag measure way fire.
jenningselizabeth@sweeney.com	2015-04-07	147473577	172816962	f	Impact amount direction evidence through. Explain community people answer arrive. Water each region same thank.
lpotts@mayo.org	2010-07-27	88553535	105975852	t	Property stop cut century sell if adult. Movement point hundred speak billion dream according. Gas position often.
xenglish@mcdaniel.com	2012-05-06	29130436	191350675	t	Discussion quality half smile side. Present same bank environmental.
kendrapatterson@salinas.com	2014-08-15	51935654	104573049	t	Fire culture sort contain part sort. Question deep bank able.
ryanmarshall@greene.com	2014-02-10	103081092	54304280	f	Top side nice response candidate very. Direction detail medical first him.
griffithwilliam@freeman.com	2011-12-15	70130291	70729056	t	Prepare eat loss evening total kitchen after. Case current participant same. Majority by article talk people stuff. Seat tell because western where early eye guess.
richardallison@morrow-jackson.net	2010-06-18	133290595	83435529	f	Change stay until policy. Ball away lawyer sign hundred. Benefit hit each indeed measure surface director.
nguyenryan@peck.com	2010-05-06	150710361	82184131	f	Yeah occur event poor. Clear road voice up myself clearly analysis. Simple challenge space.
kevinsalinas@potts.net	2014-02-01	70474610	177324602	t	Prove future along bit chair. Crime left your door eye peace.
kendrapatterson@salinas.com	2015-07-08	146674939	21409339	f	Too major teach themselves. Example show vote stand. Perhaps interest and message behavior else already.
marktaylor@haynes.com	2014-08-01	59912899	90936155	f	Table space always piece must suggest image yard. Save begin build official training without.
richardallison@morrow-jackson.net	2011-03-03	92783374	160373480	f	Appear paper inside determine everyone them cover budget. Large to could unit enjoy not page.
lowetimothy@collier.biz	2014-11-01	14734103	146632025	f	Score each trip public home space change. Take product crime early turn.
sharonhensley@thomas.com	2010-10-08	146664673	140701773	f	Those five consumer crime send cut last. Degree top political letter story receive.
ryanmarshall@greene.com	2011-10-30	9760660	48042641	f	Beyond senior tax according. Too method dog western cause rule although almost. Sea popular husband piece.
xenglish@mcdaniel.com	2013-12-30	148955512	117016499	f	Will story world explain eight serve decision. North cold east heavy. Knowledge moment exist reflect manager.
jennifermedina@edwards.com	2016-08-09	167427421	21022724	t	Population east ready. Above eye work five fish my throughout. Natural property music heavy low room candidate.
marktaylor@haynes.com	2013-05-31	77716904	92967062	f	Blue work majority blue man husband there. In reflect method simple start. Wait member direction understand.
ronald25@fields.com	2010-05-26	99570472	30247566	f	Miss reveal a. Teach agree song surface until way stay. Particularly color hundred help trade. Ask which but compare move.
kevincannon@sims.biz	2013-06-16	56214981	133903310	t	Thousand message bad miss that. All born who painting. Size success never.
sharonhensley@thomas.com	2017-09-10	53993917	180262972	t	When audience avoid test analysis. Others gas church reality anything food. Window million piece beyond.
nguyenryan@peck.com	2014-02-10	159198750	182945800	f	Provide also ahead relationship fly. Employee positive another southern.
trananthony@brown.com	2011-03-31	133009855	89348256	t	Eight claim no since catch. Before if natural figure financial allow fly movement.
shelley15@moran.com	2014-05-09	85670452	156997531	t	Them support concern interesting interview.
sarah68@fletcher.com	2010-06-24	51069669	958270	f	Chance during under phone most. Customer approach week phone degree one computer.
xenglish@mcdaniel.com	2016-03-17	92464006	73990596	t	Fish southern water arm. One really third economic bill radio.
uhoward@thomas.net	2010-01-21	50099538	889941	f	Explain six life green entire miss popular tough. Activity tree sense example game. Assume old live material.
lowetimothy@collier.biz	2018-03-21	53515860	60650610	f	Debate home environmental evening. Speak blue arm produce animal.
xenglish@mcdaniel.com	2015-05-06	157647568	27090056	t	Nation maintain clearly wrong alone nearly stage receive. Expect writer arrive wide benefit note thank production. Could shake direction choice space candidate window protect.
michaelrogers@johnston-zavala.com	2017-10-25	50441649	170088274	t	Third large almost stage very data building. Even race store maintain partner. Let product save size produce.
marktaylor@haynes.com	2016-09-17	144411111	43027147	t	Standard budget as industry data perhaps. Without start worry adult magazine push according. Themselves add least.
lgarza@moreno-villarreal.com	2012-08-18	90635583	31767964	t	Task thank than. Relationship audience produce activity green camera address.
marktaylor@haynes.com	2015-04-05	146614294	24073188	f	Level yes campaign save road arm.
xenglish@mcdaniel.com	2012-12-19	83680465	74736508	t	Administration raise everything quality. Difficult hospital little democratic campaign heavy. Audience find require. Drive affect why just.
jacquelinehood@bowman-lewis.biz	2010-05-05	155504990	87661314	f	Anything learn back. Weight age young.
douglashatfield@decker-woodard.com	2014-07-06	61935420	25029905	f	Where actually sister charge direction prove. Individual price forward prove foot.
jenningselizabeth@sweeney.com	2014-03-01	196670727	58103351	t	Meet century us fast toward herself already. Provide international quality believe contain.
kevinsalinas@potts.net	2018-03-27	170956617	62906793	t	Wind poor away husband fast. Third boy table customer coach cover physical.
ruizheather@freeman.com	2016-03-24	44227710	42010548	f	Western relate most now boy behind. Trip five player read trade.
uhoward@thomas.net	2010-07-16	82407577	116687645	f	Pay hear account store though daughter. Baby fill pay support these effect culture fact.
sarah68@fletcher.com	2018-01-06	24107197	199620007	f	Student Republican bar find author four policy view. Administration against difficult soon. Parent statement million base majority fire.
lgarza@moreno-villarreal.com	2011-01-12	41012799	80575018	t	Offer media know effect. Total base rich. Area impact investment single consumer moment wish.
griffithwilliam@freeman.com	2018-01-24	145887291	138136530	t	Ever push book method five reflect carry. Affect feel performance cold idea draw. Participant pull event country any people thus mention. New water few officer set begin avoid.
michaelrogers@johnston-zavala.com	2013-04-09	23069271	101085040	f	Nature middle weight big. Figure play place. Matter interesting third man policy investment.
shelley15@moran.com	2015-02-22	98371138	38494902	t	First night about rock generation between sure treatment. Morning hair home trade election buy. Society pretty reduce strong foreign.
kevincannon@sims.biz	2014-08-08	159401332	108681102	t	Night top personal a rather drive. Five night down prevent energy mission world.
sharonhensley@thomas.com	2011-03-15	166934667	9311305	f	Bar of either early research. Part wish its happy new. Sell record us follow.
thomaspeters@rodriguez.com	2012-12-26	169996562	27173778	t	Degree administration more year. Best these catch defense color measure middle. Happen week too.
lgarza@moreno-villarreal.com	2013-04-24	131335727	117980138	t	Team good pay analysis speech. Card mind nature itself production.
marc24@lewis.org	2013-11-14	8755964	8199575	t	Nation church group mention. Result commercial section military win industry hold serve.
lpotts@mayo.org	2015-04-16	178113783	132542550	t	Military television suggest along democratic sit. But street opportunity suffer work interest foot. Entire benefit us why.
marc24@lewis.org	2013-06-05	195129197	197232204	f	Take order behind. Development body north. Way Mr when difficult technology happy animal.
ronald25@fields.com	2014-10-25	85288728	105675190	t	Leg side another various quality magazine product. Not kind fund star treat. Think line learn will gas bring.
sharonhensley@thomas.com	2016-05-01	47400747	100851614	f	Nice no agreement can beautiful. Carry address final but just official move. Walk result responsibility owner possible usually south.
nguyenryan@peck.com	2012-07-16	69193787	6221523	t	House form task authority between if. Available door produce fact.
kevinsalinas@potts.net	2014-08-06	195374665	75786610	f	Many voice since travel.
kendrapatterson@salinas.com	2011-02-12	82743805	167228738	t	Maybe never get name. According there hour recently drop range. Concern business serious industry certain.
sarah68@fletcher.com	2010-12-10	30562858	95576716	t	Any challenge make especially in. Behind and quality task. Discussion increase thank attack writer generation structure.
cindywong@clark-torres.biz	2010-11-13	166064551	56207218	f	Them find no. Another top local offer manager reality.
thomaspeters@rodriguez.com	2012-06-24	98052945	147050235	t	Idea team themselves senior voice. Couple including deal shake run. Carry together that check half. Need section indicate sound fill practice.
cindywong@clark-torres.biz	2016-03-17	186440173	16430528	f	Business wrong lead director real whatever structure. Interest evening grow wish speech green.
xenglish@mcdaniel.com	2010-08-31	198453464	26828565	t	Next front Mrs she. Statement low send.
kevincannon@sims.biz	2013-03-15	18031442	175281859	f	Executive north best voice least outside practice. Rather young relationship southern right sit.
kendrapatterson@salinas.com	2010-06-20	199222348	135829973	t	Become color major. Reason without report set center. Who else fear who sense design.
sharonhensley@thomas.com	2017-01-23	135776605	53265377	t	Network executive affect claim scientist. Career environmental service seven. Among range daughter today. Play save draw military some series bit.
petersonmichael@miller-gray.com	2011-01-13	49852601	97893001	t	Painting central every though support them. Gun section east summer including job break. Science challenge outside whether total.
lowetimothy@collier.biz	2017-04-28	52576812	17103810	t	Describe enjoy data name. Life thousand college black doctor. Worry itself fire learn ball policy.
petersonmichael@miller-gray.com	2011-05-20	85059136	115864740	f	Culture responsibility leg sense analysis mission seek.
lgarza@moreno-villarreal.com	2018-04-20	1574960	97312361	t	Arrive participant carry professor affect less. Other study set across opportunity six.
lowetimothy@collier.biz	2010-02-25	153486087	40950804	f	Nearly rock father none former. Fly experience trouble book head.
ryanmarshall@greene.com	2010-06-03	22271009	162658035	f	Pick wish offer sister film little billion. Feel create writer knowledge. Minute rock deal yet operation member.
lowetimothy@collier.biz	2012-07-26	163709935	197528937	t	Nearly friend third second last wait. Tax perform win threat give discuss heavy.
kevincannon@sims.biz	2010-09-11	182762459	7857320	t	Husband quite join per. Computer book event run its through.
shelley15@moran.com	2011-06-21	38717557	92926820	t	Effect side give approach. Art end only pick herself boy. Price phone magazine charge hand commercial natural.
ryanmarshall@greene.com	2010-11-05	164279244	9035482	f	When among human common should. Wonder cut star effort whole.
kevincannon@sims.biz	2014-09-25	56929483	1126250	f	Exactly war take smile produce network world. Person media plan interest. Far physical recent institution apply cover create of.
michaelrogers@johnston-zavala.com	2012-11-04	167111831	17074236	t	Believe help or hit. Behavior oil significant talk power including entire. Pass remember order several. Administration become wall space job yes include.
marktaylor@haynes.com	2017-10-27	79583426	185373839	f	Quickly may business allow ahead career. Material which light miss think.
kendrapatterson@salinas.com	2013-11-25	153785741	102154397	t	Than station into finally. Explain provide field him market.
petersonmichael@miller-gray.com	2014-03-20	51817213	46742463	f	Article thing fire form should be.
uhoward@thomas.net	2010-03-22	191828784	134623983	f	Force industry message every. Onto nice reason bill go.
ronald25@fields.com	2010-08-22	132394762	188625676	t	Paper market alone skin participant foot research. Social lot time wife by be sport. Store choose indeed land see.
jacquelinehood@bowman-lewis.biz	2016-08-13	191230323	20218471	f	First record debate often message increase. Character go it read. Administration enough order seek draw sister.
petersonmichael@miller-gray.com	2017-11-01	12253661	96954497	t	Fire view human only. Itself still between western part.
ruizheather@freeman.com	2018-02-25	140663403	164578161	t	Eye relationship detail election enough. Art soon movie shoulder suggest ever.
kendrapatterson@salinas.com	2015-07-31	110968055	63225129	t	Everyone reason far station. Church me present father military order.
marc24@lewis.org	2016-09-14	65747388	180319719	f	Race defense pressure in part mission walk. Anything common style public level treat.
griffithwilliam@freeman.com	2017-09-09	130316740	162989429	t	Career nice have manager truth factor.
ronald25@fields.com	2010-10-22	110631317	156634342	f	And respond camera yard whom eye. Check bill season kitchen leave wonder. Scientist happy recent throughout particularly rich money.
marktaylor@haynes.com	2011-06-12	69996525	46427222	f	Raise several according major paper pick indicate.
uhoward@thomas.net	2015-05-04	60391694	21723714	t	Senior able let truth claim. Myself economy mind degree. Once billion the Republican push body nature.
lpotts@mayo.org	2016-09-08	130899339	78084358	f	Toward here other and mention reflect. Explain out despite care. We agent foreign may key cell agreement.
griffithwilliam@freeman.com	2016-06-19	75490111	161538847	f	World do the place fish. Recognize indeed expect also process spring doctor.
flyons@wall.com	2011-03-27	90922378	176599029	t	Participant to decade walk. Light remain environmental could.
flyons@wall.com	2014-10-10	144903682	38358703	t	Past rate general floor education system. Likely town rise do tend gas capital. Large well current first development young though.
sharonhensley@thomas.com	2017-02-12	189254435	63441504	t	Grow next build here money throughout building watch.
richardallison@morrow-jackson.net	2012-06-29	88135335	92316060	t	Now deep smile guy throughout.
douglashatfield@decker-woodard.com	2016-02-06	79804353	48550603	t	Feel federal others tend. Yard system wait story child body. White education give race positive national.
jenningselizabeth@sweeney.com	2016-04-14	111612699	102759245	t	Today effort budget cover. Morning pull outside and. Gas message appear next event personal.
jacquelinehood@bowman-lewis.biz	2013-01-06	112345005	110910844	f	Street ready various describe. Even professional yard possible million notice spring generation. Such in skill behavior.
michaelrogers@johnston-zavala.com	2011-09-26	59426154	73529415	t	Mind once doctor machine still social. Near itself describe too all voice realize.
thomaspeters@rodriguez.com	2014-02-07	163969091	33772077	f	Heavy budget group strategy miss move wear family. Energy another realize.
ryanmarshall@greene.com	2016-03-17	45195243	193564588	t	Image always despite test later hundred travel. Remember fast get suggest suddenly.
douglashatfield@decker-woodard.com	2010-12-15	100057301	79689112	t	Sound tonight same across. Which notice administration. Back effect best name left purpose may.
jenningselizabeth@sweeney.com	2014-12-29	136007915	131660521	t	Material nation whether movie hot first letter hand. Good follow difference those.
shelley15@moran.com	2013-09-13	83854607	96822166	f	Simply door necessary fight hot. Tv everything address hotel coach both. Better interview agree identify result.
ryanmarshall@greene.com	2013-09-18	129178414	52860779	f	Could raise mission technology bag thank. Without range skin lead government. Forget think yet leg staff head.
kendrapatterson@salinas.com	2018-02-06	70702062	145951045	t	Purpose whatever last meet. Lawyer gun simply watch organization end.
sarah68@fletcher.com	2011-12-18	70126881	132889037	f	Past music yet scene. National away growth. Change deep boy situation left alone.
kendrapatterson@salinas.com	2015-07-11	23782274	148769774	f	Organization produce sport upon. Morning theory control through determine new.
richardallison@morrow-jackson.net	2010-01-15	61444562	25132314	f	Continue respond adult particular drug position since. Second have whom too win.
kendrapatterson@salinas.com	2011-06-11	173736604	132180088	t	Goal whom example anything. Technology none early who.
jacquelinehood@bowman-lewis.biz	2015-12-02	112305783	169010218	t	Page south family step what pick staff military. Join economy structure field which force while culture.
griffithwilliam@freeman.com	2016-06-14	113935211	24398640	t	Half about might week turn before. Trial heavy because serious they tonight least back.
jenningselizabeth@sweeney.com	2012-12-16	190249053	123542148	t	Government remain finish economy.
kendrapatterson@salinas.com	2017-05-20	171901790	87756729	f	Administration movie gas similar role under.
ruizheather@freeman.com	2015-12-21	97108796	152619887	f	Laugh before approach pressure. Rock maintain myself collection teacher up.
marktaylor@haynes.com	2010-06-02	74036265	97669155	t	Local beyond miss too tax. Matter do community recently father free ready. Director at administration field true charge able.
wterry@jones.org	2015-12-08	7160127	99703810	f	Save lose stuff final meeting their. Alone us charge family wish. Take chance unit appear to cell road wrong.
douglashatfield@decker-woodard.com	2013-06-14	46445539	100165161	t	Perform hold group environment instead back anything. News later natural.
marktaylor@haynes.com	2016-02-15	194082616	139180174	f	Stay parent win probably. Practice movie compare Republican factor according four.
jacquelinehood@bowman-lewis.biz	2017-06-14	192757551	179530548	t	Heart president hand hospital thought quite. South mention but protect everyone. Card relate your evidence. Someone remember fact operation agreement concern.
shelley15@moran.com	2017-04-30	87233476	120273253	t	Current growth president parent hear local focus.
shelley15@moran.com	2015-03-04	165345242	136506497	t	Congress partner bring up into probably safe. Peace modern show imagine factor newspaper. Trade discover hand program reason.
kendrapatterson@salinas.com	2014-01-25	185432966	180435208	f	Per want manager total turn than. Development course public visit. Wall ready whose pass draw kitchen house. Especially child around day.
kevincannon@sims.biz	2011-06-28	111881402	123893379	t	Pressure assume message. Expect window choice on shake side. Purpose become give.
jennifermedina@edwards.com	2015-02-16	43329507	164710710	t	Form receive to nor player. Meeting worker history. Whole top compare quickly voice require.
lowetimothy@collier.biz	2010-05-17	64839386	96241793	t	Special animal personal south husband simple design. Simple not cost green your system.
jacquelinehood@bowman-lewis.biz	2013-10-02	145815732	69583624	f	Movie discussion window themselves movie firm put. General available can drug top seat star report. Rich drive pretty tell girl toward film.
marc24@lewis.org	2011-10-11	165291873	22449453	f	Oil sure again room easy art human. Concern these approach ever. Buy strong author line certainly ten PM.
lpotts@mayo.org	2015-07-03	40216914	96525174	t	There reflect little send. Few especially firm suffer. Movement feel shoulder standard either wall memory. Field and event.
ronald25@fields.com	2010-06-16	48215416	85394699	f	Customer right network employee still pass eat. Them resource college think mind.
ronald25@fields.com	2013-12-26	58995987	124405837	f	Order concern reduce security. Daughter with difficult perform old whose top. Fast board realize still senior song carry.
kevincannon@sims.biz	2018-03-16	29350554	131197693	t	Within foot claim view reveal history. When dark box include.
jacquelinehood@bowman-lewis.biz	2012-01-03	93190131	189579249	f	Center bill story newspaper claim edge explain. Follow country cup rock line. Upon put statement whether work appear.
douglashatfield@decker-woodard.com	2016-02-12	179858867	197792278	t	Professional entire purpose. Difficult recently law condition drug.
griffithwilliam@freeman.com	2012-01-23	63440309	171515169	f	Bill artist either school executive almost. Put true special official.
flyons@wall.com	2015-07-04	123613786	144926313	t	Alone share baby four. Anyone discussion health affect thank figure control. Computer hundred entire. Ground tax voice pattern money couple.
nguyenryan@peck.com	2013-05-07	169959714	112083741	t	Gas spend interest cover beat why. Society character special list. Example discuss character beat. Build consumer similar forward would.
kendrapatterson@salinas.com	2013-02-09	9101031	160242981	f	Old color order such let price theory.
wterry@jones.org	2015-07-09	23998353	113833261	t	Again different station foreign responsibility risk. Interesting third mother most.
uhoward@thomas.net	2016-06-27	70901088	148889697	t	Far especially commercial chance body. Brother discussion shake heart bar significant green explain.
uhoward@thomas.net	2012-07-31	10098021	66117931	t	Customer information account now. So fast everything do think.
marktaylor@haynes.com	2011-06-23	4433046	121747010	f	Know method president open worker about. Hospital human example someone.
cindywong@clark-torres.biz	2015-09-25	151766279	185474283	f	Animal exactly leg record federal evening here. Hope bring political late position eight good. Generation Congress blood.
jennifermedina@edwards.com	2016-04-10	107103369	61365522	t	Fear fall institution maintain right.
jacquelinehood@bowman-lewis.biz	2012-05-27	58843181	17038264	f	Economic site the near sometimes on account. Experience believe guess increase. From bed air know wonder morning support.
nguyenryan@peck.com	2016-11-16	36643792	158980857	f	Whose onto charge career.
sarah68@fletcher.com	2015-01-18	43193005	38742492	t	Air attention nearly the huge along. Positive can stage help.
marc24@lewis.org	2014-04-14	110210054	48787305	t	Evening operation cause idea either. Thought seven song fund learn. Understand science out music would.
ruizheather@freeman.com	2016-06-21	113025904	81094347	t	Set job whom clearly imagine. Later art trial police. Want save finish mother teach six already.
cindywong@clark-torres.biz	2014-10-23	127057463	145366237	f	Talk democratic control someone affect system voice sense. Condition page partner right either church head.
petersonmichael@miller-gray.com	2018-02-06	31698643	189266057	t	Western main shake occur respond. Range young Congress.
lpotts@mayo.org	2013-08-02	64184582	105657638	t	Finally let last hold foot. School imagine citizen PM require small shoulder.
jennifermedina@edwards.com	2013-11-24	132004551	38377561	f	Not pass while could choice camera country. Under time environment better.
uhoward@thomas.net	2014-09-10	60065755	174669675	f	Collection political difference prepare agreement move send.
nguyenryan@peck.com	2015-04-16	109450596	156427182	f	Read lose design south. Perhaps certainly great herself field career. Side stay science company exactly woman.
trananthony@brown.com	2015-09-25	77665044	37392958	f	Thus hand let myself total voice. Party technology I outside official teach small area. Reflect ask available fast drop.
michaelrogers@johnston-zavala.com	2013-12-26	160790219	188612865	f	Wife try discuss good. To forward join bar. Catch though return interesting simple. Father military be lot page.
nguyenryan@peck.com	2015-07-05	151526975	20319926	f	Especially per its see. Rock which defense school win. Ready million reflect late.
jennifermedina@edwards.com	2012-06-14	34057144	176724173	t	Which decade someone according nation anyone have. Road any teacher fact million agreement once.
kevincannon@sims.biz	2016-11-18	8137724	195404197	f	Space then big around bit. Idea control stop voice. Push feel produce own challenge.
lpotts@mayo.org	2012-05-10	187926377	64666314	t	Pressure everyone player class trouble hot miss. Help bank citizen. Manager billion plant million.
wterry@jones.org	2015-03-17	9416306	98448041	t	Natural join mind just give then about just. Assume author everything eat operation above record drop. Affect face increase sense conference less.
lowetimothy@collier.biz	2015-10-21	122219813	54670286	f	Perhaps eye wear read day pressure surface. Until land air hospital.
shelley15@moran.com	2017-10-28	161962845	136090182	t	Difference cut there put already. Whether technology develop position ok sense. Reflect behind beautiful western less recently task.
ruizheather@freeman.com	2013-11-23	52166092	22430473	f	Kitchen mother coach involve page hour receive. By tax better own around ground foot. Year upon do want stop bring today.
michaelrogers@johnston-zavala.com	2017-08-18	41400696	32946938	f	Car before reflect support away seven Republican.
thomaspeters@rodriguez.com	2012-08-27	130879488	110246701	f	People reach suffer federal agreement child opportunity short. Standard interesting heavy prevent economy its thank quality. Early change travel.
marktaylor@haynes.com	2017-09-29	188063483	89147245	f	Time future she beautiful lawyer church. Say street security kitchen them develop.
ryanmarshall@greene.com	2012-07-26	60864264	161393732	f	North suddenly nor money. Rather main reflect health court wide. Address themselves detail place recent hotel buy.
uhoward@thomas.net	2016-01-27	32576384	43332598	f	Charge do probably turn. Executive service include response money democratic discussion.
wterry@jones.org	2015-03-26	88251960	30354917	t	Professor recently six. See still campaign too themselves. Military human field read those floor.
cindywong@clark-torres.biz	2013-08-31	164491405	91184946	t	Claim thank compare there vote. Total least compare character.
marktaylor@haynes.com	2018-01-16	128109793	172355167	f	Character check recent agency ten. Owner happy allow respond. Would information action garden arm detail film.
kendrapatterson@salinas.com	2014-03-03	8766110	186395699	f	Size rise ten allow explain structure. Wait state itself true I method. Well tough way why firm argue support.
jennifermedina@edwards.com	2014-02-26	99497422	134749170	f	Near area forward concern.
lowetimothy@collier.biz	2014-02-03	3221860	88314660	t	Democratic adult doctor language seem near. Specific since continue just send perhaps radio.
jacquelinehood@bowman-lewis.biz	2016-09-14	18030870	82259433	t	Take simply Mr. Main music quite two practice western. Threat possible mind again. Interesting point interest mean course third cover garden.
kevincannon@sims.biz	2010-05-31	138171600	133227059	f	Memory example art floor true Mrs. Usually huge young recent. Perform subject strong recently much town.
thomaspeters@rodriguez.com	2013-12-04	81752150	31559677	f	Attorney quality either south others.
ryanmarshall@greene.com	2015-10-26	146135360	297101	f	Go risk budget. Store new value field quite budget.
jenningselizabeth@sweeney.com	2015-10-09	58959015	7872972	f	Must television why wind class guess. Arm wide often store should culture.
trananthony@brown.com	2017-06-21	11920979	157046763	f	Decade wide because because member day miss.
kevincannon@sims.biz	2013-09-22	94661360	185616819	f	Space step inside beat. I group perhaps beautiful.
sharonhensley@thomas.com	2012-09-07	167397235	4738530	f	Range box effect short. General his stop large music.
ruizheather@freeman.com	2016-01-30	172793221	12186285	t	Whatever quite loss hospital material perhaps war. Move boy live respond service network community. Prevent spring step myself detail.
jacquelinehood@bowman-lewis.biz	2015-08-08	118315830	49389650	f	Project this hit line treatment really population often.
griffithwilliam@freeman.com	2011-10-17	171009792	33102815	t	Play office analysis half loss indicate. Friend once board we as approach. Chair better brother personal concern opportunity what friend.
thomaspeters@rodriguez.com	2014-12-17	175504575	55194998	t	Drop marriage future war. Theory small such apply owner mean.
ronald25@fields.com	2015-09-22	63804392	78727916	f	Close girl ask education. Watch minute themselves area clear.
ronald25@fields.com	2010-05-06	8728202	90673397	t	Inside peace expect last building foreign. Close will strategy one manager visit. Have when quickly term senior sound draw end.
marc24@lewis.org	2015-01-02	11454507	115846263	f	Even spend mean throw industry process doctor. Television attorney religious effect direction town kitchen better. Offer each remember go drive.
cindywong@clark-torres.biz	2013-05-26	57352874	161516818	f	Back which defense its institution. Mr remember two edge rock happy sometimes.
sharonhensley@thomas.com	2010-04-14	144922519	113230247	t	Require person still energy employee economy. Conference fear reality front partner enter think important. Exist price may consider far note tell. Democrat future from mean discussion fill bag worry.
griffithwilliam@freeman.com	2012-12-13	180265472	24722595	f	Check herself media cold deal. Page sort leave.
ronald25@fields.com	2013-12-10	177768378	35848218	f	Meeting may might soon all design. Report physical class even.
kevincannon@sims.biz	2013-07-19	152578270	137908617	t	Candidate word produce improve only reach voice. Mission myself total all hair. Notice job power for agree skin school.
jenningselizabeth@sweeney.com	2013-11-26	186838984	48746696	t	Trade authority process strong draw themselves. Case especially guess outside start. Detail table sing ability style. President American include stuff leave.
uhoward@thomas.net	2011-05-02	99980809	49850526	f	Customer often school lead thus. Area again church where own yet possible father.
douglashatfield@decker-woodard.com	2012-07-18	158590840	125145638	f	Four decide because season with maintain. Our east add actually entire recent between. South government hair little cultural.
lgarza@moreno-villarreal.com	2017-10-10	55683911	146961591	t	Truth add consider physical picture week ask. Art answer into with report camera store. Responsibility why ago very measure chance. Product record smile chance open physical.
xenglish@mcdaniel.com	2017-07-20	57590501	82912418	t	Along focus left others. Reflect wait air this daughter challenge have.
sarah68@fletcher.com	2015-06-06	184461307	184400436	t	Enter her truth style century finish. Draw Democrat guess quality. Page maybe however difficult policy.
jacquelinehood@bowman-lewis.biz	2017-12-09	67929267	50699936	f	Get case hundred grow station past employee. Rich maybe picture thus high appear detail near. Maintain already western doctor. Early stage need measure.
thomaspeters@rodriguez.com	2010-01-12	191398207	60898273	f	Production why cover civil chair budget. One or night kitchen effect yourself commercial. Later close see they yard himself serious. Sea market source woman far term task.
sharonhensley@thomas.com	2011-07-05	195764135	153491235	f	Might guy prepare close call call movement. Area consider prepare.
ryanmarshall@greene.com	2016-01-25	6061211	68066899	f	Win century performance cold same shoulder recent through. Unit because plant. My enter measure security determine ability.
shelley15@moran.com	2011-09-03	63188926	25727590	t	Be fire job. Point law firm news resource despite. Respond likely manage score reduce toward.
ronald25@fields.com	2010-09-15	154857415	83423538	t	Everybody event sign evidence collection. Sell service sense choice author fast training.
marc24@lewis.org	2016-05-02	41270353	99600389	t	Grow special everything particular which in financial. Paper lay military style cause never. Cover during finish tend floor. Phone vote alone century common.
jenningselizabeth@sweeney.com	2010-12-10	171586117	175204505	t	Keep talk rule dream whole beautiful.
ryanmarshall@greene.com	2011-10-17	152063966	28906146	t	A list upon ago. Across after catch pass be. To leave respond raise. Ahead feel century term else tend seat contain.
richardallison@morrow-jackson.net	2010-03-08	119018806	65072977	t	Nor garden minute organization according think society. Everyone relationship best range.
kendrapatterson@salinas.com	2018-03-22	14045801	150480883	f	Dog strategy wish. Dark author town appear. Lawyer policy travel network. Share he article why.
cindywong@clark-torres.biz	2018-02-08	164015369	38297361	f	Shake official her thus require past call. White collection crime effect kitchen federal road.
xenglish@mcdaniel.com	2016-10-15	17644430	164949350	f	Area friend break real truth available within. People prepare five nothing.
ryanmarshall@greene.com	2014-02-15	62427281	54235850	t	Goal talk water street. Animal discuss key forward water than southern.
ryanmarshall@greene.com	2011-07-23	32582979	157197588	t	Commercial image shake pay establish top low. Sign food left newspaper media husband us glass. Your cause effort issue soon.
lpotts@mayo.org	2010-01-20	21661585	2938646	t	Market identify successful cut yourself animal. System whether month science house.
marc24@lewis.org	2011-03-09	164806625	170467334	f	Bed thought strong democratic. Back significant through they. Focus road star short.
richardallison@morrow-jackson.net	2013-12-08	51008804	92687672	f	Similar eight should call administration least. Region beyond finally easy race work news.
marktaylor@haynes.com	2012-11-05	139618426	170529197	t	Every choice room their every nearly.
trananthony@brown.com	2011-04-07	99217216	170499466	f	Your allow strong enough. Low really thus character us perhaps. Job help build mention so computer.
sarah68@fletcher.com	2016-05-23	31852708	37254138	t	Every sign why past by shake. Agency must drug must.
sarah68@fletcher.com	2011-08-19	175845225	167766800	t	Fire approach quality. Government practice subject outside able. Might for him game keep everybody speech action.
ruizheather@freeman.com	2013-10-06	139331568	45526652	t	Human help new fire. Event tend quite economic.
richardallison@morrow-jackson.net	2014-11-25	24389481	175412884	f	Left once local here either. Color pass score do. Answer second sport never person law quite.
marc24@lewis.org	2011-06-17	42293673	30177900	t	Deep green public.
sarah68@fletcher.com	2012-02-01	166715710	56427168	t	Imagine Mrs important if project. Physical seek candidate girl. Way employee close rise.
trananthony@brown.com	2013-09-13	18009210	153376928	t	Fish rate those institution should window. Woman morning open prepare professor stock campaign range. Continue when car management bank show often section.
jennifermedina@edwards.com	2013-11-17	53209423	6829696	t	Class improve product sea discuss hard heart account. Trip federal medical west party drive market. While up interesting some financial capital mention.
griffithwilliam@freeman.com	2014-07-27	183112982	43826682	t	Medical model wall guy which. My board event. Certainly very approach such may detail.
sarah68@fletcher.com	2017-03-19	104750650	9940032	f	Walk most main push local. Others skin family watch. Everything today near contain like customer throughout.
petersonmichael@miller-gray.com	2016-11-23	111558223	188710943	t	Drug edge miss. Born always night feel. Majority firm politics.
marc24@lewis.org	2010-05-19	16321556	62611408	f	Identify month hold rise subject generation. Begin value remember though mention notice item ever.
griffithwilliam@freeman.com	2012-09-01	71667297	192130850	t	Ability main class plant notice yes great. Box base scene long.
lgarza@moreno-villarreal.com	2017-01-05	76837323	154622100	f	Bad policy bit meeting. List particularly important day thank improve six. Else maintain coach common realize something.
nguyenryan@peck.com	2011-12-26	120950417	40699277	t	Her issue notice business begin. Name they financial opportunity.
shelley15@moran.com	2016-06-04	124408599	153513520	f	Only develop sister eight team. Child high size financial exist sense. Issue across bad approach production pay.
kendrapatterson@salinas.com	2011-04-20	15563733	24767416	t	More situation blood. American sea join. Visit other fill. Institution level still meeting country window.
cindywong@clark-torres.biz	2014-08-14	7651983	99679310	t	Find mother from. Mention really employee receive movement bit.
marc24@lewis.org	2015-07-21	125706628	13199730	t	Role effect put herself create. White poor agent prevent foreign them. Situation anyone five deal service contain without. Property have life effort book hair most report.
flyons@wall.com	2017-10-20	110411947	53199515	t	Thing pull eat notice. Determine magazine every though. Make hot dog hour.
douglashatfield@decker-woodard.com	2013-10-28	40367905	18788517	f	Democrat receive produce attention. Article blue hospital yard dinner minute.
lgarza@moreno-villarreal.com	2016-09-05	10481944	172084977	f	Success high visit for I. Loss democratic part word local. Specific fast by break.
richardallison@morrow-jackson.net	2016-10-13	17603404	184262898	f	Her board situation can audience west professor. Situation listen study letter.
thomaspeters@rodriguez.com	2015-08-13	182785211	95697890	f	Car employee southern upon. Strong someone decide way write look prove. Determine service open truth other improve list.
lgarza@moreno-villarreal.com	2017-08-10	71659563	17107222	t	Happen place perhaps rest. Music they rest all particularly truth. Each his safe.
nguyenryan@peck.com	2011-07-02	83571550	42765564	f	Go space employee particular significant leader lawyer ball. Program opportunity southern natural pressure movie friend society.
lgarza@moreno-villarreal.com	2015-07-04	198114275	150944080	f	Born sell office remember boy law friend fire. Example join travel trouble president cup eat structure. Mother safe main you information left.
cindywong@clark-torres.biz	2014-02-17	34542538	141774285	t	Ok challenge source collection usually explain.
trananthony@brown.com	2011-09-21	56247120	71384022	t	Glass wife then mother memory money guy. Alone style public offer concern. Rather eat three look along benefit.
ruizheather@freeman.com	2017-10-29	162683844	163333376	t	Move same nothing minute kind still speak.
petersonmichael@miller-gray.com	2010-03-13	2821637	84450758	f	Later official model situation manager with. Radio paper white cost. Style human rather parent money.
uhoward@thomas.net	2012-08-27	161585010	122424358	t	Include ask single answer billion nice. Establish cup within soldier else race respond. Experience visit high son.
xenglish@mcdaniel.com	2011-06-21	7665740	105179286	t	Prepare view growth account court. Look modern forward worry fight herself go.
petersonmichael@miller-gray.com	2018-01-20	132335041	97588700	t	Team as peace discuss scientist. Eat agreement note despite either stand.
sarah68@fletcher.com	2018-01-19	149194652	128298424	f	Democrat while either. Fast would small.
xenglish@mcdaniel.com	2016-03-29	58184568	112193526	t	Large there thus country century dinner song. Think man enter. Wife brother commercial at condition open.
nguyenryan@peck.com	2014-11-07	4464741	172595675	f	Director democratic hope project remain even low. Place finish represent situation me. Song mind drop drop poor company during.
lpotts@mayo.org	2017-07-29	128905540	133379098	t	Other summer pick create way receive. Child whether window grow it field then. Car ten per her drug.
lpotts@mayo.org	2010-04-15	50380092	67926602	f	Wait peace public meeting. Attorney decide in reason play unit.
shelley15@moran.com	2013-03-23	194792379	161302790	f	Social color population base. Herself put what establish possible magazine. That reality suddenly understand office tell indeed summer. Guess white cover PM.
richardallison@morrow-jackson.net	2018-03-02	42255484	84674746	f	Heavy experience interest suddenly effort fall per. Forget put his million more.
kevincannon@sims.biz	2011-07-15	51266216	155082564	f	Unit mean something off. Loss to among oil seek.
jacquelinehood@bowman-lewis.biz	2012-10-18	179485535	105648450	f	International within many occur soon she.
douglashatfield@decker-woodard.com	2017-11-18	64362735	154593276	t	Source skill seem. Perhaps assume my heavy significant affect. When garden four place management training. Entire court how beat move.
\.


--
-- Data for Name: organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.organisasi (email_organisasi, website, nama, provinsi, kabupaten_kota, kecamatan, kelurahan, kode_pos, status_verifikasi) FROM stdin;
cindywong@clark-torres.biz	http://clark-torres.biz/	Smith, Anderson and Thompson and Sons	Georgia	West Pamela	Smith Manors	Dunlap Corner	50081-3741	Not verified
ryanmarshall@greene.com	https://greene.com/	Jones-Bright Group	Kentucky	South Robert	Jennifer Falls	Sarah Turnpike	65538	Verified
lowetimothy@collier.biz	https://collier.biz/	Jackson-Berg Inc	Oklahoma	Teresaland	Stevens Cove	West Highway	11361	Verified
sharonhensley@thomas.com	https://thomas.com/	Kelley Inc Inc	Connecticut	Port Samuelton	Joann Dam	Stacy Roads	24777-4741	Not verified
griffithwilliam@freeman.com	https://freeman.com/	Woods, Kelly and Hernandez LLC	Connecticut	New Kimberly	Anne Road	Tracy Crossroad	56193	Verified
lgarza@moreno-villarreal.com	http://moreno-villarreal.com/	Carter Inc Inc	New Hampshire	Port Deborah	Garcia Burg	Daniel Shores	48885-6294	Verified
trananthony@brown.com	https://brown.com/	Palmer Inc Group	West Virginia	Hallfurt	Long Plains	Nelson Greens	89493	Not verified
marktaylor@haynes.com	https://haynes.com/	Taylor-Joseph PLC	West Virginia	Port Amandahaven	Marshall Springs	Maria Estate	81477-5108	Verified
kendrapatterson@salinas.com	http://salinas.com/	Mcguire-Calderon Group	Wisconsin	Lake Kennethmouth	Gardner Lane	Taylor Oval	95578-8145	Not verified
petersonmichael@miller-gray.com	http://miller-gray.com/	Morton and Sons PLC	New Mexico	Maryton	Ronnie Divide	Ann Neck	78495-1839	Not verified
nguyenryan@peck.com	http://peck.com/	Rivera, Simmons and Gutierrez Ltd	Maine	New Marc	Cassidy Land	Williamson Park	51387	Verified
ronald25@fields.com	https://fields.com/	Gutierrez and Sons PLC	California	Strongview	Mendez Road	Perez View	43983-1588	Not verified
douglashatfield@decker-woodard.com	http://decker-woodard.com/	Hernandez, Holt and Alvarez Group	Arkansas	Lake James	Miller Cliff	Anthony Landing	23726-7396	Verified
wterry@jones.org	http://jones.org/	Dean PLC and Sons	New Jersey	Ramirezbury	Ricky Mill	Montgomery Mountains	96299-6113	Verified
kevincannon@sims.biz	https://sims.biz/	Sanders Inc Inc	Massachusetts	South Christopher	Gail Skyway	Valdez Overpass	82839	Verified
uhoward@thomas.net	http://thomas.net/	Khan-Rubio LLC	Missouri	Bradleyfurt	Diane Tunnel	Jones Groves	59941-5150	Verified
thomaspeters@rodriguez.com	https://rodriguez.com/	Bernard, Webster and Scott Inc	Iowa	South Shawnside	Jeffrey Hollow	Scott Union	06380-8939	Not verified
michaelrogers@johnston-zavala.com	https://johnston-zavala.com/	Collins Inc Group	Minnesota	Tylertown	Ruiz Coves	Wheeler Lodge	43921-0678	Verified
xenglish@mcdaniel.com	http://mcdaniel.com/	Burns LLC Inc	Illinois	Josephport	Bailey Unions	Williams Roads	16683-8805	Not verified
shelley15@moran.com	https://moran.com/	Hodge Ltd LLC	Colorado	New Bradleyborough	Roy Lock	Lorraine Land	88533-6080	Not verified
jennifermedina@edwards.com	https://edwards.com/	Lee and Sons Group	North Dakota	Amyside	Cummings Falls	Philip Springs	69426	Verified
kevinsalinas@potts.net	http://potts.net/	Walters Ltd LLC	Maryland	Michaelton	Karen Light	Jackson Pass	16172-1610	Verified
lpotts@mayo.org	http://mayo.org/	Fisher, Farrell and Scott Ltd	North Carolina	Port Amanda	Joshua Fork	Jill Cliff	81330	Not verified
flyons@wall.com	https://wall.com/	Hernandez, Gordon and Washington LLC	California	Port Nicoleland	Susan Greens	Katelyn Shoal	40585	Verified
richardallison@morrow-jackson.net	https://morrow-jackson.net/	Wise-Lopez PLC	Idaho	Hernandezstad	Jose Port	Brianna Corner	62823-3925	Verified
sarah68@fletcher.com	https://fletcher.com/	Miller-Weiss Ltd	Missouri	Walterston	Howard Courts	Sanchez Valley	54756	Verified
marc24@lewis.org	https://lewis.org/	Jones PLC Group	Mississippi	North Erin	Catherine Ville	Amy Cape	84853	Verified
ruizheather@freeman.com	https://freeman.com/	Sexton PLC Inc	California	North Frankville	Donna Bypass	Susan Points	25051	Not verified
jacquelinehood@bowman-lewis.biz	https://bowman-lewis.biz/	Hill, Johnson and Adams and Sons	Washington	West Tina	Heidi Pines	Smith Divide	45347-3583	Verified
jenningselizabeth@sweeney.com	https://sweeney.com/	Watson Ltd Group	West Virginia	Jerryton	Joseph Lodge	Gonzalez Greens	88809	Verified
organisasi3@organisasi.com	https://www.organisasi1.com	Nama Organisasi 1	-	-	-	-	-	True
organisasi4@organisasi.com	http://www.google.com	Organisasi 3	jawa barat	depok	kukusan	beji	16425	True
organisasi6@organisasi.com	http://www.google.com	Organisasi 3	jawa barat	depok	kukusan	beji	16425	True
\.


--
-- Data for Name: organisasi_terverifikasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.organisasi_terverifikasi (email_organisasi, nomor_registrasi, status_aktif) FROM stdin;
flyons@wall.com	FzqnAqfDeeEGyNWgUoNYEZTMtvpoquwxoMUomKukXmjJAvLehM	Inactive
richardallison@morrow-jackson.net	wBktuxTVdDsVHUfkOtjaCUDAdDjuTINGQZsyBCQlJioCjeMaUe	Inactive
michaelrogers@johnston-zavala.com	BzMYMsakVukEKfrTjidHjGdDnrhTIoywlFjeOobGmvnMFNeRir	Active
lpotts@mayo.org	IHMceIROlrddvqPJKWNqLSvnOHgAEIsPNGpDwpSTFJZiHGwrqO	Inactive
petersonmichael@miller-gray.com	yEbPUlAoWphomLUQkQnKOUHteaBwBLKvQVZNVpmvAQhjpmqVgC	Active
trananthony@brown.com	COEqGUYgyQzLxcwrlwIRBZWLJEDLhFCmNExCFnVcRhOuxXcqou	Active
thomaspeters@rodriguez.com	nWjFUxFwAWjrCWBjqoWZMVIljeoNihwWnWScQwnJxsFrhgOUHp	Active
sharonhensley@thomas.com	xWzhbppzMjftEBGHxirVFMdsdLhscKAiqCdrwTKVEnncRywFGo	Active
griffithwilliam@freeman.com	uTjefgBXjcVQRNytMktAXdLZtmuJlqNixJgwBIbXTbFbjjEoTw	Active
cindywong@clark-torres.biz	BZGqMtFPmRGUkJzeDVupOpQEjmGTjXuYRLoGPbbzqTcEJzDUzV	Inactive
lgarza@moreno-villarreal.com	niHfizDriHqdNYbZSbkrlnckIyLIgXibgPkdYVxWLJANpsGNpx	Active
uhoward@thomas.net	VHjuLdFKYMoOvklxjQlKYZtpDOnECyPHEfYCCKitoRQoGspbeu	Active
sarah68@fletcher.com	EyFRHTBNALTdxRfJjkIjjQgYovwgnoWXbKNLXwWIOCaKpahWSJ	Inactive
kendrapatterson@salinas.com	iLyIiBPhwSgvTRlSEzYGCJsfCGiKOAqsnSRoVWuHofBxjdZEhr	Active
marktaylor@haynes.com	wwYPcBalmbAtUXyqTtyFprLgQMIhivdNJrGkJpCIJVqiLyyRcP	Inactive
ronald25@fields.com	XSLCXQNhrXUHwhFfuilLHsoCblAcfQoLRYZNirIzFjnUWWKJdF	Inactive
nguyenryan@peck.com	xyghZAIPOlIYkVLyACLcineHhLlIyvSFNROQinerPQnOPQwinN	Active
douglashatfield@decker-woodard.com	xMtXlScVbHyBtZGPMRqHdxjRfIKjSdlnfiIAooSKBEENEUvUFC	Active
jenningselizabeth@sweeney.com	ZwsnBIFYlJRsBgfVPOmVZVetIjJymCbSphWpCGnqcLvfMZXUKN	Active
jennifermedina@edwards.com	yFBIQFGGzRqRVCgWQUxEIsDjpVPfibwgdfozAQTeNxouhbxpTH	Active
ryanmarshall@greene.com	ZGhLaQEuztuODCUyNLeallMqFBEjVqWrKAvXZPLySKnHiFmbem	Inactive
shelley15@moran.com	AuWmKXXCMyFKUAGxBRAAIfWjPVakidmCnGpiekwFiZJsNXNWLn	Active
kevincannon@sims.biz	IBiuuBAZdHdageOirQGbGvztdCMdSyFeHcRUUXmqxfKjazFvsX	Inactive
kevinsalinas@potts.net	nNpkSBxHwiJpWycyPPMAbcVoFlkMFukikxtSbpqDMkKpvgamZb	Inactive
jacquelinehood@bowman-lewis.biz	txjzpbCtJPQhtQgTVOJdHYaEFUtxUNPOzmcpyDiiFqJgRokoMr	Active
lowetimothy@collier.biz	MNkAonZJytBpduZomChILVqYqmYxvbkitmoztmjTzymqwMGiTS	Inactive
wterry@jones.org	AcLoTrHKVSJYNUXxuYLOzaQCdmjFDvlNXJPyxsZGtTqmqiRdce	Active
xenglish@mcdaniel.com	CjfnAbtJDBGZanMSTcCWPooNWjPDdpcbuFsnYTTaszjpCguWeO	Inactive
ruizheather@freeman.com	LHNTxWRiNdMBxBfDOCBxoIOgBJCbHygyMVGJlivJiGceiaDGMJ	Inactive
marc24@lewis.org	yZMJScoKjZDIDYPiZLFAydHVhquLoyXLmPiRwORmrBvNOkDfvb	Active
\.


--
-- Data for Name: pengurus_organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.pengurus_organisasi (email, organisasi) FROM stdin;
baskam2y@si.edu	kevinsalinas@potts.net
evaissiereu@ibm.com	jacquelinehood@bowman-lewis.biz
cpirdy1p@usatoday.com	michaelrogers@johnston-zavala.com
sgeerits2q@tiny.cc	jenningselizabeth@sweeney.com
smaierw@ifeng.com	ronald25@fields.com
tself4h@weibo.com	wterry@jones.org
mchater4w@jigsy.com	richardallison@morrow-jackson.net
kcurran4l@dagondesign.com	marktaylor@haynes.com
sroostan4n@cdbaby.com	griffithwilliam@freeman.com
cpierse4u@twitpic.com	ronald25@fields.com
gbonson19@marketwatch.com	lgarza@moreno-villarreal.com
kricciardiellot@google.com.au	griffithwilliam@freeman.com
gcalvie5@kickstarter.com	nguyenryan@peck.com
mbanburyp@friendfeed.com	lpotts@mayo.org
acurrington3c@answers.com	uhoward@thomas.net
sdarleyy@admin.ch	trananthony@brown.com
wbagworth7@bloomberg.com	kevinsalinas@potts.net
gjirikl@sciencedirect.com	douglashatfield@decker-woodard.com
hbloomfield1o@tuttocitta.it	xenglish@mcdaniel.com
mgrzesiewicze@histats.com	jacquelinehood@bowman-lewis.biz
awhatman2c@twitpic.com	lgarza@moreno-villarreal.com
lcollopy1v@so-net.ne.jp	ryanmarshall@greene.com
jmcroberts8@opera.com	cindywong@clark-torres.biz
mgehring1q@spiegel.de	lowetimothy@collier.biz
akezar1g@cdbaby.com	lowetimothy@collier.biz
erhymes4e@vimeo.com	petersonmichael@miller-gray.com
vcaroli6@mapy.cz	ronald25@fields.com
akistingh@imgur.com	jenningselizabeth@sweeney.com
jspat3z@rambler.ru	lpotts@mayo.org
vmeriot14@wunderground.com	kevinsalinas@potts.net
pjagson49@deviantart.com	cindywong@clark-torres.biz
eclubb2r@hp.com	shelley15@moran.com
sidle2d@sfgate.com	flyons@wall.com
gdouse44@ca.gov	shelley15@moran.com
cbrazil40@pcworld.com	douglashatfield@decker-woodard.com
rbarents26@naver.com	petersonmichael@miller-gray.com
mcardenosa37@histats.com	nguyenryan@peck.com
cshevlin2e@comcast.net	sarah68@fletcher.com
ksidnall2v@1und1.de	flyons@wall.com
acarwithim3k@wikipedia.org	kendrapatterson@salinas.com
rglide2k@ox.ac.uk	kevincannon@sims.biz
ade31@t.co	petersonmichael@miller-gray.com
lbadwick3o@mtv.com	michaelrogers@johnston-zavala.com
jmoakson3x@seesaa.net	sarah68@fletcher.com
dsteffans4j@fastcompany.com	shelley15@moran.com
iglisenan3j@ifeng.com	marc24@lewis.org
scrosse34@usatoday.com	trananthony@brown.com
wferrai51@wiley.com	griffithwilliam@freeman.com
bdureden30@netscape.com	uhoward@thomas.net
rskeldinge2t@sun.com	marktaylor@haynes.com
ntwelvetree32@seattletimes.com	uhoward@thomas.net
mericssen52@hhs.gov	thomaspeters@rodriguez.com
gstebbings23@ow.ly	kevincannon@sims.biz
pcolerick4d@hud.gov	kendrapatterson@salinas.com
ttuck53@acquirethisname.com	lowetimothy@collier.biz
dsoltan5a@ameblo.jp	kevinsalinas@potts.net
dpancoust4m@php.net	kendrapatterson@salinas.com
ashadwick1h@spiegel.de	trananthony@brown.com
amconie25@com.com	lpotts@mayo.org
khardwick5d@eventbrite.com	jenningselizabeth@sweeney.com
agrigoroni58@g.co	lowetimothy@collier.biz
cmillsomi@so-net.ne.jp	michaelrogers@johnston-zavala.com
slithcow5j@skyrock.com	ruizheather@freeman.com
ntanman42@unblog.fr	jacquelinehood@bowman-lewis.biz
amarques4z@liveinternet.ru	marktaylor@haynes.com
apaulot5e@fastcompany.com	wterry@jones.org
jgounel18@tuttocitta.it	wterry@jones.org
ierricker4r@cnbc.com	lpotts@mayo.org
kstainton2h@state.gov	kevinsalinas@potts.net
cvasenkov5g@apache.org	ruizheather@freeman.com
afoneg@biglobe.ne.jp	kendrapatterson@salinas.com
ddelion3h@prweb.com	marc24@lewis.org
zlosebief@disqus.com	ryanmarshall@greene.com
gjanuszewski4f@purevolume.com	cindywong@clark-torres.biz
vokennedy1b@i2i.jp	jenningselizabeth@sweeney.com
chanfrey4g@prlog.org	michaelrogers@johnston-zavala.com
aaharoniz@geocities.com	marktaylor@haynes.com
mhartshorn4s@salon.com	flyons@wall.com
wwillcock38@mozilla.com	kendrapatterson@salinas.com
jcalabryr@cmu.edu	sharonhensley@thomas.com
afortoun2a@imdb.com	douglashatfield@decker-woodard.com
kgress2x@last.fm	richardallison@morrow-jackson.net
rinnocenti1r@comcast.net	richardallison@morrow-jackson.net
bcundyx@reverbnation.com	douglashatfield@decker-woodard.com
efilby5i@examiner.com	xenglish@mcdaniel.com
abehning5b@g.co	petersonmichael@miller-gray.com
kdruce4@smugmug.com	trananthony@brown.com
mdrackford3a@sbwire.com	kevincannon@sims.biz
tlummis2n@springer.com	nguyenryan@peck.com
pcases1t@4shared.com	douglashatfield@decker-woodard.com
cfraczak11@jugem.jp	sharonhensley@thomas.com
jbanyard48@desdev.cn	thomaspeters@rodriguez.com
asharma4q@free.fr	thomaspeters@rodriguez.com
bellardm@harvard.edu	ruizheather@freeman.com
bpicot1d@businessinsider.com	kevinsalinas@potts.net
gfisher13@technorati.com	kevinsalinas@potts.net
oduffan16@yolasite.com	jennifermedina@edwards.com
gkinforth54@taobao.com	jacquelinehood@bowman-lewis.biz
khedditch59@wisc.edu	jenningselizabeth@sweeney.com
sspellman4c@cdbaby.com	griffithwilliam@freeman.com
bbiggerdike2i@unesco.org	kevinsalinas@potts.net
aharmston41@bravesites.com	kevincannon@sims.biz
rjacklin36@samsung.com	lowetimothy@collier.biz
gcrellin4p@hugedomains.com	kevinsalinas@potts.net
striggk@i2i.jp	ruizheather@freeman.com
ttsarovic4b@nba.com	lpotts@mayo.org
bgreber1w@moonfruit.com	wterry@jones.org
jbutt2s@addthis.com	richardallison@morrow-jackson.net
blaydonq@ocn.ne.jp	jacquelinehood@bowman-lewis.biz
mcavanaugh2z@dmoz.org	lowetimothy@collier.biz
coba@coba.com	organisasi3@organisasi.com
daninsoedjono@danin.tech	organisasi4@organisasi.com
pengurus1@organisasi6.com	organisasi6@organisasi.com
pengurus2@organisasi6.com	organisasi6@organisasi.com
\.


--
-- Data for Name: penilaian_performa; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.penilaian_performa (email_relawan, organisasi, id_number, tgl_penilaian, deskripsi, nilai_skala) FROM stdin;
awhatman2c@twitpic.com	trananthony@brown.com	1	2016-06-13	Pressure key could range expert.	4
awhatman2c@twitpic.com	trananthony@brown.com	2	2017-01-23	Collection home rich according last.	4
awhatman2c@twitpic.com	trananthony@brown.com	3	2017-09-06	Matter school next service imagine anything help.	0
awhatman2c@twitpic.com	trananthony@brown.com	4	2017-12-18	Middle close other ball send hot.	3
tlummis2n@springer.com	kendrapatterson@salinas.com	1	2017-11-01	Example indeed rate society toward home.	2
cshevlin2e@comcast.net	jacquelinehood@bowman-lewis.biz	1	2017-09-09	Fact soldier there star major statement.	1
cshevlin2e@comcast.net	jacquelinehood@bowman-lewis.biz	2	2017-10-27	Message chance Mr myself outside.	4
cshevlin2e@comcast.net	jacquelinehood@bowman-lewis.biz	3	2017-12-16	Moment interest perform.	0
cshevlin2e@comcast.net	jacquelinehood@bowman-lewis.biz	4	2018-03-22	Benefit wind election bag spring huge lose.	2
cshevlin2e@comcast.net	jacquelinehood@bowman-lewis.biz	5	2018-04-25	Clear country down serious right consumer power.	3
cbream2m@shareasale.com	cindywong@clark-torres.biz	1	2013-11-16	Small card type country sing.	3
cbream2m@shareasale.com	cindywong@clark-torres.biz	2	2013-11-17	Town kitchen thank.	1
cbream2m@shareasale.com	cindywong@clark-torres.biz	3	2014-05-27	Half these choice knowledge.	5
cbream2m@shareasale.com	cindywong@clark-torres.biz	4	2016-02-19	Model again one job.	4
cbream2m@shareasale.com	cindywong@clark-torres.biz	5	2016-08-20	Member spend line.	0
ivieyra27@cbslocal.com	uhoward@thomas.net	1	2017-08-22	Improve about agree fund.	4
ivieyra27@cbslocal.com	uhoward@thomas.net	2	2017-11-19	Consider people suggest item dinner.	1
ivieyra27@cbslocal.com	uhoward@thomas.net	3	2018-01-06	Operation herself Democrat watch receive.	3
tpinyon21@amazonaws.com	lgarza@moreno-villarreal.com	1	2016-12-03	Appear even probably send could thought professor.	2
tpinyon21@amazonaws.com	lgarza@moreno-villarreal.com	2	2017-09-22	Able involve serious yet move traditional.	4
tpinyon21@amazonaws.com	lgarza@moreno-villarreal.com	3	2017-12-06	Write maintain west statement explain.	3
tpinyon21@amazonaws.com	lgarza@moreno-villarreal.com	4	2018-02-21	These writer dark short believe pressure light.	3
tpinyon21@amazonaws.com	lgarza@moreno-villarreal.com	5	2018-04-09	Area into sometimes test cut piece.	2
bbiggerdike2i@unesco.org	thomaspeters@rodriguez.com	1	2016-05-29	Meeting protect college instead analysis occur he join.	3
bbiggerdike2i@unesco.org	thomaspeters@rodriguez.com	2	2016-07-06	Well weight condition more PM.	4
bbiggerdike2i@unesco.org	thomaspeters@rodriguez.com	3	2017-07-14	Consumer film city culture.	2
bbiggerdike2i@unesco.org	thomaspeters@rodriguez.com	4	2018-04-05	Physical reveal run so weight.	3
flyford1y@homestead.com	marktaylor@haynes.com	1	2017-11-06	Authority particularly defense despite.	4
flyford1y@homestead.com	marktaylor@haynes.com	2	2018-02-16	Prove than lose try goal condition positive generation.	3
flyford1y@homestead.com	marktaylor@haynes.com	3	2018-03-12	National amount whole song letter better might eight.	2
flyford1y@homestead.com	marktaylor@haynes.com	4	2018-04-12	Threat it shoulder guy as future.	0
sidle2d@sfgate.com	michaelrogers@johnston-zavala.com	1	2015-03-05	Week price sense for responsibility.	4
sidle2d@sfgate.com	michaelrogers@johnston-zavala.com	2	2015-08-09	Both leg never safe modern.	1
sidle2d@sfgate.com	michaelrogers@johnston-zavala.com	3	2017-01-28	Sound blood at.	1
hbloomfield1o@tuttocitta.it	lpotts@mayo.org	1	2016-07-20	Leader however offer head girl.	4
hbloomfield1o@tuttocitta.it	lpotts@mayo.org	2	2016-09-08	Majority itself step.	3
hbloomfield1o@tuttocitta.it	lpotts@mayo.org	3	2017-03-26	Continue special treat better religious involve.	2
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com	1	2016-10-17	Cell win only tough reflect include.	2
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com	2	2016-11-15	Move program Republican.	3
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com	3	2017-11-04	Subject child follow race leave but.	1
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com	4	2018-01-19	Million knowledge moment direction pass rather card.	2
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com	5	2018-04-17	Two message gas imagine car food certain though.	2
gbortolussi1z@yahoo.com	kendrapatterson@salinas.com	1	2016-05-17	Green go no tax box.	2
gbortolussi1z@yahoo.com	kendrapatterson@salinas.com	2	2016-06-19	Reduce style significant old control popular movie way.	0
gbortolussi1z@yahoo.com	kendrapatterson@salinas.com	3	2016-10-10	Ready already it scientist under pretty without only.	5
hthowless2f@opensource.org	xenglish@mcdaniel.com	1	2017-03-27	You though ok offer start culture.	0
njameson2l@yandex.ru	xenglish@mcdaniel.com	1	2012-06-27	Take catch worry dream message.	0
tlummis2n@springer.com	ruizheather@freeman.com	1	2017-10-13	Wait late feel health college necessary.	1
tlummis2n@springer.com	ruizheather@freeman.com	2	2018-04-12	Know box information college daughter.	0
tlummis2n@springer.com	ruizheather@freeman.com	3	2018-04-12	Follow available home figure.	4
tlummis2n@springer.com	ruizheather@freeman.com	4	2018-04-25	Oil expect wind sound here.	0
hbloomfield1o@tuttocitta.it	jennifermedina@edwards.com	1	2016-11-30	Claim kitchen never product always.	4
hbloomfield1o@tuttocitta.it	jennifermedina@edwards.com	2	2018-04-09	Research alone expect deep remember officer.	2
hbloomfield1o@tuttocitta.it	jennifermedina@edwards.com	3	2018-04-16	Audience ago prove attorney.	0
hbloomfield1o@tuttocitta.it	jennifermedina@edwards.com	4	2018-04-23	Either information to dark difficult cut.	4
bbiggerdike2i@unesco.org	jacquelinehood@bowman-lewis.biz	1	2017-06-03	Instead company change public actually star plant technology.	3
rglide2k@ox.ac.uk	petersonmichael@miller-gray.com	1	2015-12-24	And use size soldier office.	0
rglide2k@ox.ac.uk	petersonmichael@miller-gray.com	2	2017-08-30	Indicate second both.	5
rbarents26@naver.com	cindywong@clark-torres.biz	1	2010-05-04	Billion appear open want law point provide.	3
vvillaret2g@utexas.edu	shelley15@moran.com	1	2018-01-28	Ten lay character.	4
cshevlin2e@comcast.net	sharonhensley@thomas.com	1	2016-01-01	Item next account site may home organization reality.	2
cshevlin2e@comcast.net	sharonhensley@thomas.com	2	2017-12-26	Story indeed eat onto.	5
cshevlin2e@comcast.net	sharonhensley@thomas.com	3	2018-03-29	Charge first with four us.	2
cshevlin2e@comcast.net	sharonhensley@thomas.com	4	2018-04-04	Price network manager season phone growth product.	4
jgeorgeon29@sbwire.com	jenningselizabeth@sweeney.com	1	2017-06-11	Risk present available eight reason price crime.	1
jgeorgeon29@sbwire.com	jenningselizabeth@sweeney.com	2	2017-10-20	Sit subject later or read go kitchen force.	2
pcases1t@4shared.com	lpotts@mayo.org	1	2018-04-23	Since suddenly budget second hit star artist.	2
pcases1t@4shared.com	lpotts@mayo.org	2	2018-04-23	Once major these.	3
pcases1t@4shared.com	lpotts@mayo.org	3	2018-04-25	Away candidate significant.	5
pcases1t@4shared.com	lpotts@mayo.org	4	2018-04-25	Third want student church course cold.	0
aligertwood1k@who.int	ronald25@fields.com	1	2017-05-13	Commercial part evening center soldier could rich.	0
aligertwood1k@who.int	ronald25@fields.com	2	2018-01-28	Environmental best too direction system Republican position.	5
aligertwood1k@who.int	ronald25@fields.com	3	2018-03-12	Officer she mother camera stuff Congress.	2
aligertwood1k@who.int	ronald25@fields.com	4	2018-03-16	Adult up actually.	5
aligertwood1k@who.int	ronald25@fields.com	5	2018-04-22	Economic want north degree manage able.	3
jgot1u@hao123.com	kevinsalinas@potts.net	1	2018-01-23	Recognize put upon new.	2
ashadwick1h@spiegel.de	thomaspeters@rodriguez.com	1	2016-11-02	Government sense five line apply.	2
ashadwick1h@spiegel.de	thomaspeters@rodriguez.com	2	2017-11-02	General police yet TV.	1
ashadwick1h@spiegel.de	thomaspeters@rodriguez.com	3	2017-11-24	Article cup term offer.	2
ashadwick1h@spiegel.de	thomaspeters@rodriguez.com	4	2018-01-14	Them tough dream great fear fine.	4
hbloomfield1o@tuttocitta.it	kendrapatterson@salinas.com	1	2016-05-13	Police control candidate state table which box.	1
hbloomfield1o@tuttocitta.it	kendrapatterson@salinas.com	2	2016-11-28	Relate recently partner over just traditional inside.	1
hbloomfield1o@tuttocitta.it	kendrapatterson@salinas.com	3	2017-09-28	Tax quite decision sport focus.	3
hbloomfield1o@tuttocitta.it	kendrapatterson@salinas.com	4	2018-03-13	Support live speech or themselves down.	0
tpinyon21@amazonaws.com	uhoward@thomas.net	1	2018-03-03	Particularly south say century way already several.	0
tpinyon21@amazonaws.com	uhoward@thomas.net	2	2018-04-18	Rest magazine garden heavy police.	3
rbarents26@naver.com	nguyenryan@peck.com	1	2017-07-02	Popular today such glass agency movement individual.	0
jgeorgeon29@sbwire.com	kendrapatterson@salinas.com	1	2017-12-22	Street case somebody build research not bad few.	2
njameson2l@yandex.ru	marktaylor@haynes.com	1	2017-09-07	Will job speech pretty.	2
njameson2l@yandex.ru	marktaylor@haynes.com	2	2017-12-08	Necessary understand painting.	4
njameson2l@yandex.ru	marktaylor@haynes.com	3	2018-01-13	Seem prepare for Mrs road bag.	0
tmayhead22@hc360.com	trananthony@brown.com	1	2017-12-09	Itself tell blood hair she message.	0
tmayhead22@hc360.com	trananthony@brown.com	2	2018-02-09	Operation young its look lay person himself.	2
tmayhead22@hc360.com	trananthony@brown.com	3	2018-02-11	Happen probably threat pay or.	4
kmcwhirter1s@ibm.com	trananthony@brown.com	1	2013-07-10	Trip stage heavy most month draw participant.	3
kmcwhirter1s@ibm.com	trananthony@brown.com	2	2014-12-15	Level still I speech.	2
kmcwhirter1s@ibm.com	trananthony@brown.com	3	2015-01-16	Culture hear probably right name.	1
vvillaret2g@utexas.edu	richardallison@morrow-jackson.net	1	2016-07-21	Human miss let because fight charge company.	4
vvillaret2g@utexas.edu	richardallison@morrow-jackson.net	2	2017-03-09	Cultural personal firm necessary talk.	1
cshevlin2e@comcast.net	petersonmichael@miller-gray.com	1	2018-02-09	Affect key participant factor positive forget.	2
pgebb1l@yahoo.com	ronald25@fields.com	1	2011-05-26	Condition direction still central.	0
rglide2k@ox.ac.uk	nguyenryan@peck.com	1	2017-04-02	Natural cause situation might sing get be note.	2
rglide2k@ox.ac.uk	nguyenryan@peck.com	2	2018-01-24	Also whom only wall risk step.	0
rglide2k@ox.ac.uk	nguyenryan@peck.com	3	2018-01-31	Mouth college itself.	2
cpirdy1p@usatoday.com	uhoward@thomas.net	1	2016-02-04	Whatever through various current military decide.	0
cpirdy1p@usatoday.com	uhoward@thomas.net	2	2016-09-01	Peace magazine reality yeah form any international.	0
mgehring1q@spiegel.de	ronald25@fields.com	1	2014-09-24	Need individual popular result peace about kid war.	0
bfishbourn2o@berkeley.edu	marktaylor@haynes.com	1	2016-03-07	Think table argue material.	0
kmcwhirter1s@ibm.com	nguyenryan@peck.com	1	2017-02-28	Five including party mention nice cold.	3
eclubb2r@hp.com	jenningselizabeth@sweeney.com	1	2017-10-31	Maybe seat direction learn despite.	3
eclubb2r@hp.com	jenningselizabeth@sweeney.com	2	2018-03-05	Few soon budget.	3
eclubb2r@hp.com	jenningselizabeth@sweeney.com	3	2018-03-10	Their matter television that wall argue.	4
eclubb2r@hp.com	jenningselizabeth@sweeney.com	4	2018-04-02	Drop compare summer travel beat.	1
cpirdy1p@usatoday.com	marktaylor@haynes.com	1	2012-12-24	Near suddenly party much.	0
cpirdy1p@usatoday.com	marktaylor@haynes.com	2	2017-11-17	Low security that doctor south team all.	3
kmcwhirter1s@ibm.com	douglashatfield@decker-woodard.com	1	2013-07-13	Amount idea market appear science conference toward.	2
kmcwhirter1s@ibm.com	douglashatfield@decker-woodard.com	2	2016-03-30	Right turn against thank hand travel conference.	3
rglide2k@ox.ac.uk	ryanmarshall@greene.com	1	2018-04-03	Argue decade sing hotel.	1
bgreber1w@moonfruit.com	douglashatfield@decker-woodard.com	1	2018-03-21	Art theory action on citizen.	0
bgreber1w@moonfruit.com	douglashatfield@decker-woodard.com	2	2018-04-01	His as pass raise.	2
bgreber1w@moonfruit.com	douglashatfield@decker-woodard.com	3	2018-04-18	Organization do successful identify this.	4
bgreber1w@moonfruit.com	douglashatfield@decker-woodard.com	4	2018-04-20	Investment all our simply green pretty.	2
mgehring1q@spiegel.de	cindywong@clark-torres.biz	1	2016-08-22	Campaign theory station security charge seven whom.	3
mgehring1q@spiegel.de	cindywong@clark-torres.biz	2	2017-12-20	Hear suddenly address attack.	4
rglide2k@ox.ac.uk	kendrapatterson@salinas.com	1	2014-09-27	Technology speak specific chance.	5
rglide2k@ox.ac.uk	kendrapatterson@salinas.com	2	2017-12-31	She really per trouble ask.	2
rglide2k@ox.ac.uk	kendrapatterson@salinas.com	3	2018-02-12	Always girl act avoid loss respond itself treatment.	4
hbloomfield1o@tuttocitta.it	thomaspeters@rodriguez.com	1	2015-08-25	Mission face senior.	2
hbloomfield1o@tuttocitta.it	thomaspeters@rodriguez.com	2	2016-01-04	State adult message red mention.	5
hbloomfield1o@tuttocitta.it	thomaspeters@rodriguez.com	3	2016-10-10	It suffer inside he.	1
tpinyon21@amazonaws.com	marktaylor@haynes.com	1	2018-04-20	Everyone meet now western rise reduce lawyer than.	1
tpinyon21@amazonaws.com	marktaylor@haynes.com	2	2018-04-21	Word important interest every TV across lay.	2
tpinyon21@amazonaws.com	marktaylor@haynes.com	3	2018-04-23	Step daughter brother.	4
tpinyon21@amazonaws.com	marktaylor@haynes.com	4	2018-04-25	Decision peace buy two audience plant can.	2
tpinyon21@amazonaws.com	marktaylor@haynes.com	5	2018-04-25	Town personal care.	0
mdermot1m@foxnews.com	shelley15@moran.com	1	2015-08-20	Already hundred house meet thought training.	3
amconie25@com.com	uhoward@thomas.net	1	2012-04-18	Girl history federal per.	5
kmcwhirter1s@ibm.com	uhoward@thomas.net	1	2014-07-05	Perhaps view physical news.	5
kmcwhirter1s@ibm.com	uhoward@thomas.net	2	2017-09-29	Difficult former teach himself.	2
kmcwhirter1s@ibm.com	uhoward@thomas.net	3	2018-02-16	Wear support inside fact.	0
kmcwhirter1s@ibm.com	uhoward@thomas.net	4	2018-03-28	Maintain long young decision and Congress nothing.	4
kmcwhirter1s@ibm.com	uhoward@thomas.net	5	2018-04-17	So sound sit always.	3
cpirdy1p@usatoday.com	richardallison@morrow-jackson.net	1	2016-12-20	Science relate born international small.	1
cpirdy1p@usatoday.com	richardallison@morrow-jackson.net	2	2018-01-05	Resource responsibility morning good.	0
rglide2k@ox.ac.uk	jennifermedina@edwards.com	1	2017-07-16	Law then parent medical international.	0
rglide2k@ox.ac.uk	jennifermedina@edwards.com	2	2017-08-06	Carry letter road must cost.	0
rglide2k@ox.ac.uk	jennifermedina@edwards.com	3	2017-12-19	Because into loss author often guy.	1
sidle2d@sfgate.com	sarah68@fletcher.com	1	2014-08-12	Claim culture under house any over.	3
sidle2d@sfgate.com	sarah68@fletcher.com	2	2016-06-11	Structure TV past affect such.	0
sidle2d@sfgate.com	sarah68@fletcher.com	3	2016-07-13	Place responsibility thing official.	2
sidle2d@sfgate.com	sarah68@fletcher.com	4	2017-09-27	Eye rate base already present.	3
sidle2d@sfgate.com	sarah68@fletcher.com	5	2018-03-19	Position man view something wear education.	5
hthowless2f@opensource.org	douglashatfield@decker-woodard.com	1	2017-03-03	Free speak scene glass conference right.	3
hthowless2f@opensource.org	douglashatfield@decker-woodard.com	2	2018-03-20	Tend police day.	5
hthowless2f@opensource.org	douglashatfield@decker-woodard.com	3	2018-04-13	Ask back door impact pay recent be daughter.	1
ashadwick1h@spiegel.de	douglashatfield@decker-woodard.com	1	2017-06-16	Scientist assume area activity scene.	3
lbattill2p@apache.org	trananthony@brown.com	1	2018-02-23	Note enter possible tend fire forward.	1
edilgarno28@canalblog.com	nguyenryan@peck.com	1	2018-02-16	Necessary play police upon television type.	1
edilgarno28@canalblog.com	nguyenryan@peck.com	2	2018-03-09	Safe rise face bring quality decision floor.	3
cshevlin2e@comcast.net	flyons@wall.com	1	2016-03-11	First economic main social federal.	3
cshevlin2e@comcast.net	flyons@wall.com	2	2016-11-21	Thing bar owner hour performance civil.	0
cshevlin2e@comcast.net	flyons@wall.com	3	2018-02-25	Simple fact begin manager.	1
cshevlin2e@comcast.net	flyons@wall.com	4	2018-03-02	Break no billion rather.	0
cshevlin2e@comcast.net	flyons@wall.com	5	2018-04-22	Station near west almost book.	0
mgehring1q@spiegel.de	ronald25@fields.com	2	2016-07-10	Wide if share box need condition.	5
mgehring1q@spiegel.de	ronald25@fields.com	3	2016-08-13	Indicate laugh news go party worry believe spend.	2
eclubb2r@hp.com	kevinsalinas@potts.net	1	2017-07-04	American cause check when detail girl positive across.	0
eclubb2r@hp.com	kevinsalinas@potts.net	2	2017-10-26	Try seven policy.	1
eclubb2r@hp.com	kevinsalinas@potts.net	3	2017-12-05	Card order sing day.	2
rinnocenti1r@comcast.net	petersonmichael@miller-gray.com	1	2016-02-27	Such mind cold around war while.	0
rinnocenti1r@comcast.net	petersonmichael@miller-gray.com	2	2016-07-20	Central child determine energy office sure.	1
rinnocenti1r@comcast.net	petersonmichael@miller-gray.com	3	2016-10-10	Many government to range someone military.	3
rinnocenti1r@comcast.net	petersonmichael@miller-gray.com	4	2017-06-08	Whom care camera game way seat.	4
gbortolussi1z@yahoo.com	ruizheather@freeman.com	1	2017-04-28	Particularly marriage seek research.	1
kstickler20@nbcnews.com	sarah68@fletcher.com	1	2017-09-22	Play fact center name treat base federal.	3
kstickler20@nbcnews.com	sarah68@fletcher.com	2	2017-11-05	Nature American end religious we Republican security.	3
ashadwick1h@spiegel.de	sarah68@fletcher.com	1	2014-03-16	Such hope his room.	2
ashadwick1h@spiegel.de	sarah68@fletcher.com	2	2016-06-20	Car such include apply word.	5
ashadwick1h@spiegel.de	sarah68@fletcher.com	3	2018-01-16	Strategy leader story style.	0
lbattill2p@apache.org	richardallison@morrow-jackson.net	1	2017-12-08	Step case trade in piece against inside.	2
lbattill2p@apache.org	richardallison@morrow-jackson.net	2	2017-12-17	Create fish field spend charge member onto.	5
lbattill2p@apache.org	richardallison@morrow-jackson.net	3	2018-03-24	Paper old plant everyone better.	3
lbattill2p@apache.org	richardallison@morrow-jackson.net	4	2018-04-07	Order more law production.	4
tlummis2n@springer.com	cindywong@clark-torres.biz	1	2015-06-26	Record say house their person catch table parent.	3
rinnocenti1r@comcast.net	flyons@wall.com	1	2014-08-20	Full writer draw voice age computer.	0
rinnocenti1r@comcast.net	flyons@wall.com	2	2015-05-15	Southern wind least third.	3
rinnocenti1r@comcast.net	flyons@wall.com	3	2017-12-07	Meet because us over accept spend.	5
rinnocenti1r@comcast.net	flyons@wall.com	4	2018-02-11	Hope building network cup.	3
rinnocenti1r@comcast.net	flyons@wall.com	5	2018-04-10	Site book whole play.	2
sidle2d@sfgate.com	shelley15@moran.com	1	2013-03-12	Its police class wear speech computer clearly.	1
mgehring1q@spiegel.de	petersonmichael@miller-gray.com	1	2017-09-21	Senior country politics capital threat job.	4
mgehring1q@spiegel.de	petersonmichael@miller-gray.com	2	2017-11-30	Pull six business open feel spend door.	1
mgehring1q@spiegel.de	petersonmichael@miller-gray.com	3	2017-12-10	Between popular four simple effect whole.	4
mgehring1q@spiegel.de	petersonmichael@miller-gray.com	4	2018-01-26	Forward happy floor million.	1
mgehring1q@spiegel.de	petersonmichael@miller-gray.com	5	2018-03-17	Stage knowledge former black million.	4
rbarents26@naver.com	lgarza@moreno-villarreal.com	1	2018-03-04	Commercial knowledge computer growth capital.	4
rbarents26@naver.com	lgarza@moreno-villarreal.com	2	2018-04-06	Any performance find.	2
rbarents26@naver.com	lgarza@moreno-villarreal.com	3	2018-04-16	On executive allow executive around production.	3
rbarents26@naver.com	lgarza@moreno-villarreal.com	4	2018-04-18	Big trip set skill arrive drive smile.	4
rbarents26@naver.com	lgarza@moreno-villarreal.com	5	2018-04-20	He get evening.	2
sidle2d@sfgate.com	lowetimothy@collier.biz	1	2016-06-08	Music kid majority late system participant.	5
sidle2d@sfgate.com	lowetimothy@collier.biz	2	2017-06-23	Past writer more light former choose.	0
sidle2d@sfgate.com	lowetimothy@collier.biz	3	2018-04-02	Environment condition impact campaign well market.	1
sidle2d@sfgate.com	lowetimothy@collier.biz	4	2018-04-12	Matter someone ready.	1
eclubb2r@hp.com	trananthony@brown.com	1	2016-02-13	They half those reason analysis office brother.	3
eclubb2r@hp.com	trananthony@brown.com	2	2016-12-15	Change wrong write police color then their level.	2
eclubb2r@hp.com	trananthony@brown.com	3	2017-03-19	House politics senior most step movement.	5
eclubb2r@hp.com	trananthony@brown.com	4	2018-01-23	Style miss administration audience model outside door.	2
\.


--
-- Data for Name: relawan; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.relawan (email, no_hp, tanggal_lahir) FROM stdin;
fbartrop1e@sciencedaily.com	081284560601	1996-11-25
rtalmadge1f@php.net	081284560602	2008-11-14
akezar1g@cdbaby.com	081284560603	1993-09-24
ashadwick1h@spiegel.de	081284560604	1989-12-25
pstubbley1i@flavors.me	081284560605	2017-11-19
kbruineman1j@wikipedia.org	081284560606	1990-07-13
aligertwood1k@who.int	081284560607	2014-05-10
pgebb1l@yahoo.com	081284560608	2012-04-06
mdermot1m@foxnews.com	081284560609	2012-03-16
bfeldhorn1n@gnu.org	081284560610	2004-04-08
hbloomfield1o@tuttocitta.it	081284560611	2016-05-31
cpirdy1p@usatoday.com	081284560612	1981-09-24
mgehring1q@spiegel.de	081284560613	1983-06-19
rinnocenti1r@comcast.net	081284560614	2007-11-18
kmcwhirter1s@ibm.com	081284560615	2005-09-05
pcases1t@4shared.com	081284560616	2015-01-05
jgot1u@hao123.com	081284560617	2005-11-29
lcollopy1v@so-net.ne.jp	081284560618	1992-12-08
bgreber1w@moonfruit.com	081284560619	1996-12-12
asouthern1x@mit.edu	081284560620	2008-08-09
flyford1y@homestead.com	081284560621	1995-11-26
gbortolussi1z@yahoo.com	081284560622	1984-11-07
kstickler20@nbcnews.com	081284560623	1984-02-23
tpinyon21@amazonaws.com	081284560624	1998-03-31
tmayhead22@hc360.com	081284560625	2004-05-10
gstebbings23@ow.ly	081284560626	2016-10-05
swallworke24@imageshack.us	081284560627	1992-04-23
amconie25@com.com	081284560628	1983-04-14
rbarents26@naver.com	081284560629	1983-10-28
ivieyra27@cbslocal.com	081284560630	2014-07-15
edilgarno28@canalblog.com	081284560631	1998-01-10
jgeorgeon29@sbwire.com	081284560632	1988-09-04
afortoun2a@imdb.com	081284560633	1991-12-12
cbradnick2b@addtoany.com	081284560634	2004-02-04
awhatman2c@twitpic.com	081284560635	1982-06-29
sidle2d@sfgate.com	081284560636	2013-12-16
cshevlin2e@comcast.net	081284560637	2011-05-19
hthowless2f@opensource.org	081284560638	1990-04-01
vvillaret2g@utexas.edu	081284560639	1994-11-29
kstainton2h@state.gov	081284560640	2001-07-13
bbiggerdike2i@unesco.org	081284560641	1990-12-09
acrawshaw2j@hexun.com	081284560642	1998-05-03
rglide2k@ox.ac.uk	081284560643	1995-11-17
njameson2l@yandex.ru	081284560644	2001-02-03
cbream2m@shareasale.com	081284560645	2014-06-10
tlummis2n@springer.com	081284560646	1998-10-21
bfishbourn2o@berkeley.edu	081284560647	1988-07-03
lbattill2p@apache.org	081284560648	2004-06-12
sgeerits2q@tiny.cc	081284560649	1992-06-09
eclubb2r@hp.com	081284560650	1999-07-14
relawan1@relawan.id	081234567890	1998-08-29
\.


--
-- Data for Name: relawan_organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.relawan_organisasi (email_relawan, organisasi) FROM stdin;
kmcwhirter1s@ibm.com	shelley15@moran.com
kstainton2h@state.gov	lpotts@mayo.org
hthowless2f@opensource.org	ronald25@fields.com
hthowless2f@opensource.org	jennifermedina@edwards.com
amconie25@com.com	wterry@jones.org
njameson2l@yandex.ru	griffithwilliam@freeman.com
pcases1t@4shared.com	ruizheather@freeman.com
tpinyon21@amazonaws.com	xenglish@mcdaniel.com
swallworke24@imageshack.us	marktaylor@haynes.com
asouthern1x@mit.edu	lowetimothy@collier.biz
kstainton2h@state.gov	flyons@wall.com
mdermot1m@foxnews.com	sarah68@fletcher.com
fbartrop1e@sciencedaily.com	nguyenryan@peck.com
bgreber1w@moonfruit.com	lgarza@moreno-villarreal.com
vvillaret2g@utexas.edu	douglashatfield@decker-woodard.com
cpirdy1p@usatoday.com	ruizheather@freeman.com
kstainton2h@state.gov	thomaspeters@rodriguez.com
asouthern1x@mit.edu	kevincannon@sims.biz
aligertwood1k@who.int	jenningselizabeth@sweeney.com
awhatman2c@twitpic.com	sharonhensley@thomas.com
ivieyra27@cbslocal.com	jennifermedina@edwards.com
bfeldhorn1n@gnu.org	ronald25@fields.com
kbruineman1j@wikipedia.org	ryanmarshall@greene.com
afortoun2a@imdb.com	michaelrogers@johnston-zavala.com
rtalmadge1f@php.net	michaelrogers@johnston-zavala.com
ivieyra27@cbslocal.com	ryanmarshall@greene.com
afortoun2a@imdb.com	flyons@wall.com
hbloomfield1o@tuttocitta.it	ruizheather@freeman.com
fbartrop1e@sciencedaily.com	wterry@jones.org
awhatman2c@twitpic.com	kendrapatterson@salinas.com
njameson2l@yandex.ru	richardallison@morrow-jackson.net
afortoun2a@imdb.com	shelley15@moran.com
swallworke24@imageshack.us	kendrapatterson@salinas.com
pgebb1l@yahoo.com	michaelrogers@johnston-zavala.com
bfishbourn2o@berkeley.edu	sharonhensley@thomas.com
rglide2k@ox.ac.uk	jennifermedina@edwards.com
akezar1g@cdbaby.com	douglashatfield@decker-woodard.com
kstickler20@nbcnews.com	shelley15@moran.com
flyford1y@homestead.com	ryanmarshall@greene.com
amconie25@com.com	kevincannon@sims.biz
hbloomfield1o@tuttocitta.it	jenningselizabeth@sweeney.com
akezar1g@cdbaby.com	jenningselizabeth@sweeney.com
jgeorgeon29@sbwire.com	sarah68@fletcher.com
rbarents26@naver.com	uhoward@thomas.net
bfishbourn2o@berkeley.edu	xenglish@mcdaniel.com
rinnocenti1r@comcast.net	kevincannon@sims.biz
sgeerits2q@tiny.cc	thomaspeters@rodriguez.com
cbradnick2b@addtoany.com	kendrapatterson@salinas.com
swallworke24@imageshack.us	ruizheather@freeman.com
mdermot1m@foxnews.com	flyons@wall.com
aligertwood1k@who.int	ryanmarshall@greene.com
kstickler20@nbcnews.com	flyons@wall.com
afortoun2a@imdb.com	nguyenryan@peck.com
pgebb1l@yahoo.com	kevinsalinas@potts.net
cshevlin2e@comcast.net	jenningselizabeth@sweeney.com
afortoun2a@imdb.com	jennifermedina@edwards.com
aligertwood1k@who.int	xenglish@mcdaniel.com
rtalmadge1f@php.net	douglashatfield@decker-woodard.com
kstainton2h@state.gov	jennifermedina@edwards.com
asouthern1x@mit.edu	shelley15@moran.com
acrawshaw2j@hexun.com	uhoward@thomas.net
pgebb1l@yahoo.com	sarah68@fletcher.com
sidle2d@sfgate.com	cindywong@clark-torres.biz
acrawshaw2j@hexun.com	douglashatfield@decker-woodard.com
flyford1y@homestead.com	griffithwilliam@freeman.com
afortoun2a@imdb.com	douglashatfield@decker-woodard.com
jgot1u@hao123.com	thomaspeters@rodriguez.com
vvillaret2g@utexas.edu	flyons@wall.com
mgehring1q@spiegel.de	ryanmarshall@greene.com
mgehring1q@spiegel.de	griffithwilliam@freeman.com
cbream2m@shareasale.com	marc24@lewis.org
lbattill2p@apache.org	kevincannon@sims.biz
bbiggerdike2i@unesco.org	cindywong@clark-torres.biz
tlummis2n@springer.com	sharonhensley@thomas.com
kmcwhirter1s@ibm.com	uhoward@thomas.net
lcollopy1v@so-net.ne.jp	ruizheather@freeman.com
bfishbourn2o@berkeley.edu	kendrapatterson@salinas.com
rbarents26@naver.com	ruizheather@freeman.com
rtalmadge1f@php.net	lgarza@moreno-villarreal.com
aligertwood1k@who.int	kevincannon@sims.biz
rtalmadge1f@php.net	jennifermedina@edwards.com
rglide2k@ox.ac.uk	griffithwilliam@freeman.com
rglide2k@ox.ac.uk	marc24@lewis.org
cbradnick2b@addtoany.com	marktaylor@haynes.com
hbloomfield1o@tuttocitta.it	trananthony@brown.com
awhatman2c@twitpic.com	ryanmarshall@greene.com
hbloomfield1o@tuttocitta.it	lpotts@mayo.org
kstainton2h@state.gov	michaelrogers@johnston-zavala.com
pgebb1l@yahoo.com	thomaspeters@rodriguez.com
tmayhead22@hc360.com	michaelrogers@johnston-zavala.com
pstubbley1i@flavors.me	douglashatfield@decker-woodard.com
flyford1y@homestead.com	ruizheather@freeman.com
bfishbourn2o@berkeley.edu	marktaylor@haynes.com
amconie25@com.com	lowetimothy@collier.biz
eclubb2r@hp.com	kevinsalinas@potts.net
bfeldhorn1n@gnu.org	lpotts@mayo.org
awhatman2c@twitpic.com	petersonmichael@miller-gray.com
mgehring1q@spiegel.de	michaelrogers@johnston-zavala.com
aligertwood1k@who.int	flyons@wall.com
asouthern1x@mit.edu	lgarza@moreno-villarreal.com
eclubb2r@hp.com	jenningselizabeth@sweeney.com
asouthern1x@mit.edu	uhoward@thomas.net
pcases1t@4shared.com	xenglish@mcdaniel.com
gstebbings23@ow.ly	ruizheather@freeman.com
bfeldhorn1n@gnu.org	cindywong@clark-torres.biz
kmcwhirter1s@ibm.com	petersonmichael@miller-gray.com
cshevlin2e@comcast.net	kendrapatterson@salinas.com
tmayhead22@hc360.com	thomaspeters@rodriguez.com
awhatman2c@twitpic.com	sarah68@fletcher.com
tmayhead22@hc360.com	douglashatfield@decker-woodard.com
\.


--
-- Data for Name: reward; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.reward (kode_kegiatan, barang_reward, harga_min, harga_max) FROM stdin;
YujtGdlPLXQqvkKrGDuD	Build	97488882	1949286776
wrtadwPAbxGcHNdfODnB	Rest	89736473	729345293
OsrgGTQcTVWGwRKWRHXI	Full	23879903	705496197
oCJijwMmNJpWDQpIcXUQ	Avoid	150830232	530701183
OEODpLpwHlrVZIJZYHFh	Choice	161264187	528880628
SgEfsZCKGqYrMtBkcqyj	Power	147959373	1381205365
OKcRTVAZJSZALvMDYJfT	Together	83959798	731600069
jhtPekJETGwrNQnHLYan	Certainly	169831858	704506894
GINuFEMVNpewCOPlFAgT	Manager	183895502	408075392
YujtGdlPLXQqvkKrGDuD	Land	31347026	1420620000
gVxEOWXRhdDoqIQYenTe	Learn	163652796	786793613
ZBiiLFBoYEuMUdfetGzg	Thank	25625457	1745231415
DvyFjumsgrWqLhtXSXCR	Each	33024191	1404707704
OsrgGTQcTVWGwRKWRHXI	Article	180161701	944364191
rViakiLlouaYvslYgHvC	Particular	179664807	336376584
DBIDEXWaQGhQGtmWSBXY	Miss	39634815	1339930605
ifnWlCQOaWXrvWYoUEEj	Than	22278327	188518459
sVvjfAbHyEUtebTyFdEc	Serious	28208668	775246088
tDCtxsxbkbnvgiozPqgJ	Card	119385197	1872487327
uwLnXkzyTcPtSqjyunGS	System	97588294	1202232532
OfcsezgzEooUCuwGOhmb	Here	58887677	762731883
awBdmbqIYpIaQlEePKXG	Evening	121798742	1130660546
kGJrjWLYYMMcTCRRRCsQ	Because	137276625	1323500002
PZluZwQdywdmBuAEyWso	None	51560826	1269223975
iwLgdZcpWzKfavVWlvNm	Mrs	52145528	1178960143
lhMuerIPadCePhpTeneg	Specific	41898344	523593205
yRKJyIaonxlBZbISgtwS	Rock	41043192	801112030
ARIKBCBjjCFAhDzTijIe	Material	183963606	946421623
NcpjSfxNwODNRbSwKMvP	Act	71224581	973021444
OXasjRqPFGNuIXASbzIh	Since	142175887	918752590
SjdMssiJsIuDaxtPaadt	Side	137039857	169765082
RSYTIJzlvonWLZawvpWW	Stand	70160792	1102512326
kGJrjWLYYMMcTCRRRCsQ	His	67364956	1305987783
lhMuerIPadCePhpTeneg	Reason	97631218	734920524
adHELOGnOQoAWxxOOxqZ	Personal	129911381	980484923
HLareEiBLeTUnZXWEpIX	Guy	55788064	906725678
kmqMSHdLlFMXRvdOcYnC	Risk	44970179	1859939688
jwdXZfahMoZLsrLBDMCZ	Rate	66356319	1874712036
lmoYaVUsaDbClPRlHYxA	Suggest	111874097	1602197436
aBodfVzEbxAXHdMZHRoi	Protect	130008249	853708307
VfzzZfxDkOzMWpIilYka	Eye	96174635	1163942138
pblSsWSTxSHKAYLoBizj	Risk	149256210	838558330
AArIhcRgghqWLjVrmrac	Can	41771010	979251009
zKAtUrlOkcNGeovvnYoJ	Protect	1401048	1885606110
eFLyOHiKXBRQYGysPGCv	Agreement	108374608	151453704
bkMTjPkeCskWbwuEXpKc	Should	194532959	938533393
emgKjuWWpMpnwPKxUUfG	Next	43954529	1758464411
RqgeLdoTPECfjhPKcAUC	Shoulder	8720887	552060169
YujtGdlPLXQqvkKrGDuD	Entire	150825917	1882097153
NpJFXNUbWryizCLyswMH	Purpose	10911321	1187681876
feptvFDGTvFhfQgIKXWI	Agent	94446382	1739791761
vjtQXDqDbzrxbYZhgjPY	Remember	117206171	837796119
iafJQkAFrLKbbjZQgAhI	Understand	181300167	716271602
tnAWtpaTLqRVFpxTFUMz	Marriage	83553569	336069332
tnAWtpaTLqRVFpxTFUMz	Buy	142084582	1203849574
iwLgdZcpWzKfavVWlvNm	Reach	152805708	1192273541
yhKWyBHzFLCttDhcxgSY	Interesting	135976299	774926698
FeHbHHCrkkOtJUOzVKjn	Star	188499204	1709477369
DJgViLfeIiBYwcVzTuSN	Determine	92582176	1206358991
cXHZxCfUsiATFYBLQpwz	Society	155447802	1621712728
lhjzGExcbZuijkFrpqik	College	85777810	517479631
MuJtQBfNBsPHqrFMPHeQ	Area	122183483	1833707914
ARIKBCBjjCFAhDzTijIe	Media	176123250	1385347660
SOiupDYJvVVoGEwqHdJG	Offer	148951053	1218637002
lFQzWXHzNspnvIZhFvoA	Future	124768432	1202758038
feptvFDGTvFhfQgIKXWI	Every	177319864	994368821
OXasjRqPFGNuIXASbzIh	Serve	83451670	898904061
CnRBkPbkHYVUYwYctdPZ	Visit	144546927	1772992849
eoqHjbtdpSStiuGMSUSy	Business	113955887	1236051276
iwLgdZcpWzKfavVWlvNm	Relate	178234329	1063588730
JLtPSphRSpHTMvxkRhAN	Reach	166764163	1145025144
adHELOGnOQoAWxxOOxqZ	Degree	124542988	701655240
AvVOMQHmAwnyLDqUlGmi	Trouble	18510576	82285369
OqLsxihlvHLUSOaNeaIz	Side	6764414	899151584
kGJrjWLYYMMcTCRRRCsQ	Prevent	172616898	857950928
\.


--
-- Data for Name: sponsor; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.sponsor (email, logo_sponsor) FROM stdin;
dcrome0@wikispaces.com	http://dummyimage.com/216x235.png/dddddd/000000
cpaur1@ihg.com	http://dummyimage.com/203x245.bmp/cc0000/ffffff
lsweet2@salon.com	http://dummyimage.com/203x204.png/dddddd/000000
mwyard3@dagondesign.com	http://dummyimage.com/195x100.jpg/5fa2dd/ffffff
kdruce4@smugmug.com	http://dummyimage.com/220x221.bmp/dddddd/000000
gcalvie5@kickstarter.com	http://dummyimage.com/210x123.jpg/5fa2dd/ffffff
vcaroli6@mapy.cz	http://dummyimage.com/250x159.bmp/cc0000/ffffff
wbagworth7@bloomberg.com	http://dummyimage.com/223x192.png/5fa2dd/ffffff
jmcroberts8@opera.com	http://dummyimage.com/206x123.png/cc0000/ffffff
bpallent9@baidu.com	http://dummyimage.com/213x237.jpg/dddddd/000000
eblancowea@alibaba.com	http://dummyimage.com/169x135.jpg/dddddd/000000
aaddionisiob@princeton.edu	http://dummyimage.com/111x227.jpg/cc0000/ffffff
lpitkethlyc@tiny.cc	http://dummyimage.com/120x184.png/5fa2dd/ffffff
mstickingsd@simplemachines.org	http://dummyimage.com/105x116.bmp/cc0000/ffffff
mgrzesiewicze@histats.com	http://dummyimage.com/121x226.jpg/ff4444/ffffff
zlosebief@disqus.com	http://dummyimage.com/136x165.jpg/5fa2dd/ffffff
afoneg@biglobe.ne.jp	http://dummyimage.com/136x168.png/dddddd/000000
akistingh@imgur.com	http://dummyimage.com/107x249.bmp/ff4444/ffffff
cmillsomi@so-net.ne.jp	http://dummyimage.com/107x231.bmp/5fa2dd/ffffff
bcroanj@pbs.org	http://dummyimage.com/234x183.bmp/ff4444/ffffff
striggk@i2i.jp	http://dummyimage.com/228x196.jpg/ff4444/ffffff
gjirikl@sciencedirect.com	http://dummyimage.com/231x154.bmp/cc0000/ffffff
bellardm@harvard.edu	http://dummyimage.com/124x147.png/cc0000/ffffff
svanichkovn@phpbb.com	http://dummyimage.com/162x113.png/cc0000/ffffff
tcurthoyso@arstechnica.com	http://dummyimage.com/151x170.png/cc0000/ffffff
sponsor1@sponsor.id	https://i.imgur.com/CWRWR7o.jpg
\.


--
-- Data for Name: sponsor_organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.sponsor_organisasi (sponsor, organisasi, tanggal, nominal) FROM stdin;
svanichkovn@phpbb.com	ruizheather@freeman.com	2013-06-12	1857994707
cmillsomi@so-net.ne.jp	lgarza@moreno-villarreal.com	2014-04-20	1800555678
cmillsomi@so-net.ne.jp	ryanmarshall@greene.com	2017-03-20	366618735
eblancowea@alibaba.com	douglashatfield@decker-woodard.com	2016-07-12	1594957363
cpaur1@ihg.com	jacquelinehood@bowman-lewis.biz	2012-11-14	1764139450
afoneg@biglobe.ne.jp	marktaylor@haynes.com	2012-05-11	1930640472
striggk@i2i.jp	ryanmarshall@greene.com	2012-10-13	176717799
wbagworth7@bloomberg.com	douglashatfield@decker-woodard.com	2011-11-10	1373787799
mgrzesiewicze@histats.com	sarah68@fletcher.com	2015-12-30	679724967
eblancowea@alibaba.com	kevinsalinas@potts.net	2018-01-27	461448336
afoneg@biglobe.ne.jp	lowetimothy@collier.biz	2016-12-18	1151197889
mgrzesiewicze@histats.com	sharonhensley@thomas.com	2012-11-01	542533539
gcalvie5@kickstarter.com	michaelrogers@johnston-zavala.com	2016-10-03	1339972859
bellardm@harvard.edu	sarah68@fletcher.com	2010-04-01	570624573
mwyard3@dagondesign.com	shelley15@moran.com	2011-11-02	378922039
cmillsomi@so-net.ne.jp	michaelrogers@johnston-zavala.com	2013-09-17	997079107
aaddionisiob@princeton.edu	jacquelinehood@bowman-lewis.biz	2016-12-26	1396468794
gcalvie5@kickstarter.com	douglashatfield@decker-woodard.com	2011-11-16	1037758582
bellardm@harvard.edu	kevinsalinas@potts.net	2012-09-12	1682437708
cpaur1@ihg.com	uhoward@thomas.net	2013-09-01	330647488
mgrzesiewicze@histats.com	shelley15@moran.com	2013-07-06	1800058165
jmcroberts8@opera.com	richardallison@morrow-jackson.net	2017-02-14	8361652
bpallent9@baidu.com	marktaylor@haynes.com	2015-07-30	1804041099
bcroanj@pbs.org	kevinsalinas@potts.net	2010-12-13	1408714591
bcroanj@pbs.org	sarah68@fletcher.com	2014-11-30	926215813
mgrzesiewicze@histats.com	michaelrogers@johnston-zavala.com	2014-07-05	851909741
bellardm@harvard.edu	wterry@jones.org	2010-08-26	1022543698
akistingh@imgur.com	jenningselizabeth@sweeney.com	2011-09-30	632768645
akistingh@imgur.com	nguyenryan@peck.com	2018-04-01	157707207
mgrzesiewicze@histats.com	thomaspeters@rodriguez.com	2016-09-22	1711755125
lsweet2@salon.com	jennifermedina@edwards.com	2015-02-21	1754970582
kdruce4@smugmug.com	jacquelinehood@bowman-lewis.biz	2012-04-07	1436246587
jmcroberts8@opera.com	douglashatfield@decker-woodard.com	2011-09-28	853893688
eblancowea@alibaba.com	ruizheather@freeman.com	2015-07-22	345835888
mwyard3@dagondesign.com	jenningselizabeth@sweeney.com	2015-09-08	839422583
tcurthoyso@arstechnica.com	thomaspeters@rodriguez.com	2018-01-04	1035578143
cpaur1@ihg.com	nguyenryan@peck.com	2017-01-25	1254435680
akistingh@imgur.com	shelley15@moran.com	2014-02-05	248505754
striggk@i2i.jp	sharonhensley@thomas.com	2010-12-09	1929386316
mstickingsd@simplemachines.org	marc24@lewis.org	2014-07-13	970710986
eblancowea@alibaba.com	richardallison@morrow-jackson.net	2013-12-09	673551139
dcrome0@wikispaces.com	lpotts@mayo.org	2012-11-14	84997926
gcalvie5@kickstarter.com	flyons@wall.com	2011-01-03	1944118828
mstickingsd@simplemachines.org	petersonmichael@miller-gray.com	2017-11-28	435225906
kdruce4@smugmug.com	ronald25@fields.com	2012-01-18	847787966
mwyard3@dagondesign.com	sarah68@fletcher.com	2018-04-04	677047263
striggk@i2i.jp	richardallison@morrow-jackson.net	2015-09-13	843544145
gcalvie5@kickstarter.com	xenglish@mcdaniel.com	2016-03-07	1492878489
bellardm@harvard.edu	kendrapatterson@salinas.com	2018-02-02	1487197973
dcrome0@wikispaces.com	ryanmarshall@greene.com	2016-12-04	906808639
\.


--
-- Data for Name: tujuan_organisasi; Type: TABLE DATA; Schema: public; Owner: adalberht
--

COPY public.tujuan_organisasi (organisasi, tujuan) FROM stdin;
ruizheather@freeman.com	enable impactful applications
richardallison@morrow-jackson.net	deploy out-of-the-box e-services
lgarza@moreno-villarreal.com	facilitate dynamic platforms
kevincannon@sims.biz	visualize viral info-mediaries
jacquelinehood@bowman-lewis.biz	target clicks-and-mortar architectures
lpotts@mayo.org	reinvent wireless communities
marc24@lewis.org	aggregate visionary applications
sharonhensley@thomas.com	morph front-end supply-chains
nguyenryan@peck.com	facilitate leading-edge interfaces
shelley15@moran.com	orchestrate clicks-and-mortar functionalities
trananthony@brown.com	synergize cutting-edge e-services
xenglish@mcdaniel.com	envisioneer sticky convergence
wterry@jones.org	generate world-class platforms
jenningselizabeth@sweeney.com	empower clicks-and-mortar relationships
ryanmarshall@greene.com	optimize value-added deliverables
organisasi3@organisasi.com	Visi Misi 1
organisasi4@organisasi.com	tujuan nomer 1
organisasi6@organisasi.com	tujuan nomer 1
\.


--
-- Data for Name: user_sion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_sion (password, last_login, email, nama, alamat_lengkap, is_superuser) FROM stdin;
pbkdf2_sha256$100000$UMI01uYCaNLv$5LI9OpB7/ydOtRPFV/GIwKh0JGS9IF242ckaWkX6iuU=	\N	lsweet2@salon.com	Linet Sweet	189 Vermont Avenue	f
pbkdf2_sha256$100000$HoAODldT1wgT$/qGlOPVwpmK76rQEnmOcfCCGfnUP+giyl8JKnzULgxw=	\N	kdruce4@smugmug.com	Kriste Druce	4545 Fairview Junction	f
pbkdf2_sha256$100000$1YUUgDqhdXh8$sZtXA0z8q4ppipXrV+aneCQn1BDPdxoA8U/4U9FyTFI=	\N	gcalvie5@kickstarter.com	Guenna Calvie	190 Armistice Junction	f
pbkdf2_sha256$100000$9XeokfJJBKcv$6KaDk7RwWzFT51eC7cWbRd6O/7mg5O1NJc7RveyMphQ=	\N	wbagworth7@bloomberg.com	Walton Bagworth	8 Kropf Trail	f
pbkdf2_sha256$100000$S7lBEmeXI9iu$GqNWjlRbQIFvFjlSlxcuYtPA5Rfl0hncxUE2KuSQh28=	\N	bpallent9@baidu.com	Brant Pallent	378 Hanson Avenue	f
pbkdf2_sha256$100000$jgDWDLUmpMrM$AfbClkjDwRfg8A4V506dC90P7ZG/Zycf1AOVVsCM9GM=	\N	eblancowea@alibaba.com	Enrichetta Blancowe	98824 Manley Trail	f
pbkdf2_sha256$100000$CyksjOPq7kL8$T3FuXMy6ybaqHe5qXgWDU18Y+SYmk1iCfJ9zsFOdVuA=	\N	lpitkethlyc@tiny.cc	Laverne Pitkethly	034 Delaware Drive	f
pbkdf2_sha256$100000$46VqzCCVm6vU$U+VLpD+UJKH/m6qqOEwMXD2NHf5fbphbY15wBYEJ/Kg=	\N	mgrzesiewicze@histats.com	Marla Grzesiewicz	5557 Surrey Trail	f
pbkdf2_sha256$100000$U1dvpq3VuKkB$WSBC/hjAyidilHavwl2V8JGtlFcxoPlvzyZcypQClxQ=	\N	afoneg@biglobe.ne.jp	Almeda Fone	46 Maple Pass	f
pbkdf2_sha256$100000$KhGmnycbHNjn$pyFSyJ5xu47/qOtQcs3mdf4Dzjc1tpfQxmSID9iHeyI=	\N	akistingh@imgur.com	Allyn Kisting	0 Stuart Center	f
pbkdf2_sha256$100000$Uz0zho7PwlHG$7AvC89wXQM4AozkRVkvpRod0gl/D7pvnR1Nq4ly4BJA=	\N	bcroanj@pbs.org	Bertram Croan	468 Huxley Junction	f
pbkdf2_sha256$100000$WZ9IhYbKgYQf$6P9/pjPPBvc+MOwb40qV4jHyMYyaRL/Wr5qoMOki3bk=	\N	gjirikl@sciencedirect.com	Geoff Jirik	5 Mallory Parkway	f
pbkdf2_sha256$100000$SJPb1yhAVqZO$FrEfLyUy4qlOpwSwHmCwjGaXOUZ0o8Qp1TRWH+r6oPE=	\N	svanichkovn@phpbb.com	Steffane Vanichkov	726 Stephen Street	f
pbkdf2_sha256$100000$xUNg6fBF3RKv$6qFOzmReB0MtRAbZ6L7qQhssKgycn7/uHpQlkvDwjmU=	\N	tcurthoyso@arstechnica.com	Theda Curthoys	446 Tony Drive	f
pbkdf2_sha256$100000$s7UGkUgcTXps$I/0EppBDFBjrl+8jxVPgIFS5EbSMp4eOJkAKpgHB1EE=	\N	blaydonq@ocn.ne.jp	Benny Laydon	9395 Nobel Pass	f
pbkdf2_sha256$100000$eejRZqzH7Nsh$wYFq1QgZpAZrs9cjgbEyoZwO2EaAZ6/Z2eSXh7euaJA=	\N	dmcbays@berkeley.edu	Danny McBay	7632 Thackeray Circle	f
pbkdf2_sha256$100000$89ZhNOqfAgU2$9HJTgBqScXMSplgaovg38BTbuSQP3cEpMCV6r2vB4zo=	\N	evaissiereu@ibm.com	Elvis Vaissiere	484 Bartillon Place	f
pbkdf2_sha256$100000$svNxXmctx4dX$EHCpMIjtm7jpUIBSbsBn+JdYsO/zdWXHKxQdO3KTWD4=	\N	rwildashv@spotify.com	Rosabella Wildash	0776 Parkside Terrace	f
pbkdf2_sha256$100000$jsdGaDEIlXmW$/p2Q4n1B93FXcBOIRj0H8KanumEi+noP6Swn/XrQevA=	\N	bcundyx@reverbnation.com	Brnaba Cundy	0773 Saint Paul Terrace	f
pbkdf2_sha256$100000$XV0qsuEkhGsy$tk1ZAODx9LugbrgXrgYp3aR6fRV8gs/dMUs/dubJLbo=	\N	aaharoniz@geocities.com	Aubrette Aharoni	87 Roxbury Street	f
pbkdf2_sha256$100000$LGS5ecrhsW1t$tUI+tRJ9RTMZCCUjoFatQjiWroAp+Em0+UBmmY04v5s=	\N	cfraczak11@jugem.jp	Cornela Fraczak	60 Weeping Birch Lane	f
pbkdf2_sha256$100000$82bVI9wBtHO2$m4o4gbepOV7KHfPcPc5CNUKXgEOpN68Otbt9kEb6wsM=	\N	gfisher13@technorati.com	Ginevra Fisher	36603 Autumn Leaf Point	f
pbkdf2_sha256$100000$6fSmthWwJTWE$UgBSUJvs9NQeWw/zLGbROyZgRCdmMsNqqzsY8cqsI7Y=	\N	vmeriot14@wunderground.com	Vin Meriot	669 Holmberg Court	f
pbkdf2_sha256$100000$sQ383TeLnk9w$HnoA0qOZxBD1zlQkOLRh4B7RjplWpya/Z0dPaoocRaQ=	\N	oduffan16@yolasite.com	Orren Duffan	791 Continental Junction	f
pbkdf2_sha256$100000$CSFnqXKdoLob$jfkSujYn5n32YpDU7wGDv4W1xrDP+W4dpyfbJHqcMF4=	\N	jgounel18@tuttocitta.it	Jordain Gounel	87001 Golden Leaf Alley	f
pbkdf2_sha256$100000$AnIaDVUfciYk$SfvDBFFWQOHkZlNC7WJXOotNsrk94lcNDMc94wqMToA=	\N	cjacob1a@webmd.com	Curran Jacob	1 Aberg Point	f
pbkdf2_sha256$100000$WDjl9djQidA8$XGtUiYtG9BCY60rP5c+0fjitF+u0+A8dJcyiaVREgWA=	\N	vokennedy1b@i2i.jp	Vyky O'Kennedy	72 Granby Junction	f
pbkdf2_sha256$100000$UfJMebTcPKIU$3WYt+bx2d9CQQC5UuvcYBRzbLvVdRWZOC3foaJVLLG0=	\N	bpicot1d@businessinsider.com	Burnaby Picot	93 Sachs Avenue	f
pbkdf2_sha256$100000$BjpOrhXLgSAO$jUjL+Ksty6lAsSKKo85ccINwaJjzNuJzpIUEvbEA0pI=	\N	rtalmadge1f@php.net	Rosemary Talmadge	31293 Reinke Street	f
pbkdf2_sha256$100000$gdMNMXz64qty$BC8qixIyVdLMqxqfNWUryuYNepzbu2t5kqW7xOE1/Eg=	\N	ashadwick1h@spiegel.de	Amos Shadwick	33 Comanche Place	f
pbkdf2_sha256$100000$jvkAPxK6byQ4$x3Pso9BOqstA2gxzrB5ise2ppqI3oUMXXbKxOkFHe4Q=	\N	kbruineman1j@wikipedia.org	Kendre Bruineman	6 Anthes Drive	f
pbkdf2_sha256$100000$OJ326AKLjXfX$AyHYjH3WNpX7w2HU6N/04fiwRd6cBzkc1vay/EQgOAs=	\N	aligertwood1k@who.int	Amelina Ligertwood	3729 Kipling Terrace	f
pbkdf2_sha256$100000$WX8KlE5dlDCw$Sway/AeaGpmmirDJXlSaluUm1TY3D9SC3zqF41Diq/k=	\N	mdermot1m@foxnews.com	Mikel Dermot	376 Mayer Street	f
pbkdf2_sha256$100000$UX09zCNbacDK$S2RawPeXxpk9yFxQYu46N8jXVE5K4HYTadCeE/OmJt4=	\N	hbloomfield1o@tuttocitta.it	Hal Bloomfield	25 Novick Junction	f
pbkdf2_sha256$100000$wnJybPu9fHqa$iTuSep0d43V5B2ddLMOgqhcEaMY/jsuBidxYhvs3OM8=	\N	cpirdy1p@usatoday.com	Costanza Pirdy	8531 Dexter Street	f
pbkdf2_sha256$100000$QwCeBuT44qHB$zsMZST+xIMzohSQBJExW0j8PJvMTf/fE3JNS1s0WS/s=	\N	rinnocenti1r@comcast.net	Rog Innocenti	76725 Steensland Avenue	f
pbkdf2_sha256$100000$HcpjkYLQDUrL$/9UIrzeI6YsU9CaIp1dkOJUaqzLa8Q6UU/C3YSoMwRY=	\N	pcases1t@4shared.com	Phyllis Cases	41867 Surrey Pass	f
pbkdf2_sha256$100000$lvG1qpR99OvM$vMHN6ahR99lnyml7h6wcNS8pkeyXw4MVbPlVARxL8vU=	\N	lcollopy1v@so-net.ne.jp	Lek Collopy	54773 Spohn Circle	f
pbkdf2_sha256$100000$LYLKRQy2cs6G$ZDFKFB88e6xRQzyNV4eAwY4FEhF88Oz9RwWAyMFc8Xo=	\N	asouthern1x@mit.edu	Adolph Southern	967 Doe Crossing Court	f
pbkdf2_sha256$100000$UJomlUEhCucR$OEtYDipxfqp72qwqYRvEtzn7TPqygndjHyOgjujIC4g=	\N	flyford1y@homestead.com	Felice Lyford	493 Basil Hill	f
pbkdf2_sha256$100000$DFz48Qlmkaa7$VbaJAx4yfQYQGslISuUdInsGiWEcy8WqYkVqmzoYClA=	\N	kstickler20@nbcnews.com	Klaus Stickler	6956 Pond Street	f
pbkdf2_sha256$100000$M498oav0XuUk$a1OjghDpzqTHQ212S/koIqSvBQss859EWdjnP+cCNDc=	\N	tmayhead22@hc360.com	Tommy Mayhead	585 Hollow Ridge Lane	f
pbkdf2_sha256$100000$XwCEuIlF6Aim$YxUBuOj6kzzI3jsPEFGXL4MP8lkAtZ6rAcaip/4ZpMs=	\N	swallworke24@imageshack.us	Saul Wallworke	07961 Trailsway Alley	f
pbkdf2_sha256$100000$qaN1qgKFO7y4$RHIqhYFmDc9Kf+XVl+2hSt7NPM+z2/SVw6X6p+q4DRs=	\N	amconie25@com.com	Anita McOnie	07 Welch Lane	f
pbkdf2_sha256$100000$3JhnW88KCo6j$54nrDSWXrp3r9fWkDvo+BeEGe5pUX0HLoGSnHlp0C7E=	\N	ivieyra27@cbslocal.com	Inna Vieyra	17385 Lakewood Lane	f
pbkdf2_sha256$100000$YC57y2d2i7LD$gQpYYjdtz9AZYDoxq8OgNIyz/HFIkVCq5zsneMWE3rc=	\N	jgeorgeon29@sbwire.com	Juliane Georgeon	2 Ronald Regan Junction	f
pbkdf2_sha256$100000$nDHmohedHmGb$kH8JFadCtXeKjxuHL0N5wQKRLsSkz2eyIZCYj4ASAPU=	\N	awhatman2c@twitpic.com	Arny Whatman	4 Barnett Plaza	f
pbkdf2_sha256$100000$e07pQ0H3rzWZ$zq/kC6jLRlVjEVpEMG4CZOm8SfAAT3vTzIugfu+zjm0=	\N	sidle2d@sfgate.com	Shannon Idle	10 Graedel Crossing	f
pbkdf2_sha256$100000$FG702FjR8xdd$N6Wnrp+/9biU2790W/S8CspMILTaX/U5/5EXNXXNVdE=	\N	hthowless2f@opensource.org	Herbert Thowless	0358 Hallows Court	f
pbkdf2_sha256$100000$kCwKdq8gyuHE$04oZfGPGS21tdspHKa539wLdCtcM4uxj0fPRxyFrLlQ=	\N	kstainton2h@state.gov	Kerr Stainton - Skinn	2107 Macpherson Crossing	f
pbkdf2_sha256$100000$dtIgtLhXJA8Z$Jhqp6er9tXSjA9pkb5zR5YUgXwRW7HRrr74FFhnmTI4=	\N	acrawshaw2j@hexun.com	Al Crawshaw	70 Park Meadow Road	f
pbkdf2_sha256$100000$JxdEODKOkorL$HEKYlHfpxekf9S4JqOO4Kn7hb8ZdOQFr90C3QmeZKBE=	\N	rglide2k@ox.ac.uk	Ruttger Glide	9 Dixon Street	f
pbkdf2_sha256$100000$TzSPbL21BMxn$dKa69UgEAE+K4tYZmJDJeqyz2kc84xvLvbAr0LOFkvI=	\N	cbream2m@shareasale.com	Carleton Bream	32 Pine View Trail	f
pbkdf2_sha256$100000$By49ntatcP25$7Hmv3147gR6qa0WW+dovl2ED5ur9ygq/tckKKT+j5y0=	\N	bfishbourn2o@berkeley.edu	Bertie Fishbourn	784 Corry Way	f
pbkdf2_sha256$100000$1zffMBhGECeO$pBKR/RL8Ysr+7hqh4t5Rs0P3dDmTpPDI4F+XI4oP5BE=	\N	sgeerits2q@tiny.cc	Susannah Geerits	20 Knutson Crossing	f
pbkdf2_sha256$100000$vJOiKahCSKH6$AbFcM8YQTIZEhNix9OFyKUwSz87rgvDswOE+Fu1ubds=	\N	eclubb2r@hp.com	Erhard Clubb	1 Main Center	f
pbkdf2_sha256$100000$A9WpNDIBuB6r$kX4ISo/Ty5OXRftgs8/4U5R63CvoiJHOWPNElaYsTjc=	\N	rskeldinge2t@sun.com	Rolando Skeldinge	4901 Mcguire Point	f
pbkdf2_sha256$100000$AxmaJLZ2RcXv$ZBgOkCy303RZIrSh796TN93wwcJDF2WLYG1WPn5Jato=	\N	ksidnall2v@1und1.de	Kayle Sidnall	21 Mockingbird Trail	f
pbkdf2_sha256$100000$0wXRczpVBESm$2+fJfOL8TYt/h6iOXY1BxwV1UJ8oGmaC8cK7JnYkzsw=	\N	kgress2x@last.fm	Katharina Gress	1405 International Trail	f
pbkdf2_sha256$100000$I73OtrAlp8Os$Q9yyoEpoazRrY74hfAL9tNQLQJBSwOqfrtn9vOcEhdw=	\N	mcavanaugh2z@dmoz.org	Marco Cavanaugh	578 Northridge Point	f
pbkdf2_sha256$100000$hLAsMVc8YUop$jlTL1mMFpG36NDVJ/KSu/iJM3EG1icOA+lvmXJaj8HA=	\N	ade31@t.co	Ashli De Mattei	324 Roxbury Center	f
pbkdf2_sha256$100000$ax3yUhLqBGl5$fz+Ws+YRBS5BzH2JDsT639InyllAP1eWQ5Kym54X++E=	\N	kduffrie33@live.com	Koressa Duffrie	4344 Vahlen Hill	f
pbkdf2_sha256$100000$H60n5V9K9Kth$U/GMapzG/ASyBWe8JqhWoqVOgWasZC9jOvOlI37RUOo=	\N	scrosse34@usatoday.com	Sharona Crosse	3 Paget Road	f
pbkdf2_sha256$100000$fQClTt6soYAi$KK+Q0gwmQj6u0opgwbXxGJIXam7ZdVIvHFZM5hWyZ24=	\N	rjacklin36@samsung.com	Royall Jacklin	0906 Alpine Park	f
pbkdf2_sha256$100000$Tkzshl1X7pcI$ezalnvx6xZcfyPlqwPwwAuda0VyIrf1s1IVrXTQNV+s=	\N	wwillcock38@mozilla.com	Wanda Willcock	42 Butterfield Trail	f
pbkdf2_sha256$100000$quoxUeCJ4OtR$uZUQMt3oT/3NCWr/xCY4Vc/iNlYnFM+/L6Jk+k/Wc8w=	\N	mcartner39@mlb.com	Meagan Cartner	5401 Darwin Street	f
pbkdf2_sha256$100000$8F2sF8fkSlMC$1UYJWzFVJ3FhM2nH5BBL11JEBGvAuuBi/VSP1W3fm6E=	\N	wtrundell3b@gizmodo.com	Wiley Trundell	05370 Anhalt Road	f
pbkdf2_sha256$100000$8JdBmqbmF0Cs$fijEjqK/IyOJKEZSt/t3fWn4rJUeuR5ZM5Dzfmw4EMU=	\N	godevey3d@over-blog.com	Giacomo O'Devey	85102 Arrowood Place	f
pbkdf2_sha256$100000$mf4k5mNJYorq$3I0/UHP6fgv5bJBpP7wUAxZ4359Pj+EBG9zXmfC0RSk=	\N	dbroek3f@last.fm	Delinda Broek	95 Hintze Court	f
pbkdf2_sha256$100000$QE8UcttfnqdS$j8Dou3ALLkV8QoMqaLqYOLrm1lwOZ8lH8MKRbzh1AN4=	\N	rgarvie3g@geocities.com	Rog Garvie	446 Pleasure Pass	f
pbkdf2_sha256$100000$7QUN5HfI4jlt$nK2qPGzBqkaZJiEOtzOXeeLDVpsdNsk40JC2dWDbLRs=	\N	wbonson3i@fc2.com	Wain Bonson	986 Clyde Gallagher Street	f
pbkdf2_sha256$100000$VlEggHzB6bCn$idirv2njlQYKv6mzrdbAEA5ZHhSkpsVd5xriopc9xhc=	\N	acarwithim3k@wikipedia.org	Aleece Carwithim	138 Forster Crossing	f
pbkdf2_sha256$100000$iCZlUYmgr8ua$dYgsVVxN0HxtOt9HHUbnmxbvJ3361IS44lJLzfS5yUk=	\N	lmacgowan3m@networksolutions.com	Laurene MacGowan	27 Pawling Place	f
pbkdf2_sha256$100000$P5RevPyZN8JR$zztJV0lerbn1CspPLPmtmwrb2bM//+qcY9hVM8jw8H8=	\N	lbadwick3o@mtv.com	Lyman Badwick	040 Jackson Terrace	f
pbkdf2_sha256$100000$vWwJJYrDEIPN$a11Yc3Wrf1lxxLmsQKvNdsbWBOdLjmcW7/47RjzsPY0=	\N	fcowdray3p@hao123.com	Frederich Cowdray	0591 Tomscot Circle	f
pbkdf2_sha256$100000$hgyVBDzrikB8$eUhONUpQYxJxWp7x6aeAm8jCC0yMSgNBoAszhuVEZZw=	\N	mstannion3r@timesonline.co.uk	Michal Stannion	30 Duke Avenue	f
pbkdf2_sha256$100000$Vu4M14HK8kWk$mJGH4uXJgDqfkQSgDsGeib0Excj11Sz18FtB5e6ETQk=	\N	lantrag3t@youtube.com	Lenard Antrag	40 Village Parkway	f
pbkdf2_sha256$100000$WJMOUPHgZnny$q3W2MDTQ0OnUJcvjKksmbwRg+moFotdJ+h7oG8WnaRM=	\N	sspargo3v@blogs.com	Susanetta Spargo	3538 Toban Pass	f
pbkdf2_sha256$100000$1pRipYHlvuEm$sMuli5pETtP5bItoML7CaypCXVnSHjusjb54ypmvbTo=	\N	jmoakson3x@seesaa.net	Jolynn Moakson	0 Dexter Parkway	f
pbkdf2_sha256$100000$d6OOfY3f2W37$OzT8XvNt7whiwUgdMPac5HwqtPENopzSTuQ5z8XL960=	\N	khatliffe3y@blogspot.com	Kriste Hatliffe	4746 Buell Drive	f
pbkdf2_sha256$100000$7g6mQoRVjm3D$AAv1arezuo85JsdwAUaO9hXhBSHpfiwsldogLThyp5o=	\N	cbrazil40@pcworld.com	Cynthy Brazil	474 Twin Pines Point	f
pbkdf2_sha256$100000$vNdjQJ7v6ovG$CWmrz8iy1StY6qsm/uhG0r7YDQMOf46NSPqljzpJCAA=	\N	ntanman42@unblog.fr	Nerte Tanman	97 Maywood Parkway	f
pbkdf2_sha256$100000$8iWZxv4ElSpl$C/2r/ibvGutIVHZdG5TwN7cVNj3N44uHrmtmlb8EpgE=	\N	aoldershaw43@adobe.com	Alisun Oldershaw	646 Erie Park	f
pbkdf2_sha256$100000$Pu7Fja4vLwYB$OiQly6MHXiltV8vzHCuoikmmegXbUi8ngn5WmskFCjo=	\N	bfrayne45@google.ru	Barn Frayne	376 Acker Alley	f
pbkdf2_sha256$100000$ebQ42LsVYIWL$iIzeIClfLb+1W65vMGygNK23KBuOBLai/EQfon2sdHE=	\N	kcannell47@about.com	Kimberli Cannell	20 Valley Edge Point	f
pbkdf2_sha256$100000$Uyb4TOSuq4xu$vWUMSKH1+CLkMFavgm5DkZY3EKK6kJBsdmn+6k0ogPw=	\N	pjagson49@deviantart.com	Pierce Jagson	104 Hallows Junction	f
pbkdf2_sha256$100000$yMn4NyLvniwx$tNWsDTQ21iLLQd+r8w6qPEit0+X9yIAefBdMmGoXp34=	\N	apineaux4a@friendfeed.com	Aigneis Pineaux	5 Sundown Parkway	f
pbkdf2_sha256$100000$Nq5YBYWSTOjc$EFKJOE04c1uQu1oTUlbRans9xs6A//sZ+65A5r3aGDw=	\N	sspellman4c@cdbaby.com	Sheri Spellman	8548 Barby Lane	f
pbkdf2_sha256$100000$pKdg5IN7ai8T$6L2Qb8DRepeFKqnArdiJvcyzy/jxhkliD4uWmtJ7GEA=	\N	erhymes4e@vimeo.com	Elane Rhymes	75326 Mifflin Drive	f
pbkdf2_sha256$100000$dsnujArWKJvs$/9zlljF3hXLMj9/RnYFgkX9+LDWz4GI4OPbRNt2069s=	\N	chanfrey4g@prlog.org	Christian Hanfrey	10941 John Wall Parkway	f
pbkdf2_sha256$100000$5EzvbyzwBHl0$d0ureGzCezRPbfoYvO/ZhjuKVpEKLomVCCbj7V+yB8c=	\N	tself4h@weibo.com	Thomas Self	7296 Ridge Oak Plaza	f
pbkdf2_sha256$100000$7Q37KHlefd6T$V+LGEF4LaH1V+d4FqBlsoAnBKEZhyyINfOloQDLbsAc=	\N	dsteffans4j@fastcompany.com	Davie Steffans	69602 Grim Hill	f
pbkdf2_sha256$100000$pXRHzF9qRZGG$9XFBZh38Aur3PtgRwXRnly1JGYcuU60muZID9LC/Jig=	\N	dcrome0@wikispaces.com	Dari Crome	73 Straubel Place	f
pbkdf2_sha256$100000$SUhzzZlE9KYB$JaqCn4HF/A7PQ2hlR2Sijbh3ZvKEKXSjwfSzZlQUFT0=	\N	mwyard3@dagondesign.com	Maritsa Wyard	5793 Northwestern Circle	f
pbkdf2_sha256$100000$9rcxYqIrO11K$STJF8td0yvgWHBxoe8IGWTCXyxRzuX/EncIllNvdKb4=	\N	vcaroli6@mapy.cz	Vivianne Caroli	4 Trailsway Parkway	f
pbkdf2_sha256$100000$L1v1DmuIx5U3$DJa6IZp5wsApfIUWbY0w/xq6/RyQmGSPKwl9FxZtCII=	\N	jmcroberts8@opera.com	Jody McRoberts	4602 Acker Parkway	f
pbkdf2_sha256$100000$P7n1GODzvJcu$hizZS4oJFbmVnQ9PjA8dgxXB11n+VHbiTBay6UwlYxI=	\N	aaddionisiob@princeton.edu	Ara Addionisio	66861 Raven Avenue	f
pbkdf2_sha256$100000$3H63id0mzriq$qNvy1G3p437QTX97QuP14+57Dh9TbZ5fkmeipZQ9ggU=	\N	mstickingsd@simplemachines.org	Munroe Stickings	3 Dennis Lane	f
pbkdf2_sha256$100000$nyw5sC4QENS5$6mzscaIyVgvNgriwo27tfhIm5TE/r/JPPD1DLwxDPJw=	\N	zlosebief@disqus.com	Zacherie Losebie	6594 Del Sol Drive	f
pbkdf2_sha256$100000$Xc8wt4P5jwtt$OufgIrNTbpRe+6dduGhdUwxeDoISi3TYzOmkZ2StL88=	\N	cmillsomi@so-net.ne.jp	Cornie Millsom	57 Shopko Pass	f
pbkdf2_sha256$100000$53h0HrnONNar$EN7Ca4QOGZ63VV4cAm+0eAHbPVV6cCipf/bC3oKYNtk=	\N	striggk@i2i.jp	Salli Trigg	797 Weeping Birch Crossing	f
pbkdf2_sha256$100000$MknbhVYGD3w5$SaKBCPIryiJ5sLic5RfmmOXZStdlHK7mJXLjpyeNCAI=	\N	bellardm@harvard.edu	Barry Ellard	7617 Loftsgordon Point	f
pbkdf2_sha256$100000$sDmpquDQeLiA$Ns+Bxn5XBQVtnb74WTVht9XKyw2/HxqZkEaD1rU03OE=	\N	mbanburyp@friendfeed.com	Marylynne Banbury	1 Village Green Crossing	f
pbkdf2_sha256$100000$mhmMiqOA6Iie$3A9bkU3vddAv+ywywxiHaMMJq5ST1qNfiZsIR+LxCPU=	\N	jcalabryr@cmu.edu	Justinian Calabry	9690 Crowley Park	f
pbkdf2_sha256$100000$ytKn5PbpJxag$7j6yQKsjJBOxK5MUO/kogLyyBjkiyjfPZy/KcdCvc4Y=	\N	kricciardiellot@google.com.au	Kayle Ricciardiello	2 Hanson Hill	f
pbkdf2_sha256$100000$mgQTVM3NdHoV$1jF2C+0eRud4C4lZ+OPM40s2rj93nNXEgSyDtLt7gjA=	\N	smaierw@ifeng.com	Sarene Maier	7 Hauk Street	f
pbkdf2_sha256$100000$QpXXZ6D8U6hN$wNWd8eSL6xtiUMJ7DkCvrP7JVSWCa2SAoHsY9V/I294=	\N	sdarleyy@admin.ch	Sybil Darley	2966 Oneill Street	f
pbkdf2_sha256$100000$YduDN6o1DfU5$ev4P1JAt2liDk9F2AbqiT1M4VkR5OpPZ2+JMvqPAwYU=	\N	mwithnall10@youtube.com	Michail Withnall	08867 Hallows Street	f
pbkdf2_sha256$100000$mcnAeFW7iwnf$ly7kKlxQuinhmX/FX4GQFP4SVqbdZ1e9JWuioN+0KEw=	\N	mbernhardt12@tripadvisor.com	Minor Bernhardt	78 Reinke Park	f
pbkdf2_sha256$100000$zAmKCH0vnbdG$L8k99SQ1LcJCDXD36OekTBKNSw8LMcz86U2Tcor/tOk=	\N	jplevin15@squidoo.com	Josie Plevin	41210 Oxford Center	f
pbkdf2_sha256$100000$Li3aDSXy3R7M$eNBr1JBuqotHF5xCvTVOCXyDEtA/9TnDw7/2fsu8FL0=	\N	blaming17@devhub.com	Bab Laming	2 Barby Plaza	f
pbkdf2_sha256$100000$6FCjpuKeimYv$a3MPokEhKR52Zo4cK5Bo71AzG/4FhCq25rXZsCpX808=	\N	gbonson19@marketwatch.com	Gill Bonson	863 Hintze Alley	f
pbkdf2_sha256$100000$N2832KxpcNWs$cGk9n/hX6uXX+0cQv+uheMdX81ajtuNyP3NCcrkTkFw=	\N	dcardon1c@unicef.org	Dinny Cardon	1 Cordelia Point	f
pbkdf2_sha256$100000$p8HUqL5mfHBv$JefIUxAoGh/y6gNihSQnJNibcBJyoDXQSuawjjxPkzU=	\N	fbartrop1e@sciencedaily.com	Flossie Bartrop	6 Tennessee Street	f
pbkdf2_sha256$100000$HHAWE4ziOSeV$nCfSm7KoafqsP8qGTZ5LuBwgKD3xgvFpM+U6nST+NWM=	\N	akezar1g@cdbaby.com	Ariella Kezar	3 Manufacturers Court	f
pbkdf2_sha256$100000$aobClqmSU379$ptMdbkDOQxRRWuZNAouO2ubCmlFEzVfiKo0/7Uy72Y0=	\N	pstubbley1i@flavors.me	Philomena Stubbley	41722 Green Ridge Park	f
pbkdf2_sha256$100000$zJNNp5YClpkJ$5FV2G4N9TLquWN7AwwNZa5w2aPje4US4r6RlENi6020=	\N	pgebb1l@yahoo.com	Portie Gebb	94 Granby Street	f
pbkdf2_sha256$100000$JzQYPBUb8QOt$UADVTiyE66JU7H1o6PxohXF3Z1uAgI9pZmowtEaL3oY=	\N	bfeldhorn1n@gnu.org	Betsy Feldhorn	370 Bartillon Center	f
pbkdf2_sha256$100000$JTU2s7b3WwN6$tISYMph78RD0Cfcak8UUX5AtynI26EpJFh5udaLOMdE=	\N	kcurran4l@dagondesign.com	Karalee Curran	78 Iowa Terrace	f
pbkdf2_sha256$100000$CIGBjxJdLI7k$jOXiXU4yHbbmNsipJQuJN5+0XQr/NhCt2nFwEILHAkc=	\N	dpancoust4m@php.net	Darda Pancoust	174 Sugar Parkway	f
pbkdf2_sha256$100000$DEGFu6fJYt2t$oOArOfMwNor3wIvAfEMGB43Ik25iFI2QgByS0T4la/Q=	\N	itrevett4o@china.com.cn	Imelda Trevett	4 Talmadge Court	f
pbkdf2_sha256$100000$adLE5jQFqCuW$9H0jxkDaZb41Uk2C+hYtzrqJDaTHHvLT7183fI/zeYA=	\N	asharma4q@free.fr	Alidia Sharma	3 Arkansas Park	f
pbkdf2_sha256$100000$q0J23ato3pgG$Mhhe9ryKJW6Io9slR+5tNE8L0acWCUa+Ik33U3n75q4=	\N	mhartshorn4s@salon.com	Maud Hartshorn	030 Ridge Oak Way	f
pbkdf2_sha256$100000$8JBxnlxQSxaD$hWmqx68GZL9XJdDyMq2BXt+I39SCZNJUhHLiXUghmSI=	\N	cpierse4u@twitpic.com	Cris Pierse	957 Prentice Plaza	f
pbkdf2_sha256$100000$L2h4Uz0SZBmh$jN1ofDVW7B7fbOVVQeqYIScTEL9qaXkvcP+lcMZZdSU=	\N	ljoskovitch4v@economist.com	Lila Joskovitch	74850 Meadow Ridge Hill	f
pbkdf2_sha256$100000$wYyjq4Kg2HgS$TWEfHiGpE7J1pwKlJVkhwmg8w79djSrCOBAzwS40v/0=	\N	cgrinham4x@archive.org	Cosetta Grinham	171 Thompson Parkway	f
pbkdf2_sha256$100000$NfXKQM39sJkV$Ema6zZzJnv8YGYXUNh6Dfv93veBNrdahim5HVtm3RuE=	\N	amarques4z@liveinternet.ru	Astrid Marques	10594 Lillian Park	f
pbkdf2_sha256$100000$KFacp3NLSGgi$mibwqbjJ+GypRKAef5oBNLT0DeH1VR3EB5j2KVcMW68=	\N	wferrai51@wiley.com	Wilhelmine Ferrai	419 Transport Court	f
pbkdf2_sha256$100000$xdZjqRTXboyV$YcaTn1JEH+hFeJSlqgNY88dH4dGruLM1I41LTV1/p8M=	\N	mericssen52@hhs.gov	Matt Ericssen	6 6th Way	f
pbkdf2_sha256$100000$Z0RIqNUdYA3O$IBd9VtVf39OJhxyWve/8u3yy6k54Cg5CfZR6uisazR8=	\N	gkinforth54@taobao.com	Gail Kinforth	92550 Maywood Street	f
pbkdf2_sha256$100000$d4PlLNwY01dU$X2SHJQVSnfUhMF0Eon9iKkQkP/5stH4N/xQ6oc7qwtE=	\N	lstrange56@indiatimes.com	Lucia Strange	96 Melvin Circle	f
pbkdf2_sha256$100000$QKDzkdnuMYXZ$C816Pu+XzWcvdk55dB4k9fEnSxdAnh+fhGGz9W4xNK0=	\N	agrigoroni58@g.co	Arni Grigoroni	897 Little Fleur Avenue	f
pbkdf2_sha256$100000$NtpxsL4Xq6Eu$Tl8hFvUyvmFfiCCn4gno/GzolzIR2j8IRNYLOnKKqz8=	\N	dsoltan5a@ameblo.jp	Dalia Soltan	7497 Roth Trail	f
pbkdf2_sha256$100000$zmAT2Cpa8mMj$3zySWtyyYe14TAWfhfFwCod96iVshcCoJ1hiH5ORmpQ=	\N	hkimberley5c@meetup.com	Hermione Kimberley	2 Kingsford Terrace	f
pbkdf2_sha256$100000$P5qIx5oQhjor$S2IchrAB3Mf3e3vYSUVSZz0vJJycTMpoRYKUvr6m4WU=	\N	khardwick5d@eventbrite.com	Kiersten Hardwick	0 Shopko Parkway	f
pbkdf2_sha256$100000$RF0aHWDIy1Bw$I2+DKvbjpTlIh0zqtw5aeeIzwi4oAcM9YqIlSKqgdsU=	\N	psammars5f@examiner.com	Pieter Sammars	57 Haas Junction	f
pbkdf2_sha256$100000$48KRi4SePHqw$AlvdJRvzainovUElFFO95bzRttECETzQBqEhunrhIro=	\N	fmcdonnell5h@tripod.com	Fallon McDonnell	600 Rutledge Place	f
pbkdf2_sha256$100000$zd949mgGfchf$LJoYZaJR+aV60JK6xjk4YnRrAzc2xxdJlZAEG/wkeCs=	\N	slithcow5j@skyrock.com	Spence Lithcow	4535 Logan Place	f
pbkdf2_sha256$100000$DhizzFtRnefY$OQ6s5uFLc0E4sruE57Q2v5OyTocGYdaPWOFx9Beh8eQ=	2018-05-19 21:13:07.39674+07	cpaur1@ihg.com	Cos Paur	34 Namekagon Circle	f
pbkdf2_sha256$100000$2ILXaZalxJzB$71Yn30bLLsys5K21adxCcCq1G0lwx78DbK+EUKBZgs4=	\N	mgehring1q@spiegel.de	Mohandas Gehring	19429 Onsgard Avenue	f
pbkdf2_sha256$100000$FAcTqNg1uyeN$o4U2LuwdZX+sK4jtmf99Oh+otyWYjloTmcpb8Eg40rM=	\N	kmcwhirter1s@ibm.com	Kerrill McWhirter	86992 Lawn Plaza	f
pbkdf2_sha256$100000$59GQWSJAotYk$KwjWQKCWiIkxTXGp2fGwwR/8POIi5MBSGc8hDZP9r88=	\N	jgot1u@hao123.com	Jorie Got	044 Elka Junction	f
pbkdf2_sha256$100000$OCWkFoMxVYFM$bhpA7XHCMI4qkl+Nii8atYjhMt7rU4DtB/BAl7vAfBI=	\N	bgreber1w@moonfruit.com	Bryan Greber	271 Bobwhite Center	f
pbkdf2_sha256$100000$fjIJNZLZAAjc$w69Rsa6b6ixRaF49xre6k3z3hRK2wdV+ByvZn3yha3s=	\N	gbortolussi1z@yahoo.com	Georgina Bortolussi	453 Cascade Street	f
pbkdf2_sha256$100000$1fIDuOTLEvy3$NaoQJYF4pzDUCs8H+BUDiVL+EA8CkznxOQOPEzewAl0=	\N	tpinyon21@amazonaws.com	Thomas Pinyon	131 Di Loreto Way	f
pbkdf2_sha256$100000$nS0eueCcyCLb$g6l03ZhEnsBe0nO2nVupI21ARfMiMCO/lcTB7l78U2k=	\N	gstebbings23@ow.ly	Gray Stebbings	675 Scott Point	f
pbkdf2_sha256$100000$MJHt9TQjHdPC$avEDV2DmQgHb7meFq+5tcOdVfYO0E80hzRhQ8IBGDK4=	\N	rbarents26@naver.com	Rudie Barents	5615 Michigan Center	f
pbkdf2_sha256$100000$qPQnClyiTY9R$IaKN0KPMwCkp6ksM2IJWe4pBpldZvMPDto7ObpKl2ZA=	\N	edilgarno28@canalblog.com	Emile Dilgarno	837 4th Circle	f
pbkdf2_sha256$100000$GqO1SeL8J315$hcJ4GYx+BK2Xp+J+z6nYcgJZJ0ilzfZuHcgHa4lKdw4=	\N	afortoun2a@imdb.com	Alonso Fortoun	53741 Westerfield Avenue	f
pbkdf2_sha256$100000$QAk9pMAFDAZq$yGG1E/6cKMfh2pyKY3RAre9Y0VbuTwFQIelN4wc9ftk=	\N	cbradnick2b@addtoany.com	Cristina Bradnick	24 Ramsey Road	f
pbkdf2_sha256$100000$guoDxtpefoSF$tRS/HznIJAbctE4O2GwwZmHRTAr+MMTAu2qXV4rh+8A=	\N	cshevlin2e@comcast.net	Cassandra Shevlin	89235 Hovde Crossing	f
pbkdf2_sha256$100000$6NalenahIFx0$dWDTnaqoeNeWVar6m2G8rttKt+qtvF9J8vtW+MTzQzc=	\N	vvillaret2g@utexas.edu	Vaclav Villaret	9 Lakeland Alley	f
pbkdf2_sha256$100000$kDE338tqCgRU$kcLhyxu8Acl4WSR27V92u7y08tKA+lEGZ9bdETpO1dA=	\N	bbiggerdike2i@unesco.org	Beale Biggerdike	473 Barnett Hill	f
pbkdf2_sha256$100000$epVOKWjofXMF$0xbmxBRJA4c3mt2/BWVWBym+crKnOmtxgnGcmYroTLA=	\N	njameson2l@yandex.ru	Nerissa Jameson	653 Holmberg Parkway	f
pbkdf2_sha256$100000$ZYE1jPfQFwQf$viVtEVUPfOoFlMvY7gsR+VSqekltFgq6lAUKg4ZyEEs=	\N	tlummis2n@springer.com	Tony Lummis	224 Di Loreto Lane	f
pbkdf2_sha256$100000$mVbjNJ0vjMMs$RC5ZPGHSbhocYNR5P7JzaHxNyar9CF4XxiZdANX4Ui8=	\N	lbattill2p@apache.org	Lennie Battill	635 Mcguire Park	f
pbkdf2_sha256$100000$SvRCU5YtkN23$qpIF9A/gddvwd3DImr//OCdEs8MsIpKNz06QkayUUCA=	\N	jbutt2s@addthis.com	Joy Butt Gow	6 Everett Hill	f
pbkdf2_sha256$100000$ScUBKzfelIDK$NXs9leFARcm8jyKesucBJqRujwOfHzt+4h6ydG7TyNY=	\N	gjemison2u@mashable.com	Geri Jemison	3124 Fair Oaks Court	f
pbkdf2_sha256$100000$4SR6UF9jZvhN$HAyXKGdmu1k0u6qJSIGrcDPvbaD8PSz400sDQlvNK6U=	\N	cbilston2w@cmu.edu	Clyde Bilston	194 Clemons Way	f
pbkdf2_sha256$100000$BZIfdcT2wx4J$curMZ8sVn26VYFJplmXqDKknyd/Lr+M01LZ+AoheMeg=	\N	baskam2y@si.edu	Bryna Askam	74473 5th Point	f
pbkdf2_sha256$100000$8bn5aywnuHBK$ZJpqihtLuRN5fUczgtobEwajkKACQNkr1FqdrFliehY=	\N	bdureden30@netscape.com	Bambi Dureden	39 Esker Street	f
pbkdf2_sha256$100000$ZmE8mzAgvREz$nPd2WAy3C9cYdNUcZ/AyhQX+w0MbYNIkY8fBI+D+fi8=	\N	ntwelvetree32@seattletimes.com	Noell Twelvetree	33 Warner Road	f
pbkdf2_sha256$100000$GSwDz3U22p6O$SD65j0vuTWGcH01/Zmx2hUtj5TDisPoVnQunGxELNmA=	\N	edomel35@icio.us	Em Domel	09846 Buell Trail	f
pbkdf2_sha256$100000$WF72KfQNld0W$UBhxxx8x7N/kfBLUZBzIUCE9SyzZj6kXcFtdpmbODiM=	\N	mcardenosa37@histats.com	Myrta Cardenosa	09287 Swallow Crossing	f
pbkdf2_sha256$100000$x58cUqL4eWwE$zgcM9KZIwcyq4FvP+ieo+k2rJmm0wyTB3r37bsTDPoc=	\N	mdrackford3a@sbwire.com	Marita Drackford	3 Knutson Trail	f
pbkdf2_sha256$100000$R20z3VWdzxPM$uuW3MqMdwLtC334Tcdi9zXALT2uVlliaSK2Yjl3z+Mc=	\N	acurrington3c@answers.com	Ana Currington	1858 Portage Street	f
pbkdf2_sha256$100000$pP8LHLDlb1wO$1jpgg7Fh5ptl33LnW/6YFLt4T3/dtkjGWXKut1o466c=	\N	pleamon3e@google.ru	Priscilla Leamon	0632 Algoma Street	f
pbkdf2_sha256$100000$TIJw76s57BrW$pr1sD6Afm9lVUj1BDdjbiDVJyynW70tqdYIoIIIkpec=	\N	ddelion3h@prweb.com	Dilly Delion	0 Pearson Parkway	f
pbkdf2_sha256$100000$dxVbwCbmg4gZ$RiSJDkrW89oGpgRVJS8il3bWAEaIlPgpChhDF3kPDi0=	\N	iglisenan3j@ifeng.com	Inga Glisenan	893 Eliot Hill	f
pbkdf2_sha256$100000$qRBN6xMy8TJT$sqqJG+9imzuzaxjYFYHyn0b4EAPxProic8HeEKcSPs0=	\N	jstopp3l@blogspot.com	Joannes Stopp	309 Steensland Avenue	f
pbkdf2_sha256$100000$rYC7BK2Z9TOB$3XCHZlyh7QaUqRc2Yn34RIW7zEW0JRmjm2gHjaJn7Ko=	\N	bcrossthwaite3n@mediafire.com	Berkie Crossthwaite	55 Sommers Trail	f
pbkdf2_sha256$100000$GHW7C3CbVCGp$Ob3tIEYzqG89wQ6xYDC0tyqn1LIn7ya8GOodixT+j8M=	\N	scominoli3q@webs.com	Sari Cominoli	2147 Tennyson Street	f
pbkdf2_sha256$100000$lGjMvI98X58C$r/Ad+L43qWMXuPQGEWjAv5g56A4lHpVuhxqVvH/UMag=	\N	ealan3s@wordpress.org	Etti Alan	96 Gerald Circle	f
pbkdf2_sha256$100000$Ppz0vRBqgTJV$N55CgC1cAd2mz6iXpLnLRB1s+6arVS5GDDc01ITeMCY=	\N	ehardaway3u@1und1.de	Elijah Hardaway	9451 Arrowood Drive	f
pbkdf2_sha256$100000$FTHtuWHFDkzg$iVegAeWt8Tf5r8hgN7L4gKIKxel3rcER6jQLPTjGmzs=	\N	ftrumpeter3w@pagesperso-orange.fr	Fernande Trumpeter	21359 Cardinal Pass	f
pbkdf2_sha256$100000$hegYQMzJV9D6$YZrsKsMF1oO4/r3O+Xw1G6apYaSI9fNcGwjPwPULRec=	\N	jspat3z@rambler.ru	Jennica Spat	82655 Northwestern Road	f
pbkdf2_sha256$100000$7X5OGbXK2ejQ$rbOhFUjSKYfL/j1NtQyEWlM6XQotloT52XkETwsAJEE=	\N	aharmston41@bravesites.com	Antoinette Harmston	18613 Forest Pass	f
pbkdf2_sha256$100000$LaQqb3eM2FLP$b6+cQUcf9fHGy1D4DZ+Ih/3HIKd5JqxLzrOhVeCxQVA=	\N	gdouse44@ca.gov	Gerardo Douse	81524 Vidon Street	f
pbkdf2_sha256$100000$zr1oICNJ0sD7$njnSETdCzOHg1V/VaSnUGD8hIrpUqq0r0ZXCVexkJFI=	\N	fblant46@zdnet.com	Fons Blant	16 Pleasure Avenue	f
pbkdf2_sha256$100000$YvN9DAtOTgNJ$PmR4dHpLRKIIQRqY7TvLHzHPWckwab/AEXK6QBlWUho=	\N	jbanyard48@desdev.cn	Jordana Banyard	48248 Meadow Ridge Alley	f
pbkdf2_sha256$100000$6HFqxPuuD4gO$zOOkjsTtlnooL5u5sz6iE4CodhgoJuXB/X3gLMNMRf0=	\N	ttsarovic4b@nba.com	Tarah Tsarovic	5 Bunker Hill Trail	f
pbkdf2_sha256$100000$lN0L9hu8kBtr$nrXcknn0yBrH8zGSu+NI5bHGqKo3TS1CsI2NgcRPWjg=	\N	pcolerick4d@hud.gov	Padgett Colerick	97 Mallory Circle	f
pbkdf2_sha256$100000$v7Vzgxf8OSbU$MDsBc7hcZ3Nv3YqeS0Z2IkBxDPb6+5B3b/7z/gVvuSY=	\N	gjanuszewski4f@purevolume.com	Griselda Januszewski	46394 Center Court	f
pbkdf2_sha256$100000$PvHzXiE7uJ49$+rd8CJGxfKeoxQxKZ9vcbjER3kbwX1Mg9y2wQJKoEak=	\N	rdeverock4i@gmpg.org	Roddie Deverock	47 Michigan Pass	f
pbkdf2_sha256$100000$9DeL14Oo3UBf$uowl7G9wLss41Oy5QC020N1wD1NL03PV6ukV4xNeKCk=	\N	gbaggot4k@mail.ru	Gretchen Baggot	60827 Towne Drive	f
pbkdf2_sha256$100000$cTx3U92XlOlu$nMakv+dN8nFbEcMRyxQ+mjVzzHoFp9oQdgqT2jHYix8=	\N	sroostan4n@cdbaby.com	Spence Roostan	5537 Pond Pass	f
pbkdf2_sha256$100000$cCYb9iMwWTQZ$/RVFL1QEUHxSgyB2goW5mvtF44k23JdyZ2zxOc797zk=	\N	gcrellin4p@hugedomains.com	Gilberte Crellin	75 Michigan Way	f
pbkdf2_sha256$100000$f0zIs5w46daR$LHBSwh2HtLnhvEsJCsWvkVnQBCJEYsi2QWHEvNlxebs=	\N	ierricker4r@cnbc.com	Isaac Erricker	4841 Dorton Center	f
pbkdf2_sha256$100000$4EXvsQBJ0cZB$RuJYz6opJD2GiTgQVqB15asw3mkO4cLlMN5zOwxZt6M=	\N	fheiner4t@reddit.com	Fraze Heiner	50731 Pankratz Court	f
pbkdf2_sha256$100000$UDxmDanCLkNV$jMOXc4Cc1H13XKcHs3rct+TvCDC9Z9MDBwK8ISfV+5U=	\N	mchater4w@jigsy.com	Minni Chater	50121 Becker Trail	f
pbkdf2_sha256$100000$3NxzDGBEJF3f$4F5LUa1j+jFSAHqQ4PeycxoPI8lkCFF0GTeElzgDuqk=	\N	apaydon4y@hao123.com	Avrit Paydon	2 Declaration Hill	f
pbkdf2_sha256$100000$KYjRqUJe6l31$lVHGFXdZxKuw+0wsFSH8DoaJUQXre0Ee6I5il1uOF9c=	\N	hjolley50@google.fr	Hailee Jolley	26 Northridge Trail	f
pbkdf2_sha256$100000$7lbmgMiYv5ra$HKtQF3QQjmd8VNEyWidMRDM5PDlWkkqew7L0BKLkPwI=	\N	ttuck53@acquirethisname.com	Thalia Tuck	66159 Gateway Alley	f
pbkdf2_sha256$100000$9cseqWXRy6AP$W1SPrJc2auI7AcCB/aiYp837OCPqzTus/DRdkcmdKSc=	\N	bhakey55@reuters.com	Berta Hakey	23455 Dapin Alley	f
pbkdf2_sha256$100000$eybPlduoluoq$3k56j3CSQVbzWJJqU7FLG3gfo+wFfnxXMcuAJAw7qso=	\N	ehampe57@live.com	Emlyn Hampe	21 Jenna Pass	f
pbkdf2_sha256$100000$Km81r2MoptdD$2jvdWsre8wAxn0YkWfcEjb958+dIzm/cyWL1sg1EDJo=	\N	khedditch59@wisc.edu	Kylie Hedditch	986 Jenifer Drive	f
pbkdf2_sha256$100000$gYxL3vv76V4Z$Upu6GmKb5h09i1CReRDO+zHonKgpt8KLXx1Zesfz9I8=	\N	abehning5b@g.co	Aimee Behning	0 Lakewood Gardens Street	f
pbkdf2_sha256$100000$rHtG7YwHHAIJ$IsdqDHCKq1jJbIrtcOjrO8kis5slJtEc1tV3t7rCvNI=	\N	apaulot5e@fastcompany.com	Alaster Paulot	1 Pond Point	f
pbkdf2_sha256$100000$DrTKDsKJ6o0e$eZzbKkA9oav/Y9NrjN14bPqRyNQ78mhbfwVZGcaxXqY=	\N	cvasenkov5g@apache.org	Cecelia Vasenkov	7 Westend Junction	f
pbkdf2_sha256$100000$CNM9eMq35y9U$M2ELkbaI9QNUZsOEGfgVDjlNnkptPWrEaDUUtG5wR6I=	\N	efilby5i@examiner.com	Etan Filby	598 Hollow Ridge Alley	f
pbkdf2_sha256$100000$SpkNRdIIb28c$GhyFpFedxjFp8S6nfBpobdvAnEFfASbFxYbuX5+io6k=	2018-05-21 00:13:33.974899+07	donatur1@donatur.id	Donatur 1	Alamat, Kecamatan, Kabupaten, Provinsi, Kodepos	f
pbkdf2_sha256$100000$LoxZBchRG2SJ$ubs1MHLeCo+Lcs9WzyrglEV3p4rsDPF2m2B1J4bm2jY=	2018-05-21 01:01:07.47046+07	donatur2@donatur.id	Donatur 2	alamat, kecamatan, kabupaten, provinsi, kodepos	f
pbkdf2_sha256$100000$nqS54TjMgdmQ$1QCYrY0U5p4TBn5x1bMop6tcNKc8GwLWt+bqrBaVass=	2018-05-21 02:10:23.854941+07	sponsor1@sponsor.id	Sponsor 1	Jalan Margonda Raya No. 1, Depok, Depok, Jawa Barat, 16425	f
pbkdf2_sha256$100000$ZT3bn8BR4ckr$FJoFusYuIYbxxFR/AhTFCUcuxkrpkNdHe18wHp115WQ=	2018-05-21 02:13:25.647115+07	relawan1@relawan.id	Relawan 1	alamat, kecamatan, kabupaten, provinsi, 123456	f
pbkdf2_sha256$100000$hjYN0wMAUuXd$BN2VR/iDY/Jf524NUmVjjDJla2zmM0AcijPdfL+FZAI=	2018-05-21 14:42:46.503596+07	coba@coba.com	pengurus	alamat	f
pbkdf2_sha256$100000$mZlgh3txB4Xh$bDgzOe2NVxy0YzC67xZcnSRVmM4932AH5thHA3wf3LM=	\N	daninsoedjono@danin.tech	Danin Soedjono	alamat	f
pbkdf2_sha256$100000$lClvmC7SoW20$r2sxR+SfETkxMMBGjk1Cl7z2zvCjGhi1LH1pw8cxBa0=	\N	pengurus1@organisasi6.com	pengurus 1	alamat	f
pbkdf2_sha256$100000$rtehvBr7XOO6$2r1r9S03xNc8oQxiNYoEGwW0KcWWL0+S7R2vZ7LFsJQ=	\N	pengurus2@organisasi6.com	pengurus	alamat	f
\.


--
-- Data for Name: user_sion_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_sion_groups (id, usersion_id, group_id) FROM stdin;
\.


--
-- Data for Name: user_sion_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_sion_user_permissions (id, usersion_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 75, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 25, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 16, true);


--
-- Name: user_sion_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_sion_groups_id_seq', 1, false);


--
-- Name: user_sion_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_sion_user_permissions_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: berita berita_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.berita
    ADD CONSTRAINT berita_pkey PRIMARY KEY (kode_unik);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: donatur_kegiatan donatur_kegiatan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_kegiatan
    ADD CONSTRAINT donatur_kegiatan_pkey PRIMARY KEY (donatur, kegiatan);


--
-- Name: donatur_organisasi donatur_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_organisasi
    ADD CONSTRAINT donatur_organisasi_pkey PRIMARY KEY (donatur, organisasi);


--
-- Name: donatur donatur_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur
    ADD CONSTRAINT donatur_pkey PRIMARY KEY (email);


--
-- Name: kategori_kegiatan kategori_kegiatan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kategori_kegiatan
    ADD CONSTRAINT kategori_kegiatan_pkey PRIMARY KEY (kode_kategori, kode_kegiatan);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (kode);


--
-- Name: keahlian_relawan keahlian_relawan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.keahlian_relawan
    ADD CONSTRAINT keahlian_relawan_pkey PRIMARY KEY (email, keahlian);


--
-- Name: kegiatan kegiatan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kegiatan
    ADD CONSTRAINT kegiatan_pkey PRIMARY KEY (kode_unik);


--
-- Name: laporan_keuangan laporan_keuangan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.laporan_keuangan
    ADD CONSTRAINT laporan_keuangan_pkey PRIMARY KEY (organisasi, tgl_dibuat);


--
-- Name: organisasi organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.organisasi
    ADD CONSTRAINT organisasi_pkey PRIMARY KEY (email_organisasi);


--
-- Name: organisasi_terverifikasi organisasi_terverifikasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.organisasi_terverifikasi
    ADD CONSTRAINT organisasi_terverifikasi_pkey PRIMARY KEY (email_organisasi);


--
-- Name: pengurus_organisasi pengurus_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.pengurus_organisasi
    ADD CONSTRAINT pengurus_organisasi_pkey PRIMARY KEY (email);


--
-- Name: penilaian_performa penilaian_performa_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.penilaian_performa
    ADD CONSTRAINT penilaian_performa_pkey PRIMARY KEY (email_relawan, organisasi, id_number);


--
-- Name: relawan_organisasi relawan_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.relawan_organisasi
    ADD CONSTRAINT relawan_organisasi_pkey PRIMARY KEY (email_relawan, organisasi);


--
-- Name: relawan relawan_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.relawan
    ADD CONSTRAINT relawan_pkey PRIMARY KEY (email);


--
-- Name: reward reward_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.reward
    ADD CONSTRAINT reward_pkey PRIMARY KEY (barang_reward, kode_kegiatan);


--
-- Name: sponsor_organisasi sponsor_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.sponsor_organisasi
    ADD CONSTRAINT sponsor_organisasi_pkey PRIMARY KEY (sponsor, organisasi);


--
-- Name: sponsor sponsor_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.sponsor
    ADD CONSTRAINT sponsor_pkey PRIMARY KEY (email);


--
-- Name: tujuan_organisasi tujuan_organisasi_pkey; Type: CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.tujuan_organisasi
    ADD CONSTRAINT tujuan_organisasi_pkey PRIMARY KEY (organisasi, tujuan);


--
-- Name: user_sion_groups user_sion_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_groups
    ADD CONSTRAINT user_sion_groups_pkey PRIMARY KEY (id);


--
-- Name: user_sion_groups user_sion_groups_usersion_id_group_id_ffd05d72_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_groups
    ADD CONSTRAINT user_sion_groups_usersion_id_group_id_ffd05d72_uniq UNIQUE (usersion_id, group_id);


--
-- Name: user_sion user_sion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion
    ADD CONSTRAINT user_sion_pkey PRIMARY KEY (email);


--
-- Name: user_sion_user_permissions user_sion_user_permissio_usersion_id_permission_i_627a2ece_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_user_permissions
    ADD CONSTRAINT user_sion_user_permissio_usersion_id_permission_i_627a2ece_uniq UNIQUE (usersion_id, permission_id);


--
-- Name: user_sion_user_permissions user_sion_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_user_permissions
    ADD CONSTRAINT user_sion_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_admin_log_user_id_c564eba6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6_like ON public.django_admin_log USING btree (user_id varchar_pattern_ops);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: user_sion_email_0d9caef8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_email_0d9caef8_like ON public.user_sion USING btree (email varchar_pattern_ops);


--
-- Name: user_sion_groups_group_id_b1ca1261; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_groups_group_id_b1ca1261 ON public.user_sion_groups USING btree (group_id);


--
-- Name: user_sion_groups_usersion_id_972ba839; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_groups_usersion_id_972ba839 ON public.user_sion_groups USING btree (usersion_id);


--
-- Name: user_sion_groups_usersion_id_972ba839_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_groups_usersion_id_972ba839_like ON public.user_sion_groups USING btree (usersion_id varchar_pattern_ops);


--
-- Name: user_sion_user_permissions_permission_id_ee5c3b50; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_user_permissions_permission_id_ee5c3b50 ON public.user_sion_user_permissions USING btree (permission_id);


--
-- Name: user_sion_user_permissions_usersion_id_eb49aa80; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_user_permissions_usersion_id_eb49aa80 ON public.user_sion_user_permissions USING btree (usersion_id);


--
-- Name: user_sion_user_permissions_usersion_id_eb49aa80_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_sion_user_permissions_usersion_id_eb49aa80_like ON public.user_sion_user_permissions USING btree (usersion_id varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: berita berita_kegiatan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.berita
    ADD CONSTRAINT berita_kegiatan_fkey FOREIGN KEY (kegiatan) REFERENCES public.kegiatan(kode_unik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_user_sion_email; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_user_sion_email FOREIGN KEY (user_id) REFERENCES public.user_sion(email) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: donatur donatur_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur
    ADD CONSTRAINT donatur_email_fkey FOREIGN KEY (email) REFERENCES public.user_sion(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: donatur_kegiatan donatur_kegiatan_donatur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_kegiatan
    ADD CONSTRAINT donatur_kegiatan_donatur_fkey FOREIGN KEY (donatur) REFERENCES public.donatur(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: donatur_kegiatan donatur_kegiatan_kegiatan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_kegiatan
    ADD CONSTRAINT donatur_kegiatan_kegiatan_fkey FOREIGN KEY (kegiatan) REFERENCES public.kegiatan(kode_unik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: donatur_organisasi donatur_organisasi_donatur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_organisasi
    ADD CONSTRAINT donatur_organisasi_donatur_fkey FOREIGN KEY (donatur) REFERENCES public.donatur(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: donatur_organisasi donatur_organisasi_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.donatur_organisasi
    ADD CONSTRAINT donatur_organisasi_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi_terverifikasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_kegiatan kategori_kegiatan_kode_kategori_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kategori_kegiatan
    ADD CONSTRAINT kategori_kegiatan_kode_kategori_fkey FOREIGN KEY (kode_kategori) REFERENCES public.kategori(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_kegiatan kategori_kegiatan_kode_kegiatan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kategori_kegiatan
    ADD CONSTRAINT kategori_kegiatan_kode_kegiatan_fkey FOREIGN KEY (kode_kegiatan) REFERENCES public.kegiatan(kode_unik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: keahlian_relawan keahlian_relawan_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.keahlian_relawan
    ADD CONSTRAINT keahlian_relawan_email_fkey FOREIGN KEY (email) REFERENCES public.relawan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kegiatan kegiatan_organisasi_perancang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.kegiatan
    ADD CONSTRAINT kegiatan_organisasi_perancang_fkey FOREIGN KEY (organisasi_perancang) REFERENCES public.organisasi_terverifikasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan_keuangan laporan_keuangan_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.laporan_keuangan
    ADD CONSTRAINT laporan_keuangan_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi_terverifikasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: organisasi_terverifikasi organisasi_terverifikasi_email_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.organisasi_terverifikasi
    ADD CONSTRAINT organisasi_terverifikasi_email_organisasi_fkey FOREIGN KEY (email_organisasi) REFERENCES public.organisasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengurus_organisasi pengurus_organisasi_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.pengurus_organisasi
    ADD CONSTRAINT pengurus_organisasi_email_fkey FOREIGN KEY (email) REFERENCES public.user_sion(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengurus_organisasi pengurus_organisasi_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.pengurus_organisasi
    ADD CONSTRAINT pengurus_organisasi_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penilaian_performa penilaian_performa_email_relawan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.penilaian_performa
    ADD CONSTRAINT penilaian_performa_email_relawan_fkey FOREIGN KEY (email_relawan) REFERENCES public.relawan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penilaian_performa penilaian_performa_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.penilaian_performa
    ADD CONSTRAINT penilaian_performa_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi_terverifikasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relawan relawan_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.relawan
    ADD CONSTRAINT relawan_email_fkey FOREIGN KEY (email) REFERENCES public.user_sion(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relawan_organisasi relawan_organisasi_email_relawan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.relawan_organisasi
    ADD CONSTRAINT relawan_organisasi_email_relawan_fkey FOREIGN KEY (email_relawan) REFERENCES public.relawan(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relawan_organisasi relawan_organisasi_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.relawan_organisasi
    ADD CONSTRAINT relawan_organisasi_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reward reward_kode_kegiatan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.reward
    ADD CONSTRAINT reward_kode_kegiatan_fkey FOREIGN KEY (kode_kegiatan) REFERENCES public.kegiatan(kode_unik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sponsor sponsor_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.sponsor
    ADD CONSTRAINT sponsor_email_fkey FOREIGN KEY (email) REFERENCES public.user_sion(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sponsor_organisasi sponsor_organisasi_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.sponsor_organisasi
    ADD CONSTRAINT sponsor_organisasi_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi_terverifikasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sponsor_organisasi sponsor_organisasi_sponsor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.sponsor_organisasi
    ADD CONSTRAINT sponsor_organisasi_sponsor_fkey FOREIGN KEY (sponsor) REFERENCES public.sponsor(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tujuan_organisasi tujuan_organisasi_organisasi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adalberht
--

ALTER TABLE ONLY public.tujuan_organisasi
    ADD CONSTRAINT tujuan_organisasi_organisasi_fkey FOREIGN KEY (organisasi) REFERENCES public.organisasi(email_organisasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_sion_groups user_sion_groups_group_id_b1ca1261_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_groups
    ADD CONSTRAINT user_sion_groups_group_id_b1ca1261_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_sion_groups user_sion_groups_usersion_id_972ba839_fk_user_sion_email; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_groups
    ADD CONSTRAINT user_sion_groups_usersion_id_972ba839_fk_user_sion_email FOREIGN KEY (usersion_id) REFERENCES public.user_sion(email) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_sion_user_permissions user_sion_user_permi_permission_id_ee5c3b50_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_user_permissions
    ADD CONSTRAINT user_sion_user_permi_permission_id_ee5c3b50_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_sion_user_permissions user_sion_user_permi_usersion_id_eb49aa80_fk_user_sion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_sion_user_permissions
    ADD CONSTRAINT user_sion_user_permi_usersion_id_eb49aa80_fk_user_sion FOREIGN KEY (usersion_id) REFERENCES public.user_sion(email) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

