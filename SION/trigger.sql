CREATE OR REPLACE FUNCTION update_status_aktif()
RETURNS trigger AS
$$
	DECLARE status varchar;
	BEGIN 
		
		IF NEW.is_disetujui THEN
			status := 'Active';
		ELSE
			status := 'Inactive';
		END IF;

		UPDATE	ORGANISASI_TERVERIFIKASI 
		SET status_aktif = status
		WHERE email_organisasi = NEW.organisasi;
		
		RETURN NEW;

	END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER UPDATE_VERIFIKASI
AFTER UPDATE OF is_disetujui ON LAPORAN_KEUANGAN
FOR EACH ROW
EXECUTE PROCEDURE update_status_aktif();

CREATE OR REPLACE FUNCTION update_saldo()
RETURNS trigger AS
$$
	DECLARE saldo_modification integer;
			init_saldo integer;
			after_saldo integer;
	BEGIN
		IF (TG_OP = 'DELETE') THEN
			saldo_modification := OLD.nominal;
			
			SELECT saldo INTO init_saldo
			FROM DONATUR			
            WHERE  email = OLD.donatur;
				
			after_saldo := init_saldo + saldo_modification;
			
			UPDATE DONATUR
			SET SALDO = after_saldo
			WHERE email = OLD.donatur;		
			
        ELSIF (TG_OP = 'UPDATE') THEN
            saldo_modification := OLD.nominal - NEW.nominal;
			
			SELECT saldo INTO init_saldo
			FROM DONATUR			
            WHERE  email = NEW.donatur;
				
			after_saldo := init_saldo + saldo_modification;
			
			UPDATE DONATUR
			SET SALDO = after_saldo
			WHERE email = NEW.donatur;
			
        ELSIF (TG_OP = 'INSERT') THEN
            saldo_modification := NEW.nominal;
			
			SELECT saldo INTO init_saldo
			FROM DONATUR			
            WHERE  email = NEW.donatur;
				
			after_saldo := init_saldo - saldo_modification;
			
			UPDATE DONATUR
			SET SALDO = after_saldo
			WHERE email = NEW.donatur;
        END IF; 
		RETURN NULL;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER UPDATE_SALDO_AFTER_MODIFICATION
AFTER INSERT OR UPDATE OR DELETE ON DONATUR_KEGIATAN
FOR EACH ROW
EXECUTE PROCEDURE update_saldo();

CREATE TRIGGER UPDATE_SALDO_AFTER_MODIFICATION
AFTER INSERT OR UPDATE OR DELETE ON DONATUR_ORGANISASI
FOR EACH ROW
EXECUTE PROCEDURE update_saldo();