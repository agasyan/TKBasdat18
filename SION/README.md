# SION - Sistem Informasi Organisasi Nirlaba Application
Tugas Akhir Basis Data

## Tech Stack
- Django
- PostgreSQL
- Materialize.css
- Vue.js


## Installation & Getting Started
```
virtualenv venv
source venv/bin/activate
pip install requirements.txt
python3 manage.py runserver
```

## Setup Local Postgres
- Create new database in postgres database named `SION` (in Ubuntu: ```createdb SION```)
- Insert your database username / password in `SION/settings.py`
- Run the migration with command: `python3 manage.py migrate`
- Run the server with command: `python3 manage.py runserver`

## Deploying in ITF Server
- ```ssh user.sso@kawung.cs.ui.ac.id```
- Input your password
- ```ssh http://152.118.25.3```
- Input your password
```
git clone https://gitlab.com/agasyan/TKBasdat18
cd TKBasDat18
cd SION
DJANGO_ENV=production python3 manage.py migrate
DJANGO_ENV=production python3 manage.py runserver
```
- Running forever: edit settings.py and overwrite the default database configuration to production config
- Run ```nohup python3 manage.py runserver & > logs.txt```

### About Us
**Kelompok B02**
- Albertus Angga Raharja
- Aldo Bima Syahputra
- Agas Yanpratama

